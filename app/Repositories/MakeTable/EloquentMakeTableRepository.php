<?php namespace App\Repositories\MakeTable;

use App;
use Config;

/*Schema*/
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;


class EloquentMakeTableRepository implements MakeTableRepository
{

	protected $tableSetting;

	public function __construct()
	{
		$this->tableSetting = [];
	}

    public function makeTables( $setTables )
    {
    	$tablePrifix = Config::get('app.dataBasePrefix');


    	foreach( $setTables as $setTableName => $tableSetting )
    	{
    		$this->tableSetting = $tableSetting;

	        if( Schema::hasTable($tablePrifix.$setTableName) == false )
	        {
	            Schema::create($tablePrifix.$setTableName, function (Blueprint $table) {

	            	foreach( $this->tableSetting as $fieldName => $fieldType )
	            	{
	            		switch ($fieldType) {
	            			case is_string($fieldType):
	            				$this->setTableFields( $table, $fieldName, $fieldType );
	            				break;
	            			case is_array($fieldType):
								$newFieldType = !empty($fieldType[0]) ? $fieldType[0] : ''; // [string, 20] => 拆解成 newFieldType等於string
								$fieldLength  = !empty($fieldType[1]) ? $fieldType[1] : ''; // [string, 20] => 拆解成 fieldLength等於20
	            				$this->setTableFields( $table, $fieldName, $newFieldType, $fieldLength );
	            				break;
	            			default:
	            				# code...
	            				break;
	            		}
	            	}
	            	$table->timestamps();
	            });
	        }
    	}

    }


    protected function setTableFields( $table, $fieldName, $fieldType, $fieldLength='' )
    {
    	if( empty($fieldLength) )
    	{
    		$table->$fieldType( $fieldName );
    	}
    	else
    	{
    		$table->$fieldType( $fieldName, $fieldLength );
    	}
    }




    public function removeTables( $setTables )
    {
    	$tablePrifix = Config::get('app.dataBasePrefix');

    	foreach ($setTables as $setTableName => $tableSetting ) {
    		Schema::dropIfExists( $tablePrifix.$setTableName );
    	}
    }


}
