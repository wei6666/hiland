<?php
namespace App\Http\Controllers\Fantasy\Product;

use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
/**相關Models**/

use App\Http\Models\Banner;


class ProductController extends CRUDBaseController
{

    public $ajaxEditLink = 'Fantasy/Product/產品管理/ajax-list/';

    protected $modelName = "Product";

    public $index_select_field = ['id','rank','is_visible','pro_num','category_id'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'Product/產品管理';

    public $viewPreFix = 'Product';

   

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );


    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'rank',
        'sort' => 'asc'
    ];

    public $modelBelongs = [

       
        "ProductCategory" => [
            "parent" => "category_id",
            "filed" => 'id',
            "select" => ['title','id']
        ]
    ];

    public $modelHas = [

       "ProductPhoto" => [
            "modelName" => "ProductPhoto",
            "storedName" => "ProductPhoto", 
            "parent" => "product_id",
            "filed" => 'id',
            "select" => ['id','rank','is_visible','image','product_id'],
            
            ],
       
    ];

       public $saveSubData = [
                [   // 產品介紹的多圖
                    "modelName" => "ProductPhoto",
                    "requestModelName" => "ProductPhoto", //多圖的inputName
                    "to_parent" => "product_id",

                    
                ],

              
                
            ];

      public $photoTable = [
            'photo' => [
                "排序" => "rank",
                "圖片(1200 x 750)" => "image",
                "狀態" => "is_visible"
            ],
            
        ];    

       
}

