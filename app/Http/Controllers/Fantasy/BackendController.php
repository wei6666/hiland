<?php
namespace App\Http\Controllers\Fantasy;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use View;
use App;
use Config;
use ItemMaker;
use Debugbar;
use Mail;
use Session;
use Image;
use Cache;

use App\Http\Controllers\Fantasy\PermissionController as Permission;

/**相關Controller**/
use App\Http\Controllers\MenuController as MenuClass;

/**Models**/
use App\Http\Models\EnProduct\item_new;
use App\Http\Models\Setting;
use App\Http\Models\Seo;
use App\Http\Models\ContactUs;
use App\Http\Models\Menutitle;


abstract class BackendController extends BaseController {

	use DispatchesCommands, ValidatesRequests;

	public static $ProjectName = '黃山石';

	protected static $StatusOption = ["否","是"];

	protected static $adminer_mail = "victor@wddgroup.com";
	protected static $adminer_name = "黃山石";

	//public static $testingLink = 'Frankenstein';
	//下列標記tag 不可刪
	//MenuStart	
	public static $MenuList =
	[
		// "產生路徑" => [
		// 	"slogan" => "sckema",
		// 	"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
		// 	"link" => "Fantasy/schema"
		// ],

		"首頁管理" => [

			"slogan" => "首頁管理簡述",

			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',

			"link" => 'Fantasy/首頁管理/edit/1'
			
		],

		"產品系列" => [
			"slogan" => "Contact",
			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
			"link" => [
				"產品設定" => "Fantasy/Product/產品設定/edit/1",				
				"產品類別管理" => "Fantasy/Product/產品類別管理",
				"產品次類別管理" => "Fantasy/Product/產品次類別管理",
				"產品管理" => "Fantasy/Product/產品管理",


			]
		],


		"黃山石風格設定" => [
			"slogan" => "Contact",
			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
			"link" => [
				"黃山石風格設定" => "Fantasy/Style/黃山石風格設定/edit/1",				
				"黃山石風格管理" => "Fantasy/Style/黃山石風格管理",


			]
		],

		"常見問題" => [
					"slogan" => "Contact",
					"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
					"link" => [
						"問與答設定" => "Fantasy/Faq/問與答設定/edit/1",				
						"問與答管理" => "Fantasy/Faq/問與答管理",


					]
				],

		"銷售據點設定" => [
			"slogan" => "Contact",
			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
			"link" => [
				"銷售據點設定" => "Fantasy/Location/銷售據點設定/edit/1",				
				"銷售據點管理" => "Fantasy/Location/銷售據點管理",


			]
		],

		"新聞與活動" => [
			"slogan" => "Contact",
			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
			"link" => [
				"新聞基本設定" => "Fantasy/News/新聞基本設定/edit/1",				
				"新聞管理" => "Fantasy/News/新聞管理",


			]
		],

		"我的最愛設定" => [

			"slogan" => "我的最愛設定",

			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',

			"link" => 'Fantasy/我的最愛設定/edit/1'
			
		],


		"品牌與故事" => [

			"slogan" => "關於我們設定簡述",

			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',

			"link" => 'Fantasy/關於我們管理/edit/1'
			
		],

	


		"聯絡我們" => [
			"slogan" => "Contact",
			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
			"link" => [
				"聯絡我們設定" => "Fantasy/Contact/聯絡我們設定/edit/1",				
				"聯絡我們管理" => "Fantasy/Contact/聯絡我們管理",
				"各縣市選單管理" => "Fantasy/Contact/各縣市選單管理",

			]
		],

		
		"SEO" => [

			"slogan" => "SEO管理",

			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',

			"link" => 'Fantasy/SEO管理'
			
		],
		"搜尋地址設定" => [

			"slogan" => "SearchPlace",

			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',

			"link" => 'Fantasy/SearchPlace'
			
		],


		"網站基本設定" => [

			"slogan" => "網站基本設定/edit/1",

			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',

			"link" => 'Fantasy/網站基本設定'
			
		],

		"帳號管理" => [
			"slogan" => "帳號管理簡述",
			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
			"link" => [
				'帳號列表' => 'Fantasy/帳號管理/帳號列表',
				'權限' => 'Fantasy/帳號管理/權限',
			]
		],
			
	];  //MenuEnd

	//下列註解 不可刪
	//ModelStart
	protected static $ModelsArray = [
		"Account" => App\Http\Models\User::class,
		"Role" => App\Http\Models\Role::class,
		"banner"=> App\Http\Models\Banner\banner::class,
		"category"=> App\Http\Models\Banner\category::class,
		"item"=> App\Http\Models\Product\item::class,
		"Room"=> App\Http\Models\Room\Room::class,
		"Contact"=> App\Http\Models\Contact\Contact::class,
		"ContactCountry"=> App\Http\Models\Contact\ContactCountry::class,
		"ContactArea"=> App\Http\Models\Contact\ContactArea::class,
		"ContactSet"=> App\Http\Models\Contact\ContactSet::class,
		"Faq"=> App\Http\Models\Faq\Faq::class,
		"FaqCategory"=> App\Http\Models\Faq\FaqCategory::class,
		"FaqSet"=> App\Http\Models\Faq\FaqSet::class,
		"News"=> App\Http\Models\News\News::class,
		"NewsSet"=> App\Http\Models\News\NewsSet::class,
		"Location"=> App\Http\Models\Location\Location::class,
		"LocationSet"=> App\Http\Models\Location\LocationSet::class,
		"Product"=> App\Http\Models\Product\Product::class,
		"ProductCategory"=> App\Http\Models\Product\ProductCategory::class,
		"ProductSet"=> App\Http\Models\Product\ProductSet::class,
		"ProductBig"=> App\Http\Models\Product\ProductBig::class,
		"ProductPhoto"=> App\Http\Models\Product\ProductPhoto::class,
		"Code"=> App\Http\Models\Product\Code::class,
		"Style"=> App\Http\Models\Style\Style::class,
		"StyleSet"=> App\Http\Models\Style\StyleSet::class,
		"StylePhoto"=> App\Http\Models\Style\StylePhoto::class,
		"Seo"=> App\Http\Models\Seo::class,
		"Menutitle"=> App\Http\Models\Menutitle::class,
		"AboutCertificate"=> App\Http\Models\About\AboutCertificate::class,
		"AboutEquitment"=> App\Http\Models\About\AboutEquitment::class,
		"AboutSet"=> App\Http\Models\About\AboutSet::class,
		"AboutYearData"=> App\Http\Models\About\AboutYearData::class,
		"Home"=> App\Http\Models\Home\Home::class,
		"HomeImage"=> App\Http\Models\Home\HomeImage::class,
		"SearchPlace"=> App\Http\Models\Product\SearchPlace::class,
	];
	//ModelEnd

	public static $repositoryType = 'Eloquent';

	public static $repositoriesArray = [
		'MakeTable'
	];



	function __construct(){

		//$this->middleware('lang');

		self::checkRouteLang();

		//進後台才處理比對路徑是否為Fantasy
		if( preg_match("/Fantasy/", urldecode($_SERVER['REQUEST_URI']) ) )
		{

			$per_path = [];
			
			foreach (self::$MenuList as $key => $value) {
				$per_path[ $key ] = $value['link'];

			}
			//後台選單設定
			View::share('SileMenu',	Permission::MenuList($per_path));

			//後台首頁 首頁區圖片
			View::share('MenuList',	self::$MenuList);

			//檢查是否有權限到該路徑
			Permission::PathPermission();

			//後台狀態選項
			View::share("StatusOption",self::$StatusOption);

		}
		//前台才處理
		else
		{
			// /*前台共用資料*/
			$Setting = Setting::first();
			$globalSeo=Seo::where('id',99)->first();
			View::share('globalSeo',$globalSeo);
			
			$Setting = json_decode($Setting->content, true);
			View::share('Setting',$Setting);
			

			$Menutitle = Menutitle::where('is_visible',1)->OrderBy("rank","asc")->get();
			View::share('Menutitle',$Menutitle);
			
		}
		//語系
		View::share("locateLang",App::getLocale());



		View::share('locale', parent::getRouter()->current()->parameters()['locale']);

		//客戶名稱設定
		if( !empty($globalsSet['company_title']) )
		{
			View::share('ProjectName', $globalsSet['company_title']);
		}
		else
		{
			View::share('ProjectName', self::$ProjectName);
		}

	}

	public static function checkRouteLang()
	{

		$parameters = parent::getRouter()->current()->parameters();


		if(isset($parameters['locale']) AND !empty($parameters['locale']))
		{
			App::setLocale($parameters['locale']);
			//資料庫前綴字
			switch ($parameters['locale']) {
				case 'zh-tw':
					Config::set('app.dataBasePrefix','tw_');
					break;

				default:
					Config::set('app.dataBasePrefix',''.$parameters['locale'].'_');
					break;
			}

		}

	}

	protected function findDataAndAssociate( $set = [] )
	{
		$modelName = ( !empty( $set['modelName'] ) )? $set['modelName'] : '';
		$pageShow = ( isset( $_GET['show_per_page'] ) AND !empty( $_GET['show_per_page'] ) )? $_GET['show_per_page'] : '50';//每頁顯示數量
		//$toParent = ( !empty( $set['parent'] ) )? $set['parent'] : '';

		$Datas = [];


		if( !empty( $modelName ) )
		{
			$model = self::$ModelsArray[ $modelName ];

			if( isset( $_GET['search'] ) AND !empty( $_GET['search'] ) ) //如果沒有擷取到search的資料
			{
				$count = 0;
				$findWhere = '';
				foreach ($_GET['search'] as $key => $value) {


					if( $count == 0 )
					{
						if( preg_match("/_id/", $key) OR preg_match("/is_/", $key) ){

							if( preg_match("/_ids/", $key) )
							{
								$findWhere .=  " `".$key."` LIKE '%\"".$value."\"%' " ;
							}
							else
							{
								$findWhere .=  " `".$key."` = '".$value."' " ;
							}
						}
						else{
							$findWhere .=  " `".$key."` LIKE '%".$value."%' " ;
							// $findWhere .=  " `".$key."` = '".$value."' " ;
						}

					}
					else
					{
						if( preg_match("/_id/", $key) OR preg_match("/is_/", $key) ){

							if( preg_match("/_ids/", $key) )
							{
								$findWhere .=  " AND `".$key."` LIKE '%\"".$value."\"%' " ;
							}
							else
							{
								$findWhere .=  " AND `".$key."` = '".$value."' " ;
							}

						}else{
							$findWhere .=  " OR `".$key."` LIKE '%".$value."%' ";
							// $findWhere .=  " AND `".$key."` = '".$value."' " ;
						}
					}
					$count++;
				}
				$Datas = $model::select( $set['select'] )->whereRaw($findWhere);

				if( !empty($set['where']) )
				{
					foreach( $set['where'] as $where )
					{
						$field = $where[0];
						$condition = !empty($where[1]) ? $where[1] : '=' ;
						$val = !empty($where[2]) ? $where[2] : $where[1] ;

						$Datas = $Datas->where($field, $condition, $val);
					}
				}
				$Datas = $Datas->orderBy('id', 'desc')->paginate( $pageShow )->toArray();
			}
			else
			{
				$Datas = $model::select( $set['select'] );
				
				if( !empty($set['where']) )
				{
					foreach( $set['where'] as $where )
					{
						$field = $where[0];
						$condition = !empty($where[1]) ? $where[1] : '=' ;
						$val = !empty($where[2]) ? $where[2] : $where[1] ;

						$Datas = $Datas->where($field, $condition, $val);
					}
				}

				$Datas = $Datas->orderBy('id', 'desc')->paginate( $pageShow )->toArray();
			}



			if( !empty( $set['belong'] ) )//抓父關連資料
			{
				if( is_array( $set['belong'] ) AND count( $set['belong'] ) > 0 ) //關聯限定為陣列
				{

					foreach ($set['belong'] as $name => $content)
					{
						$associate = self::$ModelsArray[ $name ];

						//是否還有父類別(ex. 分館,或品牌)
						if( isset($content['withParentModel']) )
						{
							$find = $associate::select( $content['select'] )
											->with( $content['parentModel'] )
											->get()
											->toArray();// 抓全部

							/*
							原本名稱： 某某分類
							改為： 某某分館或品牌 > 某某分類
							 */
							foreach( $find as $key3 => $option )
							{
								$find[$key3]['title'] = $option[ strtolower($content['relateName']) ]['title'].' > '.$option['title'];
							}
						}
						else
						{
							$find = $associate::select( $content['select'] )->get()->toArray();// 抓全部
						}


						$Datas[ $name ] = $find;
						foreach ($Datas['data'] as $key => $data)
						{

							foreach ($find as $key2 => $value2)
							{
								if( $value2[ $content['filed'] ] ==  $data[ $content['parent'] ] )
								{
									$Datas['data'][ $key ][ strtolower( $name ) ] = $find[ $key2 ];
								}
							}

						}
					}

				}
			}


			if( !empty( $set['has'] ) )//抓子關連資料
			{
				if( is_array( $set['has'] ) AND count( $set['has'] ) > 0 ) //關聯限定為陣列
				{

					foreach ($set['has'] as $name => $content)
					{
						if(empty($content['select']))
						{
							$content['select']='*';
						}
						$associate = self::$ModelsArray[ $name ];

						foreach ($Datas['data'] as $key => $value)
						{
							$find = $associate::where($content['filed'],  $value[ 'id' ])
													->select( $content['select'] )
													->get()
													->toArray();

							$Datas['data'][ $key ][ strtolower( $name ) ] = $find;
						}
					}

				}
			}
			//將已經搜尋的條件帶到下一頁
			if( !empty( $_GET ) )
			{
				if( !empty( $Datas['next_page_url'] ) )
				{
					$org_page_link = '';
					foreach ($_GET as $key => $value) {
						if( $key != 'page' AND $key != 'search'  )
						{
							$Datas['next_page_url'] .= '&'.$key.'='.$value;
							
						}elseif ( $key == 'search' ) {
							foreach ($value as $key2 => $search) {

								$Datas['next_page_url'] .= '&search['.$key2.']='.$search;
							}
						}
					}
				}

				if( !empty( $Datas['prev_page_url'] ) )
				{
					$org_page_link = '';
					foreach ($_GET as $key => $value) {
						if( $key != 'page' AND $key != 'search' )
						{
							$Datas['prev_page_url'] .= '&'.$key.'='.$value;	
						}elseif ( $key == 'search' ) {
							foreach ($value as $key2 => $search) {

								$Datas['prev_page_url'] .= '&search['.$key2.']='.$search;
							}
						}
					}
				}

			}
			//Debugbar::info($Datas);

			return $Datas;
		}else
		{
			Debugbar::error('set model is empty.');
		}

	}

	public function sendMail( $set = [] )
	{
		$content = self::$ModelsArray[ $set['Model'] ];//抓資料的model
		$type = ( !empty($set['type']) )? $set['type'] : '';


		$data = $content::findOrFail( $set['id'] );
		$data->contact_type = ( !empty($type) )? $type : '';

		// if( !empty( $data->password ) AND $type == 'forgot' AND 0)
		// {
		// 	$cryptKey  = 'qJB0rGtIn5UB1xG03efyCp';
		// 	$data->password = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $data->password, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
		// }

        Mail::send( $set['view'], ['data' => $data], function ($m) use ( $set ) {
            $m->to($set['to'], $set['to_name'])->subject( $set['subject'] );
        });

	}

	protected function addNew( $set = [] )
	{
		//$Datas , $modelName, $routeFix
        $new = new self::$ModelsArray[ $set['modelName'] ];
        foreach ($set['Datas'] as $key => $value)
        {
        	if( $key != 'Photo' AND $key != 'App')
        	{
        		$new->$key = ( !is_array( $value ) )? $value : json_encode($value);
        	}
        }

        if( $new->save() )
        {

            return '{ "redirect" : "'.ItemMaker::url( 'Fantasy/'.$set['routeFix'].'/edit/'.$new->id ).'", "parent_id" : "'.$new->id.'" }';
        }
        else
        {
        	return '{ "redirect" : "" }';
        }
	}

	protected function updateOne($Datas, $modelName, $type='')
	{
		$model = self::$ModelsArray[$modelName];

        if( !empty( $type ) AND $type == 'ajaxEdit' )
        {

            foreach ($Datas as $row)
            {
                $work = $model::find($row['id']);

                foreach ($row as $key => $value) {
                    if($key !== 'id')
                    {
                        $work->$key = ( !is_array( $value ) )? $value : json_encode($value);
                    }
                }
                //update
                if(!$work->save())
                {
                    echo "<script>alert('work id ".$row['id']." update fail.');</script>";
                }else{
                    //echo "".$row['id']." update success.";
                }
            }
            echo "<script>alert('儲存成功');</script>";
            //echo "<script>toastrAlert('success', '儲存成功');</script>";
        }
        else
        {
            $id = $Datas['id'];

            $find = $model::find($id);

            foreach ($Datas as $key => $value)
            {
            	if( $key != 'id' )
            	{
            		$find->$key = ( !is_array( $value ) )? $value : json_encode($value);
            	}

            }

            if($find->save())
            {
                //echo "<script>alert('儲存成功');</script>";
                //echo "<script>toastrAlert('success', '儲存成功');</script>";
                return true;
            }

        }
	}

	protected function updateMulti($Datas, $modelName, $parent_field, $parent_id, $miniImage='')
	{
		if(count($Datas) > 0)
		{
			$model = self::$ModelsArray[$modelName];


			//儲存Introduction
			foreach($Datas as $row)
			{
				if(!empty($row['id']))//沒有id代表是新的一筆
				{
					foreach ($row as $key => $value)
					{
						$newInt = $model::where($parent_field ,'=', $parent_id)->where('id','=',$row['id'])->get();

						if($key != 'id')
						{
							$newInt[0]->$key = $value;

							//壓縮圖片
							if( isset($miniImage) AND !empty($miniImage) )
							{
								$miniImage['image'] = $newInt[0]['image'];
								$saveField = $miniImage['saveField'];
								$newInt[0]->$saveField = '/'.$this->minImage($miniImage);
							}
						}

						if(!$newInt[0]->save())
						{
							return '{ "message" : false }';
						}
						else
						{
							// return '{ "message" : true }';
						}
					}
				}
				else
				{
					$newInt = new $model;

					$newInt->$parent_field = $parent_id;

					foreach ($row as $key => $value)
					{
						if( $key !='id' )
						{
							$newInt->$key = $value;
						}
					}

						//壓縮圖片
						if( isset($miniImage) AND !empty($miniImage) )
						{
							$miniImage['image'] = $newInt->image;
							$saveField = $miniImage['saveField'];
							$newInt->$saveField = '/'.$this->minImage($miniImage);
						}

					if(!$newInt->save())
					{
						return '{ "message" : false }';
					}
					else
					{
						//return '{ "message" : true }';
					}
				}



			}
		}
	}

	protected function updataOneColumns( $set = [] )
	{
		$model = self::$ModelsArray[ $set['modelName'] ];

        $find = $model::find($set['id'] );
        $columms = $set['columns'];

        $find->$columms = $set['value'];

        if($find->save())
        {
        	return '{ "message" : "true" }';
        }
        else
        {
        	return '{ "message" : "false" }';
        }


	}

	protected function deleteOne($modelName, $id)
	{
		$model = self::$ModelsArray[$modelName];

        $del = $model::find($id);

        //產品的話先刪除規格
        if( $modelName == 'Item' )
        {
        	$spec = self::$ModelsArray['Spec'];
        	$specDel = $spec::where('machine_id', $del->machine_no )->where('machine_name', $del->title)->get();

        	if( count( $specDel ) > 0 )
        	{
        		foreach ($specDel as $row) {
	        		if( !$row->delete() )
	        		{
	        			echo "spec delete fail.";
	        			break;
	        		}
        		}

        	}
        }

        if($del->delete())
        {
            echo "<script>alert('刪除成功');</script>";
        }
	}


	protected function minImage( $set = [] )
	{
		$resizeFolder = ( !empty( $set['resizeFolder'] ) )? $set['resizeFolder'] : '';//resize後的資料夾
		$image = ( !empty( $set['image'] ) )? $set['image'] : '';//原圖
		$width = ( !empty( $set['width'] ) )? $set['width'] : null;//原圖
		$height = ( !empty( $set['height'] ) )? $set['height'] : null;//原圖
		$small_image = '';

		if( !empty( $image ) )
		{
			$img = Image::make(public_path().$image);

			//先檢查有沒有相同檔名的縮圖存在
			$small = explode('.', $img->basename);
			$resize = 'resize_image/'.$resizeFolder.'/'.$small[0].'_rs.'.$small[1];
			//$checkRSImg = $resize;

			if( !file_exists( public_path().'/'.$resize ) )
			{
				$img->resize($width,$height, function ($constraint)
				{
						$constraint->aspectRatio();
				});

				if($small[1] == 'png')
				{
					$img->save(public_path().'/'.$resize, 100);
				}
				else
				{
					$img->save(public_path().'/'.$resize);
				}
				$small_image = $resize;
			}
			else
			{
				$small_image = $resize;
			}

			return $small_image;
		}

	}
	protected function set_optgroup( $set = [] )
	{
		if( !empty( $set['Datas'] ) )
		{
			$Datas = [];
			$count = 0;
			foreach ($set['Datas'] as $row)
			{
				//產生optgroup label
				$label = '';
				if( !empty( $set['parent_level'] ) )
				{
					for ($i=1; $i <= $set['parent_level'] ; $i++)
					{
						$label .= ( $i == 1 )? $row[ $set['parent'.$i.'_key'] ]['title'] : " / ".$row[ $set['parent'.$i.'_key'] ]['title'];
					}

				}
				// array_push($Datas[ $label ], [
				// 	"id" => $row['id'],
				// 	"title" => $row['title']
				// ]);
				$Datas[ $label ][$count]['id'] = $row['id'];
				$Datas[ $label ][$count]['title'] = $row['title'];
				$count++;
			}
			return $Datas;
		}
		else
		{
			echo "Datas is empty.";
		}

	}
	protected function var_dump_pre( $data )
	{
		echo '<pre>';
	    	var_dump($data);
	    echo '</pre>';
	    return null;
	}


	protected static function processTitleToUrl( $title )
	{
		$replace1 = str_replace(' ', '+', $title);
		$replace2 = str_replace('/', '^', $replace1);
		$replace3 = str_replace('.', '`', $replace2);

		return $replace3;
	}


	protected static function revertUrlToTitle( $url )
	{
		$replace1 = str_replace('+', ' ', $url);
		$replace2 = str_replace('^', '/', $replace1);
		$replace3 = str_replace('`', '.', $replace2);

		return $replace3;
	}


	protected function getSeoData($key)
	{
		$Seo = self::$ModelsArray['Seo'];
		$seo = Seo::select('id','title','key','web_title','meta_keyword','meta_description','ga_code','gtm_code')
                    ->where('key', $key)
                    ->first()
                    ->toArray();

        return $seo;
	}




}
