<?php

namespace App\Http\Controllers\Fantasy\About;

use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
/**相關Models**/

use App\Http\Models\Banner;


class AboutController extends CRUDBaseController
{

    public $ajaxEditLink = 'Fantasy/關於我們管理/ajax-list/';

    protected $modelName = "AboutSet";

    public $index_select_field = ['id','rank','is_visible'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = '關於我們管理';

    public $viewPreFix = 'About';

   

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );


    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'rank',
        'sort' => 'asc'
    ];

     public $modelHas = [

       "AboutYearData" => [
            "modelName" => "AboutYearData",
            "storedName" => "AboutYearData", 
            "parent" => "about_id",
            "filed" => 'id',
            "select" => ['id','rank','is_visible',"year",'skill'],
            
            ],
         "AboutCertificate" => [
            "modelName" => "AboutCertificate",
            "storedName" => "AboutCertificate", 
            "parent" => "about_id",
            "filed" => 'id',
            "select" => ['id','rank','is_visible',"image","title"],
            
            ],

         "AboutEquitment" => [
            "modelName" => "AboutEquitment",
            "storedName" => "AboutEquitment", 
            "parent" => "about_id",
            "filed" => 'id',
            "select" => ['id','rank','is_visible',"image","title"],
            
            ],
       
    ];

       public $saveSubData = [
                [   // 產品介紹的多圖
                    "modelName" => "AboutYearData",
                    "requestModelName" => "AboutYearData", //多圖的inputName
                    "to_parent" => "about_id",

                    
                ],
                [   // 產品介紹的多圖
                    "modelName" => "AboutCertificate",
                    "requestModelName" => "AboutCertificate", //多圖的inputName
                    "to_parent" => "about_id",

                    
                ],
                [   // 產品介紹的多圖
                    "modelName" => "AboutEquitment",
                    "requestModelName" => "AboutEquitment", //多圖的inputName
                    "to_parent" => "about_id",

                    
                ],

              
                
            ];

      public $photoTable = [
            'AboutYearData' => [
                "排序" => "rank",
                "年分" => "year",
                "標題" => "skill",
                "狀態" => "is_visible",
                
            ],

            'AboutCertificate' => [
                "排序" => "rank",
                "圖片(160 x 255)" => "image",
                "標題" => "title",
                "狀態" => "is_visible",
                
            ],
            'AboutEquitment' => [
                "排序" => "rank",
                "圖片(700 x 430)" => "image",
                "狀態" => "is_visible",
                "圖片標題" => "title",
                
            ],
            
        ];    





       
}
