<?php namespace App\Http\Controllers\Fantasy;

use App\Http\Controllers\Fantasy\BackendController;
use Illuminate\Http\Request;
use Auth;
use Session;
use Debugbar;
use App;
//use Redirect;
use App\Http\Controllers\Fantasy\MakeItemV2;

use App\Http\Models\Role;

class PermissionController extends BackendController {

	public static function MenuList($menu)
	{
		$permission_path = [];
		// echo "all link is :<br>";
		// var_dump($menu);
		// echo "<br>=======================<br>";
		// 檢查是否為有權限的使用者
		if (Auth::user())
		{
			$MenuSession = Session::get('MenuList');
			//檢查ＳＥＳＳＩＯＮ是否有設定
			if (empty($MenuSession))
			{
				if (Auth::user()->character === 'superAdminer')
				{
					foreach ($menu as $key => $value)
					{
						$permission_path[ $key ] = $value;
					}
				}
				else
				{
					$character = json_decode(Auth::user()->character,true);
						$permission_path = [];
						foreach ($character as $row)
						{

							$Role = Role::where('id',$row)->select('permission')->get()->first()->toArray();

							$Charas = json_decode($Role['permission'], true);
							//var_dump($Charas);
							if( !empty( $Charas ) )
							{
								foreach ($Charas as $row)
								{
									// var_dump($row);
									if( !in_array($row, $permission_path ) )
									{
										array_push($permission_path, $row );
									}
								}
							}

						}

					//檢查使用者權限 , 將沒有權限的路徑剔除
					if( !empty( $permission_path ) )
					{
						// echo "permission is :";
						// print_r($Charas);
						// echo "<br>=======================<br>";
						foreach ($menu as $key=>$value)
						{

							if( is_array( $value ) )
							{
								foreach ($value as $key3 => $value3) {
									if (!in_array($value3, $permission_path))
									{
										unset($menu[$key][$key3]);

									}
								}
							}
							else if( is_string( $value ) )
							{
								if (!in_array($value, $permission_path))
								{
									unset($menu[$key]);
								}
							}
						}
					}
					//如果大選單底下是空的，那就unset
					foreach ($menu as $key=>$value)
					{

						if (is_array($menu[$key]) AND empty($menu[$key]))
						{

							unset($menu[$key]);
						}
						else
						{
							// foreach ($value as $key2 => $value2)
							// {
							// 	if (is_array($menu[$key][$key2]) AND empty($menu[$key][$key2]))
							// 	{
							//
							// 		unset($menu[$key][$key2]);
							// 	}
							// }

						}
					}


				}
				// echo "可以進去的連結：<br>";
				// var_dump($menu);
				// die;
				Session::put('MenuList',$menu);

				return Session::get('MenuList');
			}else{
				return Session::get('MenuList');
			}
		}else{
			//echo "is not user.";
		}

	}

	public static function PathPermission_old()
	{
		//層級管理
		if (Auth::user())
		{
			if (Auth::user()->character === 'superAdminer')
			{
				//print_r($_SERVER);
			}
			else
			{
				$managerLevel = 3;
				$thisLink = urldecode( $_SERVER['REQUEST_URI']);


				$Path  = array_values(array_filter(explode('/', $thisLink)));

				//$Path = array_filter( explode('/',$_SERVER['REQUEST_URI']) );
				$MenuPermission = Session::get('MenuList');
				//var_dump($MenuPermission);
				$pathToCheck = '';
				if( preg_match('/Fantasy/', urldecode( $_SERVER['REQUEST_URI']) ) )
				{
					$count = 1;
					foreach ($Path as $key => $value)
					{
						if ($key <=$managerLevel AND $key !=0)
						{
							$pathToCheck.= ( $managerLevel != $key )? $value."/" : $value;
							$count++;
						}
						echo $count;
						
					}	
					echo $pathToCheck;

					if (count($Path)>1 )
					{
						$path_array = [];

						foreach ($MenuPermission as $row)
						{
							//var_dump($row);
							//var_dump($pathToCheck);
							//Debugbar::info('=======MenuPermission as row=======');
							//Debugbar::info($row);
							if( !empty( $row ) )
							{
								//var_dump($row);
								if( is_string( $row ) )
								{


									if( !in_array( $row, $path_array ) )
									{
										array_push($path_array, $row);
									}
								}
								else if( is_array( $row ) )
								{
									foreach($row as $link )
									{
										if( !in_array( $link, $path_array ) )
										{
											array_push($path_array, $link);
										}
									}
								}

							}

						}

						//
						echo "<pre>";
						var_dump($path_array);
						echo "</pre>";
						//var_dump($pathToCheck);

						if ( !in_array($pathToCheck, $path_array) AND $pathToCheck !='Fantasy/' )
						{
							echo "You don't have permission.<br>";
							echo "<a href='".MakeItemV2::url('Fantasy')."'>返回首頁</a>";
							die;
						}
					}
				}

			}


		}
	}

	public static function PathPermission()
	{
		//層級管理
		if (Auth::user())
		{
			if (Auth::user()->character === 'superAdminer')
			{
				
			}
			else
			{

				$thisLink = urldecode( $_SERVER['REQUEST_URI']);

				$Path  = array_values(array_filter(explode('/', $thisLink)));//將空白的ＫＥＹ值去除，並重新排序ＫＥＹ值
				$MenuPermission = Session::get('MenuList');
				$checkLink = '';
				$is_permission = false;

				$path_array = [];//收集有權限的連結

				foreach ($MenuPermission as $row)//只取出功能連結，去除FANTASY
				{
					if( !empty( $row ) )
					{
						//var_dump($row);
						if( is_string( $row ) )
						{

							if( !in_array( $row, $path_array ) )
							{
								$row = str_replace("Fantasy/", "", $row);
								array_push($path_array, $row);
							}
						}
						else if( is_array( $row ) )
						{
							foreach($row as $link )
							{
								if( !in_array( $link, $path_array ) )
								{
									$link = str_replace("Fantasy/", "", $link);
									array_push($path_array, $link);
								}
							}
						}

					}

				}

				foreach ($Path as $key => $value) 
				{
					
					if( App::getLocale() == $value OR $value == 'Fantasy' )
					{
						unset($Path[$key]);
					}

				}

				if( count( $Path ) > 1)
				{
					$checkLink = implode("/", $Path);

					//過濾是否有權限使用功能
					foreach ($path_array as $row) 
					{

						$this_link_check = strpos($checkLink, $row);
						if( is_int( $this_link_check ))
						{
							$is_permission = true;
						}
					}

					if ( !$is_permission)
					{
						echo "You don't have permission.<br>";
						echo "<a href='".MakeItemV2::url('Fantasy')."'>返回首頁</a>";
						die;
					}

				}

			}


		}
	}

}
