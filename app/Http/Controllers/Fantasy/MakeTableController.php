<?php

namespace App\Http\Controllers\Fantasy;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;

/*Schema*/
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

/*repository*/
use App\Repositories\MakeTable\MakeTableRepository;

class MakeTableController extends BackendController
{
	private $tables;


    public function __construct(MakeTableRepository $makeTableRepoist)
    {
        parent::__construct();
        $this->tables = $this->setTables();
        var_dump($this->tables);die;
        $this->makeTableRepoist = $makeTableRepoist;
        

    }

    private function setTables()
    {
    	$tables = [
    		/* 食尚天地 =========================================== */
    		'food_categories' => [
    			'id' => 'increments',
    			'rank' => 'integer',
    			'is_visible' => 'integer',
    			'title' => ['string',50],
    			'sub_title' => ['string',50],
    		],
    	];

    	return $tables;
    }


    protected function makeTables()
    {
    	$this->makeTableRepoist->makeTables( $this->tables );

    	var_dump('建立資料表完成 Create Tables Done!');
    }


    protected function removeTables()
    {
    	// $this->makeTableRepoist->removeTables( $this->tables );

    	// var_dump('刪除資料表完成 Delete Tables Done!');
    }



}
