<?php

namespace App\Http\Controllers\Fantasy;

/**原生函式**/
use Illuminate\Http\Request;
use View;
use Config;
use Session;
use Route;
use App;
use Validator;
use Debugbar;
use Excel;
use App\Http\Controllers\Fantasy\MakeItemV2;
/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;

/**Models**/
use App\Http\Models\Role;

class RoleController extends BackendController
{

    //批次修改路徑
    protected static $ajaxEditLink = 'Fantasy/帳號管理/權限/ajax-list/';

    //批次修改顯示及編輯的欄位設定
    public static $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "權限標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> false
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-danger',
                "showText" => 'S'
            )
        ),
    );
    public function __construct()
    {
        parent::__construct();

        $per_path = [];

        foreach (parent::$MenuList as $key => $value) {
            $per_path[ $key ] = $value['link'];
        }

        // var_dump($per_path); die;

        View::share('MenuLinks', $per_path);

        if(!empty(Session::get('Message')))
        {
            View::share('Message',Session::get('Message'));
        }else{
            View::share('Message','');
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        $Datas = [];

        $Datas = parent::findDataAndAssociate([
            "modelName" => 'Role',
            "select" => ['id','is_visible','title'],
        ]);

        return view('Fantasy.Role.index',[
            "Datas" => $Datas,
            "ajaxEditLink" => self::$ajaxEditLink
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        return view('Fantasy.Role.edit',
            [
                'data'=>[],
                'actionUrl' => MakeItemV2::url('Fantasy/帳號管理/權限/store')
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
        $Datas = $request->input('Role');

        $Datas['permission'] = json_encode($Datas['permission']);

        $resoult = json_decode(parent::addNew([
            "Datas" => $Datas ,
            "modelName" => 'Role',
            "routeFix" => '帳號管理/權限'
            ]), true );

        if( !empty( $resoult['redirect'] ) )
        {
            return redirect( $resoult['redirect'] )->with('Message','新增成功, 轉到編輯頁面');

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($locale, $id)
    {
        $Data = Role::where('id',$id)->get()->first()->toArray();
        //Debugbar::info($Data);
        //Debugbar::info($Data);
        $Data['permission'] = ( !empty( $Data['permission'] ) )?json_decode($Data['permission'], true) : [];


        //Debugbar::info($Data);
        return view('Fantasy.Role.edit',[
                'data' => $Data,
                'actionUrl' => MakeItemV2::url('Fantasy/帳號管理/權限/update'),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postUpdate(Request $request)
    {
        if(!empty($request->input('method')) AND $request->input('method')=='ajaxEdit')
        {
            //echo "批次修改<br>";
            parent::updateOne( $request->input('Role'), 'Role', 'ajaxEdit');
        }
        else
        {
            $Datas = $request->input('Role');

            $Datas['permission'] = json_encode($Datas['permission']);

            if( parent::updateOne( $Datas, 'Role', '') )
            {
                return redirect( MakeItemV2::url('Fantasy/帳號管理/權限/edit/'.$Datas['id']) )->with('Message','修改成功');
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postDestroy(Request $request)
    {
        $id = $request->input('id');

        $method = ( !empty( $request->input('method') ) )? $request->input('method') : '';
        //$method =  $request->input('method');

        if( empty( $method ) )
        {
            parent::deleteOne( 'Role', $id );
        }
        else
        {
            if( !empty( $id ) AND count( $id ) > 0 )
            {
                foreach ($id as $row) {
                    parent::deleteOne($method, $row );
                }
            }
        }

    }
    /*==============jQuery Ajax ====================*/
    //批次修改
    public static function postAjaxList(Request $request)
    {
        $ids = $request->input("ids");
        $works = Array();
            foreach ($ids as $row) {
                $works[] = Role::where('id','=',$row)
                            ->select('id','name', 'is_visible','email')
                            ->get();
            }
        return view('Fantasy.Ajax.list')
                    ->with('ajaxEditList',self::$ajaxEditList)
                    ->with('modal','Role')
                    ->with('update_link', MakeItemV2::url('Fantasy/帳號管理/權限/update'))
                    ->with('datas',$works);
    }

    public function postChangeStatic(Request $request)
    {
        $datas = $request->all();

        $res = parent::updataOneColumns([
                'modelName' => 'Role',
                'id' => $datas['id'],
                'columns' => $datas['columns'],
                'value' => $datas['value']
            ]);

        return $res;

    }

    /*===============後台結束=======================*/

}
