<?php 
namespace App\Http\Controllers\Fantasy;

use Illuminate\Routing\Controller as BaseController;
//use App\Http\Controllers\Fantasy\MakeItem;
use App\Http\Controllers\Fantasy\MakeItemV2;

use Debugbar;
class MakeFormV2 extends BackendController {
	/***
		產生產品規格

		$tabelTags -> 表格抬頭thead的標籤

		$datas -> 資料

	***/
	public static function SpecTable($tabelTags,$datas = Array()){
		if(is_array($tabelTags)){

			$shows = Array();
			$thead ='';

			foreach ($tabelTags as $key => $value) {
				if ($value === 'Image' OR $value === 'image'){

					$thead.= '<th width="12%">'.$value.'</th>';

				}else{

					$thead.= '<th>'.$value.'</th>';

				}
				$shows[] = $value;
			}
			$tbody = '';
			if (count($datas)>0) {
				$count = 0;
				foreach ($datas as $row) {
					$tbody.='<tr>'; 

					//拆解表格內容
					$formContent = json_decode($row['form_details'],true);
					$sort = $count+1;
					$tbody.='<td width="2%" align="center"><label><h1>'.$sort.'</h1><input type="hidden" name="Spec['.$count.'][id]" value="'.$row['id'].'"></label></td>';

					//讓表格內容可以依照設定的順序顯示
					foreach ($tabelTags as $key => $value) {
						//圖片
						if ($value === 'Image' OR $value === 'image') {

							$tbody.='<td>'.MakeItemV2::photo('', 'Spec['.$count.'][image]', $row['image'] ,'' , 'table').'</td>';
							
						}else{
							if (!array_key_exists($value, $formContent)) {
								$tbody.='<td><label><input type="text" name="Spec['.$count.']['.$value.']" value=""></label></td>';
							}else{
								$tbody.='<td><label><input type="text" name="Spec['.$count.']['.$value.']" value="'.$formContent[$value].'"></label></td>';
							}
							
						}
					}

					$tbody.='<td>
								<input type="hidden" name="thisID_'.$row['id'].'" id="thisID_'.$row['id'].'" value="'.$row['id'].'">

								<a name="rank_up" class="btn purple" ng-click="moveUp('.$row['id'].')"><i class="fa fa-level-up"></i></a>
								<a name="rank_down" class="btn purple" ng-click="moveDown('.$row['id'].')"><i class="fa fa-level-down"></i> </a>

								<a href="javascript:;" class="btn red" id="dataDel"  data-id="'.$row['id'].'" data-model="Spec"><i class="fa fa-times"></i></a>
							</td>
											
					';
					$tbody.='</tr>';
					$count++;	
				}
			}else{
				$span = count($tabelTags)+2;
				$tbody = '<tr><td colspan="'.$span.'" align="center"> No Datas.</td></tr>';
			}

		print('

			<table class="table table-bordered table-hover" id="specTable" name="Spec" data-tags="'.implode(",",$shows).'">
				<thead >
					<tr role="row" class="heading" id="FormTags">
					<th width="10%"></th>
						'.$thead.'
						<th width="10%"></th>
					</tr>
				</thead>
				<tbody id="FormLine">
					'.$tbody.'
				</tbody>
			</table>
			');			
		}else{
			echo "Table Tags Isn't Array. " ;
		}
	}
	/***
		相關圖片表格

		$tabelTags -> 表格抬頭thead的標籤

		$datas -> 資料

		$FilePath-> 上傳路徑
	***/

	public static function  photosTable( $set = [] )
	{
		$nameGroup = ( !empty( $set['nameGroup'] ) )? $set['nameGroup']: '';
		$datas = ( !empty( $set['datas'] ) )? $set['datas'] : [];
		$table_set= ( !empty( $set['table_set'] ) )? $set['table_set']: [];

		if (is_array($table_set))
		{
			//存放要顯示的資料庫欄位
			$shows = Array();
			$thead ='';
			$nameGroup = ( preg_match("/-/", $nameGroup) )? (explode('-', $nameGroup)) : $nameGroup;
			$prefix = ( is_array($nameGroup) )? $nameGroup[1] : $nameGroup; 
			$modelGrop = ( is_array($nameGroup) )? $nameGroup[0].'/'.$prefix : $prefix ;
			$count = 0;
			//產生標頭
			foreach ($table_set as $key => $value) {
				$thead.=(preg_match("/Image/", $key) OR preg_match("/image/", $key)) ? '<th class="header_th_'.$count.'" >'.$key.'</th>' : '<th class="header_th_'.$count.'">'.$key.'</th>';
				$shows[] = $value;
				$count++;
			}

			$tbody = '';

			if (count($datas) > 0)
			{
				$count = 0;
				foreach ($datas as $row) 
				{
					$tbody.='<tr>';
						$tbody .='                                                                                
								<td>
                                    <input type="checkbox" name="'.$prefix.'['.$count.'][id]" value="'.$row['id'].'">
                                    <input type="hidden" name="'.$prefix.'['.$count.'][id]" value="'.$row['id'].'" />
                                </td>';
					
						foreach ($table_set as $row2) 
						{
							//if ($row2 =='image' OR $row2 == 'images') 
							if(preg_match("/image/", $row2))
							{
								$img = (!empty($row[$row2])) ? '<img src="'.$row[$row2].'" >' : '<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>';
								
								$tbody.='<td lass="change_img">'.MakeItemV2::photo([
										'inputName' => $prefix.'['.$count.']['.$row2.']',
										'value' => $row[$row2],
										'type' => 'table'
									]).'</td>';
							}else if($row2 == 'rank')
							{
								$tbody .= '<td align="center" id="sortNum"> 
												<h1>'.($count+1).'</h1>
												<input type="hidden" name="'.$prefix.'['.$count.']['.$row2.']" value="'.$row[$row2].'">
											</td>';
							}
							else if( preg_match("/color/", $row2) )
							{
								$tbody .= '<td>'.MakeItemV2::colorPicker([
										'inputName' => $prefix.'['.$count.']['.$row2.']',
										'value' => $row[$row2],
										'type' => 'table'
									] ).'</td>';

							}
							else if( $row2 == 'go_news_content')
							{							
								$tbody .= '<td Width=5%><input type="button" value = "go and edit" data-id = "'.$row['id'].'" id = "go_news_content" data-url = "'.MakeItemV2::url("/").'"></td>';

							}
							else if( preg_match("/file/", $row2) )
							{
								$tbody .= '<td>'.MakeItemV2::filePicker([
										'inputName' => $prefix.'['.$count.']['.$row2.']',
										'value' => $row[$row2],
										'type' => 'table'
									] ).'</td>';
							}
							else if( $row2 == 'skill' || $row2 =="question" || $row2 =="answar")
							{							
								$tbody .= '<td>'.MakeItemV2::textArea([
										'inputName' => $prefix.'['.$count.']['.$row2.']',
										'value' => $row[$row2],
										'type' => 'table'
									]).'</td>';

							}
							else if( $row2 == 'features')
							{							
								$tbody .= '<td>'.MakeItemV2::editor([
										'inputName' => $prefix.'['.$count.']['.$row2.']',
										'value' => $row[$row2],
										'type' => 'table'
									]).'</td>';

							}
							else if( $row2 == 'time' AND 0)
							{							
								$tbody .= '
								<td>
									<table class="table table-bordered table-hover" data-model ="'.$prefix.'" data-tags="time">
										<thead>
											<tr>
												<th>
													&nbsp;
												</th>
												<th>
													<a href="javascript:;" id="addItem_min" data-tags="time" class="btn green" ><i class="fa fa-plus"></i> </a>
												</th>
											</tr>
										</thead>
										<tbody id="itemTbody">
	
										</tbody>
									</table>
								</td>
								';

							}
							else if( preg_match("/is_/", $row2)  )
							{	
								if($row2 == 'is_perc')
								{
									$tbody .= '<td>'.MakeItemV2::radio_btn([
											'inputName' => $prefix.'['.$count.']['.$row2.']',
											'value' => $row[$row2],
											'options' => ['否', '是'],
											'type' => 'table'
										]).'</td>';
								}
								else
								{
									$tbody .= '<td>'.MakeItemV2::radio_btn([
											'inputName' => $prefix.'['.$count.']['.$row2.']',
											'value' => $row[$row2],
											'options' => ['否', '是'],
											'type' => 'table'
										] ).'</td>';
								}						
								

							}
							else
							{
								$tbody .='<td><input class="form-control form-filter input-sm" name="'.$prefix.'['.$count.']['.$row2.']" value="'.$row[$row2].'" /></td>';
							}
							
						}

						$count++;
					$tbody.='</tr>';
				}
			}

			$addLink = ( !empty( $set['pic'] ) AND $set['pic'] )? 'id="multi-add"' : 'id="text-add"';

			print('
	                <div id="tab_images_uploader_container" class="text-align-reverse margin-bottom-10 margin-bottom-10a text-align-reverse-left container_pad40 fixed_header_newcode default">
	                    <a  '.$addLink.' href="javascript:;" class="btn btn-success tooltips" data-container="body" data-name="'.$prefix.'_table" data-tags="'.implode(",",$shows).'" data-placement="top" data-original-title="新增/建立資料" data-rel="fancybox-button">
	                        <i class="fa fa-plus"></i> 新增/插入 
	                    </a>
	                    <!--<a '.$addLink.' href="javascript:;" class="btn btn-success  tooltips" data-container="body" data-name="'.$prefix.'_table" data-tags="'.implode(",",$shows).'" data-placement="top" data-original-title="編輯圖片" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="批次新增/建立">
	                        <i class="fa fa-plus-square"></i> 批次新增/插入 
	                    </a>-->
	                    <a id="delete-multi" data-model="'.$modelGrop.'" data-routename="'.$nameGroup[0].'" data-prefix="'.$prefix.'" href="javascript:;" class="btn btn-success tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="刪除">
	                        <i class="fa fa-remove"></i> 刪除 
	                    </a>
	                    <a id="tab_images_uploader_uploadfiles" name="rank_up" href="javascript:;" data-prefix="'.$prefix.'" data-model="'.$modelGrop.'" class="btn dark btn-outline tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="排序往上">
	                        <i class="fa fa-long-arrow-up"></i> 
	                    </a>
	                    <a id="tab_images_uploader_uploadfiles" name="rank_down" href="javascript:;" data-prefix="'.$prefix.'" data-model="'.$modelGrop.'" class="btn dark btn-outline tooltips" data-container="body" data-placement="top" data-original-title="排序往上">
	                        <i class="fa fa-long-arrow-down"></i> 
	                    </a>
	                    <a id="tab_images_uploader_uploadfiles" href="javascript:;" data-model="'.$modelGrop.'" class="btn dark btn-outline tooltips" data-container="body" data-placement="top" data-original-title="重新載入">
	                        <i class="fa fa-undo"></i> 
	                    </a>
	                </div>

					<input type="hidden" name="method" value="'.$prefix.'" >
		            <div class="portlet-body portlet-body-news">
		                <div class="table-container">
		                    <div class="table-container">
		                        <div id="datatable_ajax_wrapper" class="dataTables_wrapper dataTables_extended_wrapper_news dataTables_extended_wrapper no-footer ">
		                            <div class="table-responsive">
										<table class="table table-striped table-bordered table-hover table-header-fixed dataTable no-footer  table-header-news  marign-top-pad0 " id="'.$prefix.'_table" data-model ="'.$prefix.'" name="'.$prefix.'" data-tags="'.implode(",",$shows).'" >
											<thead>
												<tr role="row" class="heading">
					                                <th width="20" class="sorting_disabled" rowspan="1" colspan="1" aria-label="">
					                                    <input type="checkbox" class="group-checkable">
					                                </th>
													'.$thead.'
												</tr>
											</thead>
											<tbody id="itemTbody" >
												'.$tbody.'
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
			');
		}else
		{
			echo "Table Tags Isn't Array. " ;
		}

	}
	/*****
		
	******/
	public static function  haveSubTable($nameGroup = '', $datas = Array() ,$table_set= Array(), $multi)
	{

		if (is_array($table_set))
		{
			//存放要顯示的資料庫欄位
			$shows = Array();
			$thead ='';
			$nameGroup = (explode('-', $nameGroup))? (explode('-', $nameGroup)) : $nameGroup;

			//產生標頭
			foreach ($table_set as $key => $value) {
				$thead.=($key == 'Image' OR $key == 'image') ? '<th width="15%">'.$key.'</th>' : '<th>'.$key.'</th>';
				$shows[] = $value;
			}

			$tbody = '';

			if (count($datas) > 0)
			{
				$count = 0;
				foreach ($datas as $key => $value) 
				{
					$tbody.='<tr>';
					$tbody .='<td><input name="'.$nameGroup[1].'['.$count.'][time_area]" value="'.$key.'" /></td>';
						$timeDatas = '';
						$subCount = 0;
						foreach ($value as $key2 => $value2) 
						{
							$timeDatas.='<tr>';
							$timeDatas.='<td><input type="text" name="'.$nameGroup[1].'['.$count.']['.$subCount.'][time]" value="'.$value2.'" /></td>';

							$timeDatas.='<td>
										<input type="hidden" name="'.$nameGroup[1].'['.$count.']['.$subCount.'][id]" value="'.$key2.'" />
										<a href="javascript:;" class="btn red" id="dataDel"  data-id="'.$key2.'" data-model="'.$nameGroup[0].'/'.$nameGroup[1].'"><i class="fa fa-times"></i></a>
									</td>';
							$timeDatas.='</tr>';
							$subCount++;
						}
						$tbody .= '
						<td>
							<table class="table table-bordered table-hover" data-model ="'.$nameGroup[1].'" data-tags="time">
								<thead>
									<tr>
										<th>
											&nbsp;
										</th>
										<th>
											<a href="javascript:;" id="addItem_min" data-tags="time" class="btn green" ><i class="fa fa-plus"></i> </a>
										</th>
									</tr>
								</thead>
								<tbody id="itemTbody">
									'.$timeDatas.'
								</tbody>
							</table>
						</td>
						';

						$count++;
					$tbody.='</tr>';
				}
			}else
			{
				$span = count($table_set)+1;
				$tbody = '<tr id="is_no_data"><td colspan="'.$span.'" align="center" > No Datas.</td></tr>';
			}
			$isMulti = ($multi == 'yes')?'<a id="multi-add" href="javascript:;"  class="btn green" data-name="'.$nameGroup[1].'_table"><i class="fa fa-plus"> 批次新增</i></a>' : '';
			print('
					<div class="row">
						<div id="tab_images_uploader_filelist" class="col-md-6 col-sm-12">
						</div>
					</div>

					<input type="hidden" name="method" value="'.$nameGroup[1].'" >
						'.$isMulti.'
					<br>
					<table class="table table-bordered table-hover" id="'.$nameGroup[1].'_table" data-model ="'.$nameGroup[1].'" name="'.$nameGroup[1].'" data-tags="'.implode(",",$shows).'" >
						<thead>
							<tr role="row" class="heading">
								'.$thead.'
								<th width="17%">
									<a id="addItem" href="javascript:;" data-tags="'.implode(",",$shows).'" class="btn green"  >
										<i class="fa fa-plus"></i> 
									</a>
								</th>
							</tr>
						</thead>
						<tbody id="itemTbody" >
							'.$tbody.'
						</tbody>
					</table>
			');
		}else
		{
			echo "Table Tags Isn't Array. " ;
		}

	}

	/*批次修改*/
	public static function ajaxEditFrom ($model, $table, $data, $update_link)
	{

		if (is_array($table)) {

			//表格標頭
			$header = '';
			$tbody = '';
			foreach ($table as $key => $value) {
				$header.=($key == 'Image' OR $key == 'image') ? '<th width="15%">'.$key.'</th>' : '<th>'.$key.'</th>';
			}
			$count = 0;
			if (is_array($data)) {
				//表格內容
				foreach ($data as $row) {
					foreach ($row as $key => $value) {
						$tbody.='<tr>';

							$tbody.= '<td>'.($count+1).'<input type="hidden" name="'.$model.'['.$count.'][id]" value="'.$value['id'].'"></td>';

							foreach ($table as $row2) {
								
								//如果這一個row有多組設定，Exp:狀態
								if(isset($row2['field']) AND $row2['field']!='')
								{
									if ($row2['inputType'] =='text') {
										if ($row2['is_edit']) {
											$tbody.='<td ><input class="form-control form-filter input-sm" type="text" name="'.$model.'['.$count.']['.$row2['field'].']" value="'.$value[$row2['field']].'"></td>';
										}else{
											$tbody.='<td align="center">'.$value[$row2['field']].'</td>';
										}
									}

									if ( $row2['inputType'] == 'date' ) {
										if ($row2['is_edit']) {
											//$tbody.='<td ><input class="form-control form-filter input-sm" type="text" name="'.$model.'['.$count.']['.$row2['field'].']" value="'.$value[$row2['field']].'"></td>';
											$tbody .= '<td>'.MakeItemV2::datePicker([
													'inputName'=> $model.'['.$count.']['.$row2['field'].']',
													'value' => $value[$row2['field']],
													'type' => 'table'
												]).'</td>';
										}else{
											$tbody.='<td align="center">'.$value[$row2['field']].'</td>';
										}
									}

									if ($row2['inputType'] =='textarea') {
										if ($row2['is_edit']) {
											$tbody.='<td><textarea cols="30" rows="8" name="'.$model.'['.$count.']['.$row2['field'].']">'.$value[$row2['field']].'</textarea></td>';
										}else{
											$tbody.='<td align="center">'.$value[$row2['field']].'</td>';
										}
									}

								}else{
									//產出群組設定項
									$tbody.= '<td>';
									foreach ($row2 as $key2=>$value2) {



										if ($value2['inputType'] =='radio') {

											//$showText = (isset($value2['show_text']))? $value2['show_text'][$value[$value2['field']]] : $key2;
											$tbody .= MakeItemV2::radio_color(
												[
													'value' => $value[$value2['field']],
													'inputName' => $model.'['.$count.']['.$value2['field'].']',
													'ajax' => false,
													'showColor' => $value2['showColor'],
													'helpText' => $key2,
													'showText' => $value2['showText']
												]
											);

											//$thisValue = ($value[$value2['field']] == '1')? 'blue': 'default';

											

											/*暫時先不去控制項目能不能修改   不修改就先不出現
											if ($value2['is_edit']) {

											}else{
												$tbody.= ''.$key2.' : <a  class="btn btn-circle default ">'.$showText.'</a>';
											}*/
											// $tbody.= '
											// 	<input type="hidden" name="'.$model.'['.$count.']['.$value2['field'].']" value="'.$value[$value2['field']].'" >

											// 	'.$key2.' : 
											// 	<a id="EditStatic" href="javascript:;" data-id="'.$value['id'].'" data-model="'.$model.'" data-field="'.$model.'['.$count.']['.$value2['field'].']"  class="btn btn-circle '.$thisValue.' ">
											// 		'.$showText.'
											// 	</a>
											// ';
										}

									}
									$tbody.='</td>';
								}
							}
						$tbody.='</tr>';
						$count++;
					}

				}
			}else{
				echo "Data is not Array.";
			}

			print('
				<form method="POST" action="'.$update_link.'" target="hidFrame">
					'.csrf_field().'
					<input type="hidden" name="method" value="ajaxEdit">
			            <div class="modal-body modal-body_min">
			                <div id="blockui_sample_3_1_element"><!--get data from ajax - list-->
								<table class="table table-striped table-bordered table-hover table-header-fixed dataTable no-footer table_overflow table_overflow_width" aria-describedby="datatable_ajax_info" role="grid" id="datatable_list" >
									<thead>
										<tr role="row" class="heading filter">
											<th></th>
											'.$header.'
										</tr>
									</thead>
									<tbody >
										'.$tbody.'
									</tbody>
								</table>
			                </div>
			            </div>
			            <div class="modal-footer">
			                <button type="button" id="ajaxEditClose" class="btn btn-outline sbold red" data-dismiss="modal">關閉</button>
			                <button type="submit" id="ajaxEditSubmit" class="btn btn-outline sbold blue">儲存</button>
			            </div>
			        </div>

				</form>
				<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
			');
		}else{
			echo "Table Set is not Array.";
		}
	}

	//後台列表table

	public static function listTable($set = [])
	{

		$ajaxLink = ( !empty( $set['ajaxEditLink'] ) )? $set['ajaxEditLink'] : '' ;
		$searchLink = ( !empty( $set['model'] ) )? MakeItemV2::url( 'Fantasy/'.$set['model'] ) :'';

		$trClass = ["even", "odd"];//tr 漸層類別
		$deleteButton = '
			<a class="btn red" data-id="" data-model="" id="dataDel" href="javascript:;">
				<i class="fa fa-times"></i>
			</a>';

		//thead
		$thead = '';
		$fliter = '';

		if( !empty( $set['tableSet'] ) )//tableSet 必須設定
		{
			$count = 1;
			$specTdArray = ['rank','date','image','img','select'];

			foreach ($set['tableSet'] as $row ) {

				$width = ( !empty( $row['width'] ) )? ''. $row['width'] .'': '';

				if( !empty( $row['columns'] ) AND in_array($row['columns'], $specTdArray) )
				{

					$tdClass = "list_table_td_".$row['columns'];
				}
				else if( !empty( $row['fliter'] ) AND in_array($row['fliter'], $specTdArray) )
				{
					$name = ( $row['fliter'] == 'select' )? 'category' : $row['fliter'];

					$tdClass = "list_table_td_".$name;
				}
				else
				{
					$name = ( $row['title'] == '狀態列' )? 'status' : 'normal';
					$tdClass = 'list_table_td_'.$name;	
				}

				$thead .= '<td class="'.$tdClass.'" width="'. $width .'" > '. $row['title'] .' </td>';

				if( !empty( $row['group'] ) )
				{
					$fliter .='
						<td>
						<select name="group_static" data-model="'.$set['modelName'].'" class="form-control form-filter input-sm" id="formFilter"  data-columns="'. $count .'">
							<option value="" > All</option>
					';
						foreach ($row['group'] as $group) {
							$fliter .='<optgroup label=" '.$group['title'].'">';
								foreach ($group['options'] as $key2 => $value2) {
									$getValue = ( isset( $_GET['search'][ $group['columns'] ] ) AND $_GET['search'][ $group['columns'] ] == $key2 )? 'selected' : '';
									
									$fliter .='<option value="'.$group['columns'].','.$key2.'" '.$getValue.'>'.$value2.'</option>';
								}
							$fliter .='</optgroup>';
						}
					$fliter .='
						</select>	
					</td>
					';
				}
				else
				{
					$fliter_columns = ( !empty( $row['fliter'] ) )? $row['fliter'] : 'text';//data table 篩選欄位類型
					$getValue = ( !empty( $_GET['search'][ $row['columns'] ] ) )? $_GET['search'][ $row['columns'] ] : '';
					switch ( $fliter_columns ) 
					{
						case 'select':
							$fliter .= '
								<td>
		                            <select name="'.$set['modelName'].'['.$row['columns'].']" class="form-control form-filter input-sm" data-model="'.$set['modelName'].'" id="formFilter"  data-columns="'. $count .'">
		                                <option value=""> All </option>
		                     ';         
		                     			if( !empty( $row['options'] ) )
		                     			{

			                     			foreach ($row['options'] as $option) {

			                     				if(empty($option['title']))
			                     				{
			                     					$option['title']=$option['sub_title'];
			                     				}
			                     				$getValue = ( !empty( $_GET['search'][ $row['columns'] ] ) AND $_GET['search'][ $row['columns'] ] == $option['id'] )? 'selected' : '';
			                     				$fliter .= '<option value="'. $option['id'] .'" '.$getValue.'>'. $option['title'] .'</option>';
			                     			}
		                     			}
		                     			else{
		                     				print '<script>'.
		                     				'document.querySelector("div.actions .btn-group.btn-group-devided").style.display = "none";'.
		                     				'alert("請先建立分類。");'.
		                     				'</script>'.
		                     					'';
		                     			}

		                    $fliter .= '
		                    		</select>
	                            </td>
							';
							break;
						case 'selectMulti':
							$fliter .= '
								<td>
		                            <select name="'.$set['modelName'].'['.$row['columns'].']" class="form-control form-filter input-sm" data-model="'.$set['modelName'].'" id="formFilter"  data-columns="'. $count .'">
		                                <option value=""> All </option>
		                     ';         
		                     			if( !empty( $row['options'] ) )
		                     			{
			                     			foreach ($row['options'] as $option) {
			                     				$getValue = ( !empty( $_GET['search'][ $row['columns'] ] ) AND $_GET['search'][ $row['columns'] ] == $option['id'] )? 'selected' : '';
			                     				$fliter .= '<option value="'. $option['id'] .'" '.$getValue.'>'. $option['title'] .'</option>';
			                     			}
		                     			}
		                     			else{
		                     				print '<script>'.
		                     				'document.querySelector("div.actions .btn-group.btn-group-devided").style.display = "none";'.
		                     				'alert("請先建立分類。");'.
		                     				'</script>'.
		                     					'';
		                     			}

		                    $fliter .= '
		                    		</select>
	                            </td>
							';
							break;
						default:
							$fliter .= '										
								<td>
									<input type="text" class="form-control form-filter input-sm" id="formFilter" name="'.$set['modelName'].'['.$row['columns'].']" value="'.$getValue.'" data-columns="'. $count .'">
								</td>';
							break;
					}
				}


			

				$count++;
			}
		}
		else{
			echo "tableSet is empty .";
			die;
		}
		//thead and fliter end

		$tbody = '';
		$dataCount = 0;
		foreach ($set['Datas']['data'] as $row) 
		{
			$tbody .='<tr>';
				$tbody .='
					<td>
						<input type="checkbox" name="ids[]"  id="ids"  value="'.$row['id'].'">
					</td>';
			foreach ($set['tableSet'] as $row2) 
			{

				if( !empty( $row2['group'] ) )
				{
					//icon有這些可以選擇
					$icon = ['label-danger','label-default','label-info','label-success','label-warning','data-original'];
					$tbody .='<td>';
					 	$iconCount = 0;
						foreach ($row2['group'] as $row3) {

							$showClass = ( $row[$row3['columns']] =='1' )? $row3['color'] : 'label-default';//如果這一個現在不是「是」，就反灰

							$tbody .='
								<input type="hidden" name="'.$row3['columns'].'['.$dataCount.']" value="'.$row[$row3['columns']].'">
								<div style="display:none;" id="'.$row3['columns'].'['.$dataCount.']">'.$row3['columns'].$row[$row3['columns']].'</div>
								<a 
									id="list_ajax_static"
									href="javascript:;" 
									class="label label-sm '.$showClass.' label-sm-icon tooltips" 
									data-container="body" 
									data-placement="top" 
									data-ajax="true"
									data-original-title="'. $row3['helpText'] .'"
									data-colortype = "'.$row3['color'].'"
									data-columns = "'.$row3['columns'].'"
									data-hiddenValue = "'.$row3['columns'].'['.$dataCount.']"
									data-model ="'. $set['model'] .'"
									data-parent = "'.$row['id'].'"
								>
									'.$row3['sub_title'].'
								</a>
							';
							$iconCount++;
						}
					$tbody .='</td>';
				}
				else
				{
					$columns = ( !empty( $row2['fliter'] ) )? $row2['fliter'] : 'text';

					$columnsValue = $row[ $row2['columns'] ];


					switch ($columns) {
						case 'select':
							$options = $row2['options'];

							$hasOption = false;
							foreach ($options as $option) {
								// var_dump('columns : '.$row[ $row2['columns'] ]);
								// var_dump('id : '.$option['id']);
								if( $row[ $row2['columns'] ] == $option['id'] )
								{
									if(empty($option['title']))
                     				{
                     					$option['title']=$option['sub_title'];
                     				}
									
									$tbody .= '<td>'.$option['title'].'</td>';
									$hasOption = true;
								}
							}

							if( $hasOption === false )
							{
								$tbody .= '<td></td>';
							}
							//$tbody .= '<td>'.$options[ $row[ $row2['columns'] ] ].'</td>';
							break;
						case 'selectMulti':
							$options = $row2['options'];

							$itemValue = json_decode( $row[ $row2['columns'] ] );
							$showOptions = [];

							foreach ($options as $option) {

								if( !empty($itemValue) AND in_array( (string)$option['id'], $itemValue) )
								{
									 array_push($showOptions, $option['title']);
								}
							}

							$showOptions = implode(",<br>", $showOptions);

							$tbody .= '<td>'.$showOptions.'</td>';
							break;
						case 'img':
							if( file_exists(public_path().$columnsValue) )
							{
								$tbody .= '<td><img width="100" src="'.$columnsValue.'"></td>';
							}
							else
							{
								$tbody .= '<td>&nbsp;</td>';
							}
							break;
						case 'image':
							// var_dump($columns);
							if( file_exists(public_path().$columnsValue) )
							{
								$tbody .= '<td><img width="100" src="'.$columnsValue.'"></td>';
							}
							else
							{
								$tbody .= '<td>&nbsp;</td>';
							}
							break;
						case '':
							if( $columnsValue != 'image' AND $columnsValue != 'img' )
							{
								$tbody .= '<td>&nbsp;</td>';
							}
							break;
						default:
							// if( !empty( $columnsValue ) AND $columnsValue != 0 )
							// {
								$tbody .= '<td>'.$columnsValue.'</td>';	
							// }
							break;
					}

				}

			}
			$tbody .= '
				<td>
				    <a href="'. MakeItemV2::url( 'Fantasy/'.$set['model'].'/edit/'.$row['id'] ) .'" class="tooltips edit " data-container="body" data-placement="top" data-original-title="修改"><span class="edit"> Edit </span> </a>

				      <span class="edit_line"> │ </span>

	                <a href="javascript:;" data-routename="'. $set['model'] .'" data-id="'. $row['id'] .'" id="dataDel" class="tooltips delet" data-container="body" data-placement="top" data-original-title="刪除"><span class="delet"> Delete</span> </a>
	            </td>
	        ';
	        $dataCount++;
			$tbody .='</tr>';
		}

        print('
        <div class="table-container">
			<a id="toColorbox" href="#basic" data-toggle="modal"></a><!-- 批次修改用 -->

            <div id="datatable_ajax_wrapper" class="dataTables_wrapper dataTables_extended_wrapper no-footer">
                '.self::listPageCount( [
                		"Datas" => $set['Datas']
                	] ).'

                <!-- ui-block-->
                <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
                    <div class="modal-dialog modal_style" id="show_ajax_edit">

                    </div>
                </div>
                <!--ui-block End-->

                <div class="table-responsive">
					<form id="dataContent" target="fancyBox" action="'.$ajaxLink.'" data-search="'.$searchLink.'" method="POST">
						<input type="hidden" name="_token" value="'.csrf_token().'">
	                    <table class="table table-striped table-bordered table-hover table-header-fixed dataTable no-footer" id="datatable_list" aria-describedby="datatable_ajax_info" role="grid">
	                        <thead >
	                            <tr role="row" class="heading ">
	                                <td width="2%" class="sorting_disabled list_table_td_check" rowspan="1" colspan="1" aria-label="
	                             ">
	                                    <input type="checkbox" id="idCheckAll" class="group-checkable">
	                                </td>
									'. $thead .'
	                                <td width="100"  class="sorting list_table_td_actions"> Actions </td>
	                            </tr>
	                        </thead>
	                        <thead id="searchThead">	                            
		                        <tr role="row" class="filter" >
		                                <td rowspan="1" colspan="1"> </td>
										'. $fliter .'
		                                <td rowspan="1" colspan="1">
		                                    <div class="margin-bottom-5 search_icon_left">
		                                        <a id="searchData" data-model="'.$set['modelName'].'" class="btn btn-sm green btn-outline filter-submit margin-bottom">
		                                            <i class="fa fa-search"></i>
		                                        </a>
		                                    </div>
		                                    <a class="btn btn-sm red btn-outline filter-cancel" href="'.MakeItemV2::url('Fantasy/'.$set['model']).'">
		                                        <i class="fa fa-times"></i>
		                                    </a>
		                                </td>
		                         </tr>
		                    </thead>
	                        <tbody>

	                        	'. $tbody .'
	                        </tbody>
	                    </table>
	                </form>

                    <iframe name="fancyBox" id="fancyBox" style="display:none;" ></iframe>
                </div>
                '.self::listPageCount( [
                		"Datas" => $set['Datas']
                	] ).'
<!--                <div class="row">
                     <div class="col-md-8 col-sm-12">
                        <div class="dataTables_paginate paging_bootstrap_extended" id="datatable_ajax_paginate">
                            <div class="pagination-panel"> Page <a href="#" class="btn btn-sm default prev disabled"><i class="fa fa-angle-left"></i></a>
                                <input type="text" class="pagination-panel-input form-control input-sm input-inline input-mini" maxlenght="5" style="text-align:center; margin: 0 5px;"><a href="#" class="btn btn-sm default next"><i class="fa fa-angle-right"></i></a> of <span class="pagination-panel-total">18</span></div>
                        </div>
                         
                        <div class="dataTables_info" id="datatable_ajax_info" role="status" aria-live="polite"><span class="seperator">,</span>found total <span class="font_bold">178</span> records</div>
                    </div>

                    <div class="pages_right">

<div class="dataTables_length" id="datatable_ajax_length">
                            <label>View <span class="seperator"></span> Records　
                            
                                <select name="datatable_ajax_length" aria-controls="datatable_ajax" class="form-control input-xs input-sm input-inline">
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="150">150</option>
                                    <option value="-1">All</option>
                                </select></label>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-12"></div>
                </div>-->
            </div>
        </div>');
	}
	//後台列表table

	protected static function listPageCount( $set = [] )
	{
		$html = '';

		$per_page = ['All','10','20','50','100','150'];

		$per_page_select = '';
		
		$now_page = ( isset( $_GET['page'] ) )? $_GET['page'] : '1';

		foreach ($per_page as $row) 
		{
			$select = ( !empty( $set['Datas']['per_page'] ) AND $set['Datas']['per_page'] == $row )?"selected": '';
			if( $row =='All' )
			{
				$per_page_select .='<option value="" '.$select.'>'.$row.'</option>';
			}
			else
			{
				$per_page_select .='<option value="'.$row.'" '.$select.'>'.$row.'</option>';
			}
			
		}

		$next_link = ( !empty( $set['Datas']['next_page_url'] ) )? $set['Datas']['next_page_url'] : 'javascript:;';
		$prev_link = ( !empty( $set['Datas']['prev_page_url'] ) )? $set['Datas']['prev_page_url'] : 'javascript:;';

		$html .= '
                <div class="row">
                     <div class="col-md-8 col-sm-12">
                        <div class="dataTables_paginate paging_bootstrap_extended" id="datatable_ajax_paginate">
                            <div class="pagination-panel"> 
                            	Page 
                            	<a href="'.$prev_link.'" class="btn btn-sm default prev "><i class="fa fa-angle-left"></i></a>
                                <input type="text" name="to_page" class="pagination-panel-input form-control input-sm input-inline input-mini" style="text-align:center; margin: 0 5px;" value="'.$now_page.'">
                                <a href="'.$next_link.'" class="btn btn-sm default next">
                                	<i class="fa fa-angle-right"></i>
                                </a> 
                                of 
                                <span class="pagination-panel-total">'.$set['Datas']['last_page'].'</span>
                            </div>
                        </div>
                         
                        <div class="dataTables_info" id="datatable_ajax_info" role="status" aria-live="polite">
                        	<span class="seperator">,</span>found total 
                        	<span class="font_bold">'.$set['Datas']['total'].'</span> records
                        </div>
                </div>

                    <div class="pages_right">

						<div class="dataTables_length" id="datatable_ajax_length">
                            <label>View <span class="seperator"></span> Records　
                            
                                <select name="per_page_count" aria-controls="datatable_ajax" class="form-control input-xs input-sm input-inline">
									'.$per_page_select.'
                                </select>
                            </label>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-12"></div>
                </div>
            
		';
		return $html;
	}

	/*schema html表格*/
	public static function schematable()
	{	
		/*抓menulist*/
		$backend = file_get_contents(base_path().'/app/Http/Controllers/Fantasy/BackendController.php');
		/*排除抓到辨識字串 頭*/
		$start = strpos($backend,'MenuStart')+strlen('MenuStart');
		/*排除抓到辨識字串 尾*/
		$end = strpos($backend,'//MenuEnd',$start);

		$lengh = ($end-$start);

		$menuArea = substr($backend,$start,$lengh);
			
		/***********************************************************/
		/*抓modelarray*/
		/*排除抓到辨識字串 頭*/
		$start = strpos($backend,'ModelStart')+strlen('ModelStart');
		/*排除抓到辨識字串 尾*/
		$end = strpos($backend,'//ModelEnd',$start);

		$lengh = ($end-$start);

		$modelArea = substr($backend,$start,$lengh);
		/***********************************************************/
		/*form表頭 + token*/
		$table ='<div><form id="tableForm" method="get" action="'. MakeItemV2::url('Fantasy/schema/build') .'" >
		<input type="hidden" name="_token" value="'.csrf_token().'">';
		echo '<td width="70%"><h3>ModelArray</h3></td>
		<hr>';
		/************************************************************/
		/*產出menulist textarea*/
		$menuArea ='<div><textarea style="height:200px;width:650px;" name="backend" >'. $menuArea .' </textarea></div><hr>';
		echo $menuArea;

		/*產出modelarray textarea*/
		$modelArea ='<div><textarea style="height:200px;width:650px;" name="backend" >'. $modelArea .' </textarea></div><hr>';
		echo $modelArea;
		/**********************************************************/

		echo '<td width="70%"><h3>建立Table</h3></td><input id="addTable" type="button" value="增加Table"><hr>';

		$table .= '<tr>
			<div>
				<td >*Table 名稱</td>
				<tr>
					<input type="text" name="tableName" value="" required>
					<h4>Ex: csr_financal 自動抓語系 => 變成en_csr_financal</h4>
				</tr>
				<br><hr>
				<td >*MenuList Master</td>
				<tr>
					<input type="text" name="masterTitle" value="" >
					<h4>Ex: 後台列表Title 1名稱</h4>
					<h4>要建立view 就必須要填</h4>
				</tr>
				<td >*MenuList Slaver</td>
				<tr>
					<input type="text" name="slaverTitle" value="" >
					<h4>Ex: 後台列表Title 2名稱</h4>
					<h4>要建立view 就必須要填</h4>
				</tr>
			</div>
			<hr>
			<div>
				<td>*存放路徑</td>
				<td>
					<input type="text" name="forderName" value="" required><br>
					controller   路徑-> /app/Http/Controllers/<font color="red">XXX</font>/ <br>
					model     路徑-> /app/Http/Models/<font color="red">XXX</font>/ <br> 
					view    路徑->  /public/views/Fantasy/<font color="red">XXX</font>/

					<h3 style="color:red;">** 參數共用 & 用於判斷Route是否是附屬 字串要相同</h3>
					</h3>
				</td>
			</div>
			<hr>
			<div>
				<td>*檔案名稱</td>
				<td>
					<input type="text" name="fileName" value="" required><br>Ex:<br>
					 Controller = <font color="blue">OOO</font>Controller.php <br> Model = <font color="blue">OOO</font>.php <br> View = 放在<font color="red">XXX</font>/<font color="blue">OOO</font>資料夾裡面
				</td>
			</div>
			<hr>
			<div id="tableTd">
				<td>*Table 欄位<input id="addField" type="button" value="增加欄位"></td>
				<div id="fieldInput">
					<input type="text" name="fieldName[]" value="" required>=><input type="text" name="typeName[]" value="" required>
				</div>
				** 欄位名稱=>型態
				<br>
				<br>
				已經預設<br>id<br>rank(0)<br>is_visible(1)<br>created_at<br>updated_at 
			</dv></tr>
			<hr>
			<div >
				選擇建立檔案
				<input type="checkbox" name="check[]" value="c" >Controller
  				<input type="checkbox" name="check[]" value="m" required>Model
  				<input type="checkbox" name="check[]" value="v" >Views<br>
  				**至少要有Model
			</duv>
			';
		$table .= '<input type="submit" value="Submit"></form></div><hr>';
		
		echo $table;
		echo '<br><div id="Box1" ondrop="Drop(event)" ondragover="AllowDrop(event)"></div>
			  <img id="Img1" src="/assets/img/icon-dis.png" draggable="true" ondragstart="Drag(event)">';
		echo '<a href="javascript:;" id="atage" draggable="true" ondragstart="Drag(event)"></a>';
	
	}

}
