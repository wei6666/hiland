<?php

namespace App\Http\Controllers\Fantasy\Style;

use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
/**相關Models**/




class StyleLightController extends CRUDBaseController
{

    public $ajaxEditLink = 'Fantasy/StyleLight/ajax-list/';

    protected $modelName = "StylePhoto";

    public $index_select_field = ['id','rank','is_visible'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'Style/StyleLight';

    public $viewPreFix = 'StyleLight';

   

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );


    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'rank',
        'sort' => 'asc'
    ];


// public $modelBelongs = [

       
//         "Product" => [
//             "parent" => "id",
//             "filed" => 'id',
//             "select" => ['title','id']
//         ]
//     ];


      public $modelBelongs = [

        "Product" => [
            "parent" => "id",
            "filed" => 'id',
            "select" => ['pro_num','id']
        ],
        
    ];


  
}
