<?php

namespace App\Http\Controllers\Fantasy\Style;

use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
/**相關Models**/

use App\Http\Models\Banner;


class StyleController extends CRUDBaseController
{

    public $ajaxEditLink = 'Fantasy/Style/黃山石風格管理/ajax-list/';

    protected $modelName = "Style";

    public $index_select_field = ['id','rank','is_visible','out_title'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'Style/黃山石風格管理';

    public $viewPreFix = 'Style';

   

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );


    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'rank',
        'sort' => 'asc'
    ];

     public $modelHas = [

       "StylePhoto" => [
            "modelName" => "StylePhoto",
            "storedName" => "StylePhoto", 
            "parent" => "style_id",
            "filed" => 'id',
            "select" => ['id','rank','is_visible','image','style_id','go_news_content'],
            
            ],
       
    ];

       public $saveSubData = [
                [   // 產品介紹的多圖
                    "modelName" => "StylePhoto",
                    "requestModelName" => "StylePhoto", //多圖的inputName
                    "to_parent" => "style_id",

                    
                ],

              
                
            ];

      public $photoTable = [
            'photo' => [
                "排序" => "rank",
                "圖片(1200 x 750)" => "image",
                "狀態" => "is_visible",
                "編輯"=>"go_news_content",
            ],
            
        ];    





       
}
