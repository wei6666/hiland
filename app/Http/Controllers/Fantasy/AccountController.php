<?php

namespace App\Http\Controllers\Fantasy;

/**原生函式**/
use Illuminate\Http\Request;
use View;
use Config;
use Session;
use Route;
use App;
use Validator;
use Debugbar;
use Excel;
use App\Http\Controllers\Fantasy\MakeItemV2;
/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;

/**Models**/
use App\Http\Models\User;
use App\Http\Models\Role;

class AccountController extends BackendController
{

    //批次修改路徑
    protected static $ajaxEditLink = 'Fantasy/帳號管理/帳號列表/ajax-list/';


    //批次修改顯示及編輯的欄位設定
    public static $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "姓名" => Array(
            "field" => "name",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "帳號" => Array(
            "field" => "email",
            "inputType" => "text",
            "is_edit"=> false
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-danger',
                "showText" => 'S'
            )
        ),
    );
    public function __construct()
    {
        parent::__construct();

        $Permission = [];
		$Role = Role::where('is_visible','=','1')->get()->toArray();
        $count = 0;

		foreach ($Role as $row) {
			//$Permission[$row['id']] = $row['title'];
            $Permission[$count]['id'] = $row['id'];
            $Permission[$count]['title'] = $row['title'];

            $count++;
		}

        // $this->var_dump_pre($Permission);
        // echo "tes";
        // die;
		view::share('Permission',$Permission);
        //系統訊息
        if(!empty(Session::get('Message')))
        {
            View::share('Message',Session::get('Message'));
        }else{
            View::share('Message','');
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        $Datas = [];

        $Datas = parent::findDataAndAssociate([
            "modelName" => 'Account',
            "select" => ['id','is_visible','name','email','character'],
        ]);

        foreach ($Datas['data'] as $key => $value)
        {
            if( $value['character'] == 'superAdminer' )
            {
                unset($Datas['data'][$key]);
            }
        }
        //Debugbar::info( $Datas );

        return view('Fantasy.Account.index',[
            "Datas" => $Datas,
            "ajaxEditLink" => self::$ajaxEditLink
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        return view('Fantasy.Account.edit',
            [
                'data'=>[],
                'actionUrl' => MakeItemV2::url('Fantasy/帳號管理/帳號列表/store')
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
        $Datas = $request->input('Account');
        $Datas['password'] = bcrypt($Datas['password']);

        $resoult = json_decode(parent::addNew([
            "Datas" => $Datas ,
            "modelName" => 'Account',
            "routeFix" => '帳號管理/帳號列表'
            ]), true );

        if( !empty( $resoult['redirect'] ) )
        {
            return redirect( $resoult['redirect'] )->with('Message','新增成功, 轉到編輯頁面');

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($locale, $id)
    {
        $Data = User::where('id',$id)->get()->first()->toArray();
        $Data['character'] = json_decode($Data['character'],true);
        //Debugbar::info($Data);
        return view('Fantasy.Account.edit',[
                'data' => $Data,
                'actionUrl' => MakeItemV2::url('Fantasy/帳號管理/帳號列表/update'),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postUpdate(Request $request)
    {
        if(!empty($request->input('method')) AND $request->input('method')=='ajaxEdit')
        {
            //echo "批次修改<br>";
            parent::updateOne( $request->input('Account'), 'Account', 'ajaxEdit');
        }
        else
        {
            $Datas = $request->input('Account');
            print_r($Datas);

            if( !empty( $Datas['password'] ) )
            {
                $Datas['password'] = bcrypt($Datas['password']);
            }
            else
            {
                unset( $Datas['password'] );
            }

            $Datas['character'] = json_encode($Datas['character']);
            //Debugbar::info($Datas['character']);
            //Debugbar::info(json_decode($Datas['character'], true));

            if( parent::updateOne( $Datas, 'Account', '') )
            {
                return redirect( MakeItemV2::url('Fantasy/帳號管理/帳號列表/edit/'.$Datas['id']) )->with('Message','修改成功');
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postDestroy(Request $request)
    {
        $id = $request->input('id');

        $method = ( !empty( $request->input('method') ) )? $request->input('method') : '';
        //$method =  $request->input('method');

        if( empty( $method ) )
        {
            parent::deleteOne( 'Account', $id );
        }
        else
        {
            if( !empty( $id ) AND count( $id ) > 0 )
            {
                foreach ($id as $row) {
                    parent::deleteOne($method, $row );
                }
            }
        }

    }
    /*==============jQuery Ajax ====================*/
    //批次修改
    public static function postAjaxList(Request $request)
    {
        $ids = $request->input("ids");
        $works = Array();
            foreach ($ids as $row) {
                $works[] = User::where('id','=',$row)
                            ->select('id','name', 'is_visible','email')
                            ->get();
            }
        return view('Fantasy.Ajax.list')
                    ->with('ajaxEditList',self::$ajaxEditList)
                    ->with('modal','Account')
                    ->with('update_link', MakeItemV2::url('Fantasy/帳號管理/帳號列表/update'))
                    ->with('datas',$works);
    }

    public function postChangeStatic(Request $request)
    {
        $datas = $request->all();

        $res = parent::updataOneColumns([
                'modelName' => 'Account',
                'id' => $datas['id'],
                'columns' => $datas['columns'],
                'value' => $datas['value']
            ]);

        return $res;

    }

    /*===============後台結束=======================*/

}
