<?php

namespace App\Http\Controllers\Fantasy\Home;

use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
/**相關Models**/

use App\Http\Models\Banner;


class HomeController extends CRUDBaseController
{

    public $ajaxEditLink = 'Fantasy/首頁管理/ajax-list/';

    protected $modelName = "Home";

    public $index_select_field = ['id','rank','is_visible'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = '首頁管理';

    public $viewPreFix = 'Home';

   

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );


    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'rank',
        'sort' => 'asc'
    ];

     public $modelHas = [

         "HomeImage" => [
            "modelName" => "HomeImage",
            "storedName" => "HomeImage", 
            "parent" => "home_id",
            "filed" => 'id',
            "select" => ['id','rank','is_visible',"image","title","sub_title",'color'],
            
            ],
       
    ];

       public $saveSubData = [
                
                [   // 產品介紹的多圖
                    "modelName" => "HomeImage",
                    "requestModelName" => "HomeImage", //多圖的inputName
                    "to_parent" => "home_id",

                    
                ],

              
                
            ];

      public $photoTable = [
            
            'HomeImage' => [
                "排序" => "rank",
                "圖片(2150 x 1000)" => "image",
                "圖片標題" => "title",
                "圖片副標題" => "sub_title",
                "字體顏色" => "color",
                "狀態" => "is_visible",
                
                
            ],
            
        ];    





       
}
