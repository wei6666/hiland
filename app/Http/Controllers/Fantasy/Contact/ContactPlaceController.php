<?php
namespace App\Http\Controllers\Fantasy\Contact;

use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
/**相關Models**/

use App\Http\Models\Banner;


class ContactPlaceController extends CRUDBaseController
{

    public $ajaxEditLink = 'Fantasy/Contact/各縣市選單管理/ajax-list/';

    protected $modelName = "ContactCountry";

    public $index_select_field = ['id','rank','is_visible','title'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'Contact/各縣市選單管理';

    public $viewPreFix = 'ContactCountry';

   

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );


    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'rank',
        'sort' => 'asc'
    ];

        public $modelHas = [

         "ContactArea" => [
            "modelName" => "ContactArea",
            "storedName" => "ContactArea", 
            "parent" => "country_id",
            "filed" => 'id',
             "select" => ['id','rank','is_visible','title'],
            
            ],
    ];

       public $saveSubData = [
             

                 [   // 產品介紹的多圖
                    "modelName" => "ContactArea",
                    "requestModelName" => "ContactArea", //多圖的inputName
                    "to_parent" => "country_id",

                    
                ],
                
            ];

      public $photoTable = [
          
            'ContactArea' => [
                "排序" => "rank",
                "標題" => "title",              
                "狀態" => "is_visible"  
            ],
        ];    





       
}
