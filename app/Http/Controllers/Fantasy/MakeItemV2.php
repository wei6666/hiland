<?php namespace App\Http\Controllers\Fantasy;

use Illuminate\Routing\Controller as BaseController;

/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;

use Config;
use App;
use Debugbar;

class MakeItemV2 extends BackendController
{

	//id hidden input
	public static function idInput( $set = []){

		$value = ( !empty( $set['value'] ) )? $set['value'] : '';

		print('
            <input type="hidden" name="'.$set['inputName'].'" value="'. $value .'" >
		');

	}


	//input
	public static function textInput( $set = []){

		$labelText = (empty($set['labelText'])) ? '預設Label Text' : $set['labelText'];

		$helpBlock = (!empty($set['helpText']))? $set['helpText'] : '';

		$disabled = ( !empty($set['disabled']) AND $set['disabled'] == 'disabled' )? 'disabled="disabled"' : '';

		$required = ( !empty( $set['required'] ) AND $set['required'] )? '<span class="required"> * </span>' : '';

		$value = $set['value'];

		print('
            <div class="form-group">
                <label class="col-md-2 control-label">'.$labelText.'
                    '. $required .'
                </label>
                <div class="col-md-10">
                    <input
                    	type="text"
                    	class="form-control tooltips"
                    	name="'.$set['inputName'].'"
                    	placeholder=""
                    	data-container="body"
                    	data-placement="top"
                    	data-original-title="'.$helpBlock.'"
                    	'.$disabled.'
                    	value="'. $value .'"
                    >
                </div>
            </div>
		');

	}

	//pass word input
	public static function passwordInput($set = []){

		$labelText = (empty($set['labelText'])) ? '預設Label Text' : $set['labelText'];

		$helpBlock = (!empty($set['helpText']))? $set['helpText'] : '';

		$disabled = ( !empty($set['disabled']) AND $set['disabled'] == 'disabled' )? 'disabled="disabled"' : '';

		$required = ( !empty( $set['required'] ) AND $set['required'] )? '<span class="required"> * </span>' : '';

		print('
            <div class="form-group">
                <label class="col-md-2 control-label">'.$labelText.'
                    '. $required .'
                </label>
                <div class="col-md-10">
                    <input
                    	type="password"
                    	class="form-control tooltips"
                    	name="'.$set['inputName'].'"
                    	placeholder=""
                    	data-container="body"
                    	data-placement="top"
                    	data-original-title="'.$helpBlock.'"
                    >
                </div>
            </div>
		');

	}

	//radio
	/* $Options 必須傳入陣列，EXP: Array("否","是") */

	public static function radio_color( $set = [] )
	{
		$showClass = ( !empty($set['showColor']) AND ( !empty($set['value']) AND $set['value'] == '1' ) )? $set['showColor'] : 'label-default';//如果這一個現在不是「是」，就反灰

		$helpBlock = (!empty($set['helpText']))? $set['helpText'] : '';

		$buttonID = ( $set['ajax'] == false )? 'EditStatic' : 'list_ajax_static';

		$html = '';

		$ajax = '';
		if( $set['ajax'] == true )
		{
			$ajax .= '
				data-model ="'. $set['model'] .'"
				data-parent = "'.$set['id'].'"
			';
		}

		$html .='
			<input type="hidden" name="'. $set['inputName'] .'" value="'.$set['value'].'">
			<a
				id="'.$buttonID.'"
				href="javascript:;"
				class="label label-sm '.$showClass.' label-sm-icon tooltips"
				data-container="body"
				data-placement="top"
				data-original-title="'. $helpBlock .'"
				data-colortype = "'.$set['showColor'].'"
				data-field = "'.$set['inputName'].'"
				'.$ajax.'
			>
				'.$set['showText'].'
			</a>
		';
		return $html;
	}

	public static function radio_btn( $set = [] )
	{

		$labelText = (empty($set['labelText'])) ? '預設Label Text' : $set['labelText'];

		$helpBlock = (!empty($set['helpText']))? $set['helpText'] : '';

		$disabled = ( !empty($set['disabled']) AND $set['disabled'] == 'disabled' )? 'disabled="disabled"' : '';

		$required = ( !empty( $set['required'] ) AND $set['required'] )? '<span class="required"> * </span>' : '';

		$value = ( !empty( $set['value'] ) )? $set['value']: '';

		$thisValue = ($value == '1')? 'blue': 'default';

		//$showText = ( !empty( $set['options'] ) AND !empty( $value ) )?  $set['options'][ $value ] : $set['options'][0];

		$showText = ( $value == '1' )? 'check' : 'close';

		$btn = '
			<input type="hidden" name="'.$set['inputName'].'" value="'.$value.'" >

			<a id="EditStatic" href="javascript:;" data-field="'.$set['inputName'].'"  class="btn '.$thisValue.'  tooltips switch_radio" placeholder=""
                    	data-container="body"
                    	data-placement="top"
                    	data-original-title="'.$helpBlock.'">
                <i class="fa fa-'.$showText.'"></i>
			</a>
		';


		// $btn = '
		// 	<input type="hidden" name="'.$set['inputName'].'" value="'.$value.'" >

		// 	<a id="EditStatic" href="javascript:;" data-field="'.$set['inputName'].'" data-showText="'.implode(",",$set['options']).'" class="btn '.$thisValue.'  tooltips switch_radio" placeholder=""
  //                   	data-container="body"
  //                   	data-placement="top"
  //                   	data-original-title="'.$helpBlock.'">
  //               <i class="fa '.$showText.'"></i>
		// 	</a>
		// ';
		if( !empty( $set['type'] )  AND $set['type'] == 'table' )
		{
			return $btn;
		}
		else
		{
			print('
	            <div class="form-group">
	                <label class="col-md-2 control-label">'.$labelText.'
	                    '. $required .'
	                </label>
	                <div class="col-md-10">
						'.$btn.'
	                </div>
	            </div>
			');
		}

	}

	public static function radio( $set = [] ){

			$labelText = (empty($set['labelText'])) ? '預設Label Text' : $set['labelText'];

			$helpBlock = (!empty($set['helpText']))? '<span class="help-block">'.$set['helpText'].'</span>' : '<span class="help-block"></span>';


/*			if ($set['value'] == '1') {
				$checked = 'checked="checked" ';
			}else{
				$checked = '';
			}*/
			$radio = '';
			if( !empty( $set['options'] ) )
			{
				foreach ($set['options'] as $key => $value) {
					$checked = ( $set['value'] == $key )? 'checked="checked" ' : '';
					$radio .= '<input type="radio" class="manainput" name="'.$set['inputName'].'" value="'.$key.'" '.$checked.'>'.$value.' ';
				}
			}

			//$radio = '<input type="checkbox" class="make-switch" data-name="'.$set['inputName'].'"  value="1" '.$checked.'> ';

			if( !empty($set['type']) AND  $set['type'] == 'table')
			{
				return $radio;
			}
			else
			{
				print('
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                        	'.$labelText.'
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-10">
                            '.$radio.''.$helpBlock.'
                        </div>
                    </div>

				');
/*				print('
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                        	'.$labelText.'
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-10">
                            '.$radio.''.$helpBlock.'
                            <input type="hidden" name="'.$set['inputName'].'" value="'.$set['value'].'" >
                        </div>
                    </div>

				');*/
			}

	}

	//下拉選單
	public static function select( $set = [] ){
		if (is_array($set['options'])) {

			$labelText = (empty($set['labelText'])) ? '預設Label Text' : $set['labelText'];

			$helpBlock = (!empty($set['helpText']))? $set['helpText'] : '';

			$required = ( !empty( $set['required'] ) AND $set['required'] )? '<span class="required"> * </span>' : '';

			$multiple = ( !empty($set['multiple']) AND $set['multiple'] )? 'multiple="multiple"' : '';

			$disabled = ( !empty($set['disabled']) AND $set['disabled'] == true )?  true : false;

			$options = '';
				foreach ($set['options'] as $row)
				{
					//如果有下一層
					if (isset($row['children']) AND !empty($row['children']))
					{
						foreach ($row['children'] as $row2) {

							//如果有第三層
							if (isset($row2['children']) AND !empty($row2['children']))
							{
								foreach ($row['children'] as $row2)
								{
									$options .='<optgroup label=" '.$row['title'].' / '.$row2['title'].'">';
										foreach ($row2['children'] as $row3)
										{
											$select = ($row3['id']==$set['value'])? 'selected': '';

											$options .='<option value="'.$row3['id'].'" '.$select.' >'.$row3['title'].'</option>';
										}
									$options .='</optgroup>';

								}
							}else
							{
								$options .='<optgroup label=" '.$row['title'].' ">';

									$select = ($row2['id']==$set['value'])? 'selected': '';
									$options .='<option value="'.$row2['id'].'" '.$select.' >'.$row2['title'].'</option>';

								$options .='</optgroup>';
							}
						}
					}else
					{

						//dd($row);
						//為了給沒有title的資料表所做的更動
						if(empty($row['title']))
							{
								
								
								if(empty($row['sub_title']))
								{
									$row['title'] = $row['pro_num'];
								}
								else
								{
									$row['title'] = $row['sub_title'];
								}
							}
						$select = ($row['id']==$set['value'])? 'selected': '';

						$options .='<option value="'.$row['id'].'" '.$select.' >'.$row['title'].'</option>';

						if( $row['id'] == $set['value'] )
						{
							$selectedText = $row['title'];
						}
					}

				}
			if( $disabled === true )
			{
				$selectedText = ( !empty($selectedText) ) ? $selectedText : '';
				print('
		            <div class="form-group">
		                <label class="col-md-2 control-label">'.$labelText.'
		                    '. $required .'
		                </label>
		                <div class="col-md-10">
		                    <input
		                    	type="text"
		                    	class="form-control tooltips"
		                    	placeholder=""
		                    	data-container="body"
		                    	data-placement="top"
		                    	data-original-title="'.$helpBlock.'"
		                    	'.'disabled="disabled"'.'
		                    	value="'. $selectedText .'"
		                    >
		                </div>
		            </div>
				');
			}
			else
			{
				print('
		            <div class="form-group">
		                <label class="col-md-2 control-label">'.$labelText.'
		                    '. $required .'
		                </label>
		                <div class="col-md-10">
		                	<select class="form-control select2 tooltips" name="'.$set['inputName'].'" data-container="body"  data-placement="top" data-original-title="'.$helpBlock.'" '.$multiple.' >
		                		<option value="">Select...</option>
		                		'.$options.'
		                	</select>

		                </div>
		            </div>
				');
			}
		}else
		{
			echo "Options is not array.";
		}
	}
	//level 1
	public static function selectMulti($set = []){
		//$labelText, $inputName, $inputID, $Options, $inputValue, $helpText
		$labelText = (empty($set['labelText'])) ? '預設Label Text' : $set['labelText'];

		$helpBlock = (!empty($set['helpText']))? $set['helpText'] : '';

		$disabled = ( !empty($set['disabled']) AND $set['disabled'] == 'disabled' )? 'disabled="disabled"' : '';

		$required = ( !empty( $set['required'] ) AND $set['required'] )? '<span class="required"> * </span>' : '';

		$Options = ( !empty( $set['options'] ) )? $set['options'] : [];

		$inputValue = ( !empty( $set['value'] ) )? $set['value'] : '';

		$type = ( !empty( $set['type'] ) )? $set['type'] :'';

		if (is_array($Options) ) {

			$html = '';
			//var_dump($Options);
			if( !empty( $type ) AND $type == 'Role' )
			{
				foreach ($Options as $key => $value)
				{
						if( is_array( $value ) )
						{
							$html .='<optgroup label="' .$key. '">';
								foreach ($value as $key2 => $value2)
								{
									$select = '';
									if( !empty( $inputValue ) )
									{
										if( in_array($value2, $inputValue) )
										{
											$select = 'selected';
										}
									}
									//$select = ( !empty( $inputValue )  AND in_array($value2, $inputValue) )? 'selected' : '';
									$html.='<option value="'. $value2 .'" '.$select.'>'.$key2.'</option>';
								}
							$html .='</optgroup>';
						}
						else
						{
							$select = '';
							if( !empty( $inputValue ) )
							{
								if( in_array($value, $inputValue) )
								{
									$select = 'selected';
								}
							}
							//$select = ( in_array($value, $inputValue) AND !empty( $inputValue ) )? 'selected' : '';
							$html.='<option value="'. $value .'" '.$select.'>'.$key.'</option>';
						}


				}
			}
			else
			{
				foreach ($Options as $row)
				{
					//Debugbar::info($inputValue);
					//Debugbar::info($row['id']);
					//如果沒有傳值
					if (!empty($inputValue) AND isset($inputValue))
					{
						if( is_string($inputValue) )
						{
								$inputValue = json_decode($inputValue);
						}

						$select = ( in_array($row['id'], $inputValue) )? 'selected' : '';

						$html.='<option value="'. (int)$row['id'] .'" '.$select.'>'.$row['title'].'</option>';
					}
					else
					{
						$html.='<option value="'. (int)$row['id'] .'" >'.$row['title'].'</option>';
					}

				}

			}

			print('
	            <div class="form-group">
	                <label class="col-md-2 control-label">'.$labelText.'
	                    '. $required .'
	                </label>
	                <div class="col-md-10">
	                	<select class="form-control select2 tooltips" name="'.$set['inputName'].'" data-container="body"  data-placement="top" data-original-title="'.$helpBlock.'" multiple>
	                		'.$html.'
	                	</select>

	                </div>
	            </div>
			');
			// print('
			// 		<div class="form-group">
			// 			<label class="control-label col-md-3">'.$labelText.'</label>
			// 			<div class="col-md-4">
			// 				<select id="select2_sample2" class="form-control select2" multiple name="'.$set['inputName'].'">
			// 				'.$html.'
			// 				</select>
			// 			</div>
			// 		</div>
			// ');
		}else{
			echo "Options is not array.";
		}

	}

	//文字區塊
	public static function textArea( $set = [] )
	{

		$labelText = (empty($set['labelText'])) ? '預設Label Text' : $set['labelText'];

		$helpBlock = (!empty($set['helpText']))? $set['helpText'] : '';

		$disabled = ( !empty($set['disabled']) AND $set['disabled'] == 'disabled' )? 'disabled="disabled"' : '';

		$required = ( !empty( $set['required'] ) AND $set['required'] )? '<span class="required"> * </span>' : '';

		$value = ( !empty( $set['value'] ) )? $set['value'] : '';

		if ( !empty( $set['type'] ) AND $set['type'] == 'table')
		{
			return '<textarea name="'.$set['inputName'].'" class="form-control manainput"' .$disabled. '>'.$value.'</textarea>';
		}
		else
		{
			print('
                <div class="form-group">
                    <label class="col-md-2 control-label">
                    	'. $labelText .'
                        '. $required .'
                    </label>
                    <div class="col-md-10">
                        <textarea
                        	name="'.$set['inputName'].'"
                        	data-provide="markdown"
                        	rows="10"
                        	class="tooltips  form-control"
                        	data-container="body"
                        	data-placement="top"
                        	data-original-title="'.$helpBlock.'"
                        	'.$disabled.'
                        >'.$value.'</textarea>
                    </div>
                </div>
			');
		}


	}

	//photo
	public static function photo($set = []){

		$img = (!empty($set['value']))? '<img id="showPic" src="'.$set['value'].'" style="width : 200px; height: auto;" alt=""/>' : '<img id="showPic" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" style="width : 200px!important; height: auto;" alt=""/>';

		$labelText = (empty($set['labelText'])) ? '預設Label Text' : $set['labelText'];

		$helpBlock = (!empty($set['helpText']))? '<span class="label label-danger">NOTE!</span> '.$set['helpText'] : '';

		$disabled = ( !empty($set['disabled']) AND $set['disabled'] == 'disabled' )? 'disabled="disabled"' : '';

		$required = ( !empty( $set['required'] ) AND $set['required'] )? '<span class="required"> * </span>' : '';

		$value = ( !empty( $set['value'] ) )? $set['value'] : '';


		if ( !empty($set['type']) AND $set['type'] ==='table')
		{

			return '
			        <a href="'.$value.'" class="fancybox-button">
			            '.$img.'
			            <input type="hidden" name="'.$set['inputName'].'" id="filePicker" value="'.$value.'" >
			        </a>
			        <div class="images_icon">
			            <a id="filePicker" data-name="'.$set['inputName'].'" href="javascript:;" class="label label-sm label-success tooltips" data-container="body" data-placement="top" data-original-title="編輯圖片" data-rel="fancybox-button">
			                <i class="fa fa-pencil"></i>
			            </a>
			        </div>
			        <div class="images_icon2">
			            <a id=" " href="javascript:;" class="label label-sm label-info tooltips" data-container="body" data-placement="top" data-original-title="複製路徑" data-rel="fancybox-button">
			                <i class="fa fa-chain"></i>
			            </a>
			        </div>
			';

		}else
		{

/*			print('
	            <div class="form-group">
	                <label class="col-md-2 control-label">'.$labelText.'
	                    '. $required .'

	                </label>
	                <div class="col-md-10">
	                	<div >
					        <a href="'.$value.'" class="fancybox-button">
					            '.$img.'
					            <input type="hidden" name="'.$set['inputName'].'" id="filePicker" value="'.$value.'" >
					        </a>
					        <div class="images_icon">
					            <a id="filePicker" data-name="'.$set['inputName'].'" href="javascript:;" class="label label-sm label-success tooltips" data-container="body" data-placement="top" data-original-title="編輯圖片" data-rel="fancybox-button">
					                <i class="fa fa-pencil"></i>
					            </a>
					        </div>
					        <div class="images_icon2">
					            <a id=" " href="javascript:;" class="label label-sm label-info tooltips" data-container="body" data-placement="top" data-original-title="複製路徑" data-rel="fancybox-button">
					                <i class="fa fa-chain"></i> </a>
					        </div>

						</div>
	                </div>
	            </div>
			');	*/
			print('
	            <div class="form-group">
	                <label class="col-md-2 control-label">'.$labelText.'
	                    '. $required .'

	                </label>

                    <div class="col-md-10">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail show_big_image" id="showPic_div" style="width: 400px; height: 200px;">
                                '.$img.'
                            </div>
                            <div>
                            	<a href="javascript:;" id="filePicker" data-name="'.$set['inputName'].'">
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new"> Select image </span>
                                        <input type="hidden" name="'.$set['inputName'].'"  value="'.$value.'">
                                    </span>
                                </a>
                                <!--<a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>-->
                            </div>
                        </div>
                        <div class="clearfix margin-top-10">

                            '.$helpBlock.'
                        </div>
                    </div>


	            </div>
			');
		}

	}



	/*
	{{ItemMaker::anchorPhoto([
        'labelText' => '照片',
        'inputName' => 'Banner[anchorImage]',
        'value' => ( !empty($data['anchorImage']) )? $data['anchorImage'] : '',
        'leftPropName' => 'Banner[leftProp]',   (與左邊界的距離 > 要存起來的欄位名稱)
        'topPropName' => 'Banner[topProp]'     (與上邊界的距離 > 要存起來的欄位名稱)
    ])}}
	 */
	public static function anchorPhoto($set = []) {

		$img = (!empty($set['value']))? '<div class="cover">'.'<img id="showPic" class="anchorImg" src="'.$set['value'].'"  alt="">'.'</div>' : '<div class="cover">'.'<img id="showPic" class="anchorImg" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"  alt="">'.'</div>' ;

		$anchorPoint = '<div class="anchorPoint"></div>';

		$img .= $anchorPoint;

		$labelText = (empty($set['labelText'])) ? '預設Label Text' : $set['labelText'];

		$helpBlock = (!empty($set['helpText']))? '<span class="label label-danger">NOTE!</span> '.$set['helpText'] : '';

		$disabled = ( !empty($set['disabled']) AND $set['disabled'] == 'disabled' )? 'disabled="disabled"' : '';

		$required = ( !empty( $set['required'] ) AND $set['required'] )? '<span class="required"> * </span>' : '';

		$value = ( !empty( $set['value'] ) )? $set['value'] : '';

		$topPropVal = ( !empty( $set['leftPropVal'] ) )? $set['leftPropVal'] : '';

		$topPropVal = ( !empty( $set['topPropVal'] ) )? $set['topPropVal'] : '';



		print('
	            <div class="form-group">
	                <label class="col-md-2 control-label">'.$labelText.'
	                    '. $required .'

	                </label>

                    <div class="col-md-10">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                        	<div class="containAnchorImgDiv">
                                '.$img.'
                            </div>
                            <div class="anchorPhotoValue">
                            	<a href="javascript:;" id="filePicker" data-name="'.$set['inputName'].'">
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new"> Select image </span>
                                        <input type="hidden" name="'.$set['inputName'].'"  value="'.$value.'">
                                        <input class="anchorImage_leftProp" type="hidden" name="'.$set['leftPropName'].'"  value="">
                                        <input class="anchorImage_topProp" type="hidden" name="'.$set['topPropName'].'"  value="">
                                    </span>
                                </a>
                                <!--<a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>-->
                            </div>
                        </div>
                        <div class="clearfix margin-top-10">

                            '.$helpBlock.'
                        </div>
                    </div>


	            </div>
			');


	}




	//input  tags
	public static function tags($set = [])
	{
		$labelText = (empty($set['labelText'])) ? '預設Label Text' : $set['labelText'];

		$helpBlock = (!empty($set['helpText']))? $set['helpText'] : '';

		$disabled = ( !empty($set['disabled']) AND $set['disabled'] == 'disabled' )? 'disabled="disabled"' : '';

		$required = ( !empty( $set['required'] ) AND $set['required'] )? '<span class="required"> * </span>' : '';

		$value = ( !empty( $set['value'] ) )? $set['value'] : '';

		print('

            <div class="form-group">
                <label class="col-md-2 control-label">'.$labelText.'
                    '. $required .'

                </label>
                <div class="col-md-10">
                    <input
                    	type="text"
                    	class="form-control tooltips tags medium tags_2"
                    	name="'.$set['inputName'].'"
                    	placeholder=""
                    	data-container="body"
                    	data-placement="top"
                    	data-original-title="'.$helpBlock.'"
                    	value="'. $value .'"
                    >
                </div>
            </div>
		');

	}

	public static function editor($set = [])
	{
		$labelText = (empty($set['labelText'])) ? '預設Label Text' : $set['labelText'];

		$helpBlock = (!empty($set['helpText']))? $set['helpText'] : '';

		$disabled = ( !empty($set['disabled']) AND $set['disabled'] == 'disabled' )? 'disabled="disabled"' : '';

		$required = ( !empty( $set['required'] ) AND $set['required'] )? '<span class="required"> * </span>' : '';

		$editorType = ['ckeditor','wysihtml5'];

		if( !empty( $set['type'] ) AND $set['type'] =='table')
		{
			return'
				<textarea class="wysihtml5 form-control" name="'.$set['inputName'].'" rows="15" >'.$set['value'].'</textarea>
			';
		}
		else
		{
			print('
                <div class="form-group">
                    <label class="col-md-2 control-label">
                    	'. $labelText .'
                        '. $required .'
                    </label>
                    <div class="col-md-10">
                        <textarea
                        	name="'.$set['inputName'].'"
                        	data-provide="markdown"
                        	rows="10"
                        	class="ckeditor tooltips  form-control"
                        	data-container="body"
                        	data-placement="top"
                        	data-original-title="'.$helpBlock.'"
                        >'.$set['value'].'</textarea>
                    </div>
                </div>
			');
		}

	}

	public static function colorPicker( $set = [] )
	{

		$labelText = (empty($set['labelText'])) ? '預設Label Text' : $set['labelText'];

		$helpBlock = (!empty($set['helpText']))? $set['helpText'] : '';

		$disabled = ( !empty($set['disabled']) AND $set['disabled'] == 'disabled' )? 'disabled="disabled"' : '';

		$required = ( !empty( $set['required'] ) AND $set['required'] )? '<span class="required"> * </span>' : '';

		if( !empty( $set['type'] ) AND  $set['type'] == 'table')
		{

			return '<input class="mycolor" name="'.$set['inputName'].'" value="'.$set['value'].'" style="width:100px;">';
		}
		else
		{
			print('
	            <div class="form-group">
	                <label class="col-md-2 control-label">'.$labelText.'
	                    '. $required .'
	                </label>
	                <div class="col-md-10">
							<input
								class="form-control tooltips mycolor"
								name="'.$set['inputName'].'"
								value="'.$set['value'].'"
								style="width:150px;"
		                    	placeholder=""
		                    	data-container="body"
		                    	data-placement="top"
		                    	data-original-title="'.$helpBlock.'"
		                    	'. $disabled .'
							>
	                </div>
	            </div>
			');
		}

	}
	//檔案欄位
	public static function filePicker( $set = [] )
	{

		$labelText = (empty($set['labelText'])) ? '預設Label Text' : $set['labelText'];

		$helpBlock = (!empty($set['helpText']))? $set['helpText'] : '';

		$disabled = ( !empty($set['disabled']) AND $set['disabled'] == 'disabled' )? 'disabled="disabled"' : '';

		$required = ( !empty( $set['required'] ) AND $set['required'] )? '<span class="required"> * </span>' : '';

		if( !empty( $set['type'] ) AND $set['type'] == 'table')
		{

			return '<input name="'.$set['inputName'].'" value="'.$set['value'].'" style="width:100px;">
					<a href="javascript:;" data-name="'.$set['inputName'].'" id="filePicker" class="btn default btn-file">Select File</a>';
		}
		else
		{
			print('
	            <div class="form-group">
	                <label class="col-md-2 control-label">'.$labelText.'
	                    '. $required .'
	                </label>
	                <div class="col-md-10">
							<input
								class="form-control tooltips file_pick_input"
								name="'.$set['inputName'].'"
								value="'.$set['value'].'"
		                    	placeholder=""
		                    	data-container="body"
		                    	data-placement="top"
		                    	data-original-title="'.$helpBlock.'"
		                    	'. $disabled .'
							>
							<a href="javascript:;" data-name="'.$set['inputName'].'" id="filePicker" class="btn default btn-file">Select File</a>
	                </div>
	            </div>
			');
		}

	}

	//日期挑選
	public static function datePicker($set = [])
	{

		$labelText = (empty($set['labelText'])) ? '預設Label Text' : $set['labelText'];

		$helpBlock = (!empty($set['helpText']))? $set['helpText'] : '';

		$disabled = ( !empty($set['disabled']) AND $set['disabled'] == 'disabled' )? 'disabled="disabled"' : '';

		$required = ( !empty( $set['required'] ) AND $set['required'] )? '<span class="required"> * </span>' : '';

		if(!empty( $set['type'] ) AND $set['type'] == 'table')
		{

			return '<input name="'.$set['inputName'].'" value="'.$set['value'].'" class="form-control tooltips date-picker" data-container="body" data-placement="top" data-original-title="'.$helpBlock.'" style="width:100px;">';
		}
		else
		{
			print('
	            <div class="form-group">
	                <label class="col-md-2 control-label">'.$labelText.'
	                    '. $required .'
	                </label>
	                <div class="col-md-10">
							<input
								type="text"
								class="form-control tooltips date-picker"
								name="'.$set['inputName'].'"
								value="'.$set['value'].'"
		                    	placeholder=""
		                    	data-container="body"
		                    	data-placement="top"
		                    	data-original-title="'.$helpBlock.'"
		                    	'. $disabled .'
							>
	                </div>
	            </div>
			');
		}

	}

	//日期挑選
	public static function numberInput( $set = [] )
	{
		$labelText = (empty($set['labelText'])) ? '預設Label Text' : $set['labelText'];

		$helpBlock = (!empty($set['helpText']))? $set['helpText'] : '';

		$disabled = ( !empty($set['disabled']) AND $set['disabled'] == 'disabled' )? 'disabled="disabled"' : '';

		$required = ( !empty( $set['required'] ) AND $set['required'] )? '<span class="required"> * </span>' : '';

		if( !empty( $set['type'] ) AND $set['type'] == 'table' )
		{

			return '<input name="'.$inputName.'" value="'.$inputValue.'" style="width:100px;">
					<a href="javascript:;" data-name="'.$inputName.'" id="multi-add" class="btn default btn-file">Select File</a>';
		}
		else
		{
			print('
	            <div class="form-group">
	                <label class="col-md-2 control-label">'.$labelText.'
	                    '. $required .'
	                </label>
	                <div class="col-md-10">
							<input
								type="text"
								class="form-control tooltips mask_currency"
								name="'.$set['inputName'].'"
								value="'.$set['value'].'"
		                    	placeholder=""
		                    	data-container="body"
		                    	data-placement="top"
		                    	data-original-title="'.$helpBlock.'"
		                    	'. $disabled .'
							>
	                </div>
	            </div>
			');
		}

	}

	public static function url( $path )
	{
		//Debugbar::info($path);
        //2015.9.23 加入語系路徑
        $path = (!empty(Config::get('app.dataBasePrefix')) )? App::getLocale().'/'.$path : $path;

        //加上測試機的路徑
        $path = ( !empty( parent::$testingLink ) )? parent::$testingLink.'/'.$path : $path;

        return url( $path );
	}

	public static function asset( $path )
	{

        return  base_path('views/'.$path) ;
	}



	public static function processTitleToUrl( $title )
	{
		$replace1 = str_replace(' ', '+', $title);
		$replace2 = str_replace('/', '^', $replace1);
		$replace3 = str_replace('.', '`', $replace2);

		return $replace3;
	}


	public static function revertUrlToTitle( $url )
	{
		$replace1 = str_replace('+', ' ', $url);
		$replace2 = str_replace('^', '/', $replace1);
		$replace3 = str_replace('`', '.', $replace2);

		return $replace3;
	}




}
