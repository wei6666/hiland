<?php

namespace App\Http\Controllers;

/**原生函式**/
use Illuminate\Http\Request;
use View;
use Session;
use Mail;
use Config;
use App\Http\Controllers\Fantasy\MakeItemV2;
use Cache;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;



/***相關Model*****/
use App\Http\Models\Setting;
use App\Http\Models\Menutitle;

class SettingController extends BackendController
{

    public function __construct()
    {
        parent::__construct();

        //系統訊息
        if(!empty(Session::get('Message')))
        {
            View::share('Message',Session::get('Message'));
        }else{
            View::share('Message','');
        }
        if(!empty(Session::get('Message_type')))
        {
            View::share('Message_type',Session::get('Message_type'));
        }else{
            View::share('Message_type','');
        }
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        //檢查
        //LangController::checkLang($lang);

        $setting = Setting::findOrFail(1);

        //print_r(json_decode($setting->content,true));

        return view('Fantasy.Setting.index', [
            "data" => json_decode($setting->content, true),
            "seo" => json_decode($setting->seo, true)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function postUpdate(Request $request)
    {
        
        $data = $request->input('Setting');
        $Menutitle=$request->input('Menutitle');
        $ContactUs =  "App\Http\Models\Menutitle";
        Menutitle::truncate();
        foreach ($Menutitle as $key => $value) {        

            $new = new $ContactUs;
            foreach ($value as $key1 => $value1) {
                $new->$key1 = $value1;
            }

            $new->save();

            
            # code...
        }
        //更新Cache
        //Cache::forever('globalsSet', $data);

        $find = Setting::find(1);
        $find->content = json_encode($data, JSON_UNESCAPED_UNICODE);

        if ($find->save()) {
            return redirect( MakeItemV2::url('Fantasy/網站基本設定') )->with('Message','儲存成功')->with('Message_type','success');
        }
        else{
            return redirect( MakeItemV2::url('Fantasy/網站基本設定') )->with('Message','儲存失敗')->with('Message_type','error');

        }
    }
}
