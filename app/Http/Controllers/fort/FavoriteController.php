<?php
namespace App\Http\Controllers\fort;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;
use Session;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;


/*相關的Controller*/
use App\Http\Controllers\Product\FunctionController as ProductFunction;

use App\Http\Models\Product\Product;
use App\Http\Models\Product\Code;
use App\Http\Models\Product\ProductCategory;
use App\Http\Models\Contact\ContactCountry;
use App\Http\Models\Contact\ContactArea;
use App\Http\Models\Seo;
use App\Http\Models\Home\Home;

class FavoriteController extends BackendController {

	
	/*首頁========================================================*/
	public function index($locale,$code='')
	{
		$Home=Home::first();
		
		if(!empty($code))
		{
			$code=Code::where('code',$code)->first();
			if(count($code) == 0 )
			{
				$sessiondata=[];
				Session::forget('product_data');
				Session::put('product_data',$sessiondata);
			}
			else
			{
				Session::put('product_data',explode(',', $code->data));
				$sessiondata=explode(',', $code->data);
			}
		
		}
		else
		{
			$sessiondata=Session::get('product_data');
		}
		

		$productdata=Product::where('is_visible',1)->whereIn('id',$sessiondata)->OrderBy('rank',"asc")->get();


		$ProductCategory=ProductCategory::where('is_visible',1)->get();
		$pcate=[];

		foreach($ProductCategory as $value)
		{
			$pcate[$value->id]=$value->title;
		}


		$country=ContactCountry::where('is_visible',1)->OrderBy('rank','asc')->get();
		foreach ($country as $key => $value) {
			$arr[$value->title]=ContactArea::where('is_visible',1)->where('country_id',$value->id)->OrderBy('rank',"asc")->get();
		}
		
		//dd(Session::get('product_data'));
		
		$seo=Seo::where('id',15)->first();
		return View::make( $locale.'.favorite.index',
			[
				'productdata'=>$productdata,
				'pcate'=>$pcate,
				'country'=>$arr,
				'seo'=>$seo, 
				'Home'=>$Home, 
				
				
				
			]);
	}

	
	public function formenu($locale,$code='')
	{
		return View::make( $locale.'.menu');
	}



	

	
	

	
}
