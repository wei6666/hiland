<?php
namespace App\Http\Controllers\fort;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;
use Session;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;


/*相關的Controller*/
use App\Http\Controllers\Product\FunctionController as ProductFunction;
use App\Http\Models\Product\Product;
use App\Http\Models\Product\ProductPhoto;
use App\Http\Models\Product\ProductSet;
use App\Http\Models\Product\ProductBig;
use App\Http\Models\Product\ProductCategory;
use App\Http\Models\Product\SearchPlace;
use App\Http\Models\Seo;


class ProductCategoryController extends BackendController {

	
	/*首頁========================================================*/
	public function index($locale)
	{
		
		$ProductSet=ProductSet::first();

		$allbig=ProductBig::where('is_visible' , 1)->OrderBy('rank',"asc")->get();

		$allcate=ProductCategory::where('is_visible' , 1)->OrderBy('rank',"asc")->get();
		$seo=Seo::where('id',3)->first();
		
		return View::make( $locale.'.product.index',
			[
				"allbig"=>$allbig,
				"allcate"=>$allcate,
				"ProductSet"=>$ProductSet,
				'seo'=>$seo, 
			]);
	}

	public function productajax($locale,$id='')
	{
		
		
		if($id =='all')
		{
		
			$allcate=ProductCategory::where('is_visible' , 1)->OrderBy('rank',"asc")->get();
		}
		elseif($id =='news')
		{
		
			$allcate=ProductCategory::where('is_visible' , 1)->where('is_new' , 1)->OrderBy('rank',"asc")->get();
		}
		else
		{
			$allcate=ProductCategory::where('is_visible' , 1)->where("big_id",$id)->OrderBy('rank',"asc")->get();
		}


		
		
		return View::make( $locale.'.product.productcategoryajaxchangedata',
			[
				
				"allcate"=>$allcate,
			]);


	}


	public function categorydetail($locale,$id='')
	{

		$pageBodyId="product_more";
		$sessiondata=Session::get('product_data');
		
		
		$Opendata="";
	    if(!empty($_GET['Open']))
	    {
	      $Opendata=Product::where('is_visible',1)
	                            ->where('id',$_GET['Open'])
	                            ->first();

	    }

		$ProductSet=ProductSet::first();

		$pagedata=ProductCategory::where('is_visible' , 1)->where("id",$id)->OrderBy('rank',"asc")->first();


		$productdata=Product::where('is_visible' , 1)->where("category_id",$id)->OrderBy('rank',"asc")->get();

		$productlist=Product::where('is_visible' , 1)->where("category_id",$id)->OrderBy('rank',"asc")->groupBy('size')->get();

		foreach($productdata as $key => $value)
		{

			$productdata[$key]->is_favorite='no';
			if(count($sessiondata) >0 && in_array($value->id, $sessiondata))
			{
				$productdata[$key]->is_favorite='yes';
			}
		}

		


		return View::make( $locale.'.product.categorydetail',
			[
				"ProductSet"=>$ProductSet,
				"pagedata"=>$pagedata,
				"productdata"=>$productdata,
				"productlist"=>$productlist,
				"Opendata"=>$Opendata,
				"pageBodyId"=>$pageBodyId,
				"cateid"=>$id,
			]);
	}


	public function productdetail($locale,$id='')
	{

		$id=$_GET['Open'];
		$productdata=Product::where('is_visible' , 1)->where("id",$id)->OrderBy('rank',"asc")->first();


		$sessiondata=Session::get('product_data');

		$productphoto=ProductPhoto::where('is_visible' , 1)->where("product_id",$productdata->id)->OrderBy('rank',"asc")->get();

		$ProductSet=ProductSet::first();


		$productdata->is_favorite='no';

		if(count($sessiondata) >0 && in_array($productdata->id, $sessiondata))
		{
			$productdata->is_favorite='yes';
		}

		
		


		return View::make( $locale.'.product.productdetail',
			[
				
				"pagedata"=>$productdata,
				"ProductSet"=>$ProductSet,
				"productphoto"=>$productphoto,
				
			]);

	}


	public function searchajax($locale,$color='')
	{

		
		
		$place=SearchPlace::distinct()->get();

		$sizesq=Product::select('size')->where('shape',1)->distinct()->OrderBy('size','asc')->get();

		$sizere=Product::select('size')->where('shape',2)->distinct()->OrderBy('size','asc')->get();

		$sizeha=Product::select('size')->where('shape',3)->distinct()->OrderBy('size','asc')->get();

		//$sizesq=$this::sortmyself($sizesq);

		$sizesq=$this::sortmyself($sizesq);

		$sizere=$this::sortmyself($sizere);

		$sizeha=$this::sortmyself($sizeha);



		//$sizeha=$this::sortmyself($sizeha);

		$face=Product::select('face')->distinct()->OrderBy('size')->get();

		

		
		return View::make( $locale.'.product.search',
			[
				
				"place"=>$place,
				"sizesq"=>$sizesq,
				"sizere"=>$sizere,
				"sizeha"=>$sizeha,
				"face"=>$face,
				"color" =>$color,
				
				
			]);
	}

	
	public function searchresult($locale, Request $request)
	{

		$forurl=$request->input('forurl');

		$ProductCategory=ProductCategory::get();
		$categoryarr=[];
		foreach($ProductCategory as $value)
		{
			$categoryarr[$value->id]=$value->title;
		}


		if(!empty($request->input('pro_num')))
		{
			$getproquery=Product::where('is_visible',1)
								->where('pro_num',$request->input('pro_num'))
								->OrderBy('rank')								
								->get();

			
			$allcondition=$request->input('pro_num');
			return View::make( $locale.'.product.result',
			[
				"allcondition"=>$allcondition,
				"getproquery"=>$getproquery,
				"categoryarr"=>$categoryarr,
			]);
		}
		
		if(!empty($_GET['place']))
		{
			$getplace=$_GET['place'];
		}
		else
		{
			$getplace=$request->input('place');
		}

		if(!empty($_GET['size']))
		{
			$getsize=$_GET['size'];
		}
		else
		{
			$getsize=$request->input('size');
		}

		if(!empty($_GET['face']))
		{
			$getface=$_GET['face'];
		}
		else
		{
			$getface=$request->input('face');
		}
		

		

		$allcondition=$getplace.",".$getsize.','.$getface;

		if(!empty($getplace))
		{
			
		$getplacearr=explode( ',', $getplace );

		$allplacedata=[];

		foreach ($getplacearr as $key => $value) {
				if(!empty($value))
				{
				$selectcate=ProductCategory::where('is_visible',1) ->where('place','like','%'.$value.'%')->get();

				array_push($allplacedata, $selectcate);
				}

			}

			$allcateid=[];



			foreach ($allplacedata as $key => $value) {
				foreach ($value as $key1 => $value1) {
					
					array_push($allcateid, $value1->id);
				}
			}

			$allProductData=[];

			$getproquery=Product::OrderBy('rank')->whereIn("category_id",$allcateid);
		
		}
		else
		{
			$getproquery=Product::OrderBy('rank');
		}


		

		if(!empty($getface)){
			foreach (explode(",", $getface) as $key => $value) {
				if(!empty($value))
				{
					$getproquery=$getproquery->orWhere('face',"like","%".$value."%");			
				}
			}
		}

		if(!empty($getsize)){
			foreach (explode(",", $getsize) as $key => $value) {
				if(!empty($value))
				{
					$getproquery=$getproquery->orWhere('size',"like","%".$value."%");			
				}
			}
		}

		$getproquery=$getproquery->where('is_visible',1)->get();


		// dd($getproquery);
		// foreach($getproquery as $value)
		// {
		// 	var_dump($value);
		// }

		$pageBodyId="rr";
		

		return View::make( $locale.'.product.result',
			[
				
				"getproquery"=>$getproquery,
				"allcondition"=>$allcondition,
				"categoryarr"=>$categoryarr,
				"pageBodyId"=>$pageBodyId,
				"forurl"=>$forurl,

			]);

	}


	public function forfavoriteajax($locale,$id='')
	{
		$pageBodyId="product_more";
		$sessiondata=Session::get('product_data');
		
		
		

		$ProductSet=ProductSet::first();

		$pagedata=ProductCategory::where('is_visible' , 1)->where("id",$id)->OrderBy('rank',"asc")->first();


		$productdata=Product::where('is_visible' , 1)->where("category_id",$id)->OrderBy('rank',"asc")->get();

		$productlist=Product::where('is_visible' , 1)->where("category_id",$id)->OrderBy('rank',"asc")->groupBy('size')->get();

		foreach($productdata as $key => $value)
		{

			$productdata[$key]->is_favorite='no';
			if(count($sessiondata) >0 && in_array($value->id, $sessiondata))
			{
				$productdata[$key]->is_favorite='yes';
			}
		}

		


		return View::make( $locale.'.product.forfavoriteajax',
			[
				"ProductSet"=>$ProductSet,
				"pagedata"=>$pagedata,
				"productdata"=>$productdata,
				"productlist"=>$productlist,
				
				"pageBodyId"=>$pageBodyId,
				"cateid"=>$id,
			]);

	}



	public function sortmyself($obj="")
	{
		$orderarr=[];
		if(!empty($obj))
		{
			$arr=$obj->toArray();
		}
		
		while(count($arr) != 0)
		{	
			$smallest=array_search(last($arr),$arr);
			
			foreach ($arr as $key => $value) 
			{
				if(explode('cm', $value['size'])[0] < explode('cm',$arr[$smallest]['size'])[0])
				{					
					$smallest=$key;
					if(explode('cm', $value['size'])[0] == explode('cm',$arr[$smallest]['size'])[0] && explode('cm', $value['size'])[1] > explode('cm',$arr[$smallest]['size'])[1])
					{					
						$smallest=$key;				
					}		
				}
				
			}
			
			//echo 'smallest is '.$smallest."--".$arr[$smallest]['size']."<br>";
			array_push($orderarr, $smallest);			
			unset($arr[$smallest]);
		}
		
		
		$lala=$obj->toArray();
		
	


		foreach ($orderarr as $key => $value) {
			
			
			$obj[$key]->size=$lala[$value]['size'];
			
		}
	


		return $obj;
	}
	

	
}
	