<?php
namespace App\Http\Controllers\fort;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;


/*相關的Controller*/
use App\Http\Controllers\Product\FunctionController as ProductFunction;

use App\Http\Models\Home\Home;
use App\Http\Models\Product\Product;
use App\Http\Models\Home\HomeImage;
use App\Http\Models\Style\Style;
use App\Http\Models\Style\StylePhoto;
use App\Http\Models\Product\ProductCategory;
use App\Http\Models\Product\ProductBig;
use App\Http\Models\Contact\ContactCountry;
use App\Http\Models\Seo;



class HomeController extends BackendController {

	
	/*首頁========================================================*/
	public function index($locale)
	{
		
		
		$pageBodyId='home';
		$Home=Home::first();

		$ContactCountry=ContactCountry::where('is_visible',1)->OrderBy('rank',"ASC")->get();
		$HomeImage=HomeImage::where('is_visible',1)->OrderBy('rank',"ASC")->get();

		if(!empty($Home->style_id))
        {

        $Home->style_id=str_replace('"', "", $Home->style_id);
        $Home->style_id=str_replace('[', "", $Home->style_id);
        $Home->style_id=str_replace(']', "", $Home->style_id);
        $downarr=$Home->style_id;
        $downarr=explode( ",",$Home->style_id);
        
    	}
    	else
    	{
    		$downarr=[];
    	}
        //取得內頁下載的表格的資料
        $Style=Style::where('is_visible',1)
                          ->whereIn('id',$downarr)
                          ->OrderBy('rank','asc')
                          ->get();


           


        if(!empty($Home->big_id))
        {

        $Home->big_id=str_replace('"', "", $Home->big_id);
        $Home->big_id=str_replace('[', "", $Home->big_id);
        $Home->big_id=str_replace(']', "", $Home->big_id);
        $downarr=$Home->big_id;
        $downarr=explode( ",",$Home->big_id);
        
    	}
    	else
    	{
    		$downarr=[];
    	}
        //取得內頁下載的表格的資料
        $ProductBig=ProductBig::where('is_visible',1)
                          ->whereIn('id',$downarr)
                          ->OrderBy('rank','asc')
                          ->limit(2)
                          ->get();


        if(!empty($Home->small_id))
        {

        $Home->small_id=str_replace('"', "", $Home->small_id);
        $Home->small_id=str_replace('[', "", $Home->small_id);
        $Home->small_id=str_replace(']', "", $Home->small_id);
        $downarr=$Home->small_id;
        $downarr=explode( ",",$Home->small_id);
        
    	}
    	else
    	{
    		$downarr=[];
    	}
        //取得內頁下載的表格的資料
        $ProductCategory=ProductCategory::whereIn('id',$downarr)         
                          ->OrderBy('rank','asc')
                          ->get();


         foreach($Style as $key => $value)
         {
         	$Style[$key]->image=StylePhoto::where('is_visible',1)
         								 ->where('style_id',$value->id)
         								 ->OrderBy("rank","ASC")
         								 ->first();
         	
         	$pcate=[];
         	foreach($ProductCategory as $value)
			{
				$pcate[$value->id]=$value->title;
			}
			

	        if(!empty($Style[$key]->image)){		        
					$proarr=[];
					$forpro=$Style[$key]->image->toArray();
					for($i=1;$i<=9 ; $i++)
					{
						if(!empty($forpro['has_product'.$i] ))
						{
						
							$data=Product::where('id', $forpro['has_product'.$i])->first();	

							$dorposition='';
							if($i==1)
							{
								$dorposition='tl';
							}
							if($i==2)
							{
								$dorposition='tm';
							}
							if($i==3)
							{
								$dorposition='tr';
							}
							if($i==4)
							{
								$dorposition='ml';
							}
							if($i==5)
							{
								$dorposition='mm';
							}
							if($i==6)
							{
								$dorposition='mr';
							}
							if($i==7)
							{
								$dorposition='dl';
							}
							if($i==8)
							{
								$dorposition='dm';
							}
							if($i==9)
							{
								$dorposition='dr';
							}
							
							//--->
							if(!empty($pcate[$data->category_id]))
							{
							$proarr[$dorposition]=$pcate[$data->category_id]."-".$data->pro_num."-".$data->category_id."-".$data->id."-".$data->for_night_img;
							}

							//array_push($proarr,$pcate[$data->category_id]."-".$data->pro_num);
						}
						else
						{
							
							array_push($proarr, "-");
						}

					}

					
					$Style[$key]->image->proarr=$proarr;					

					//下面的上下頁
				
			}
         }

         $seo=Seo::where('id',16)->first();
        //dd($Home,$Style,$ProductBig,$ProductCategory);
         $is_home='1';
		return View::make( $locale.'.home.index',
			[
				"Home"=>$Home,
				"Style"=>$Style,
				"ProductBig"=>$ProductBig,
				"ProductCategory"=>$ProductCategory,
				"HomeImage"=>$HomeImage,
				"ContactCountry"=>$ContactCountry,
				'seo'=>$seo, 
				"pageBodyId"=>$pageBodyId,
				"is_home"=>$is_home,
				
				
			]);
	}



	public function privacy($locale)
	{
			return View::make( $locale.'.private');
	}


	public function aa($locale)
	{
			
			$arr=['中正區','大同區','中山區','松山區','大安區','萬華區','信義區','士林區','北投區','內湖區','南港區','文山區'];

			echo 'insert INTO `tw_country` (`id`,`title`) VALUES(16,"嘉義縣");'."<br>";
			foreach($arr as $value)
			{
				echo 'insert INTO `tw_area` (`country_id`,`title`,`is_visible`) VALUES(16,"'.$value.'",1);'."<br>";
			}
	}

}
