<?php
namespace App\Http\Controllers\fort;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;
use Mail;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;


/*相關的Controller*/
use App\Http\Controllers\Product\FunctionController as ProductFunction;
use App\Http\Models\Contact\ContactCountry;
use App\Http\Models\Contact\ContactArea;
use App\Http\Models\Contact\ContactSet;
use App\Http\Models\Contact\Contact;
use App\Http\Models\Seo;
use App\Http\Models\Setting;

class ContactController extends BackendController {

	
	/*首頁========================================================*/
	public function index($locale)
	{
		$arr=[];
		$ContactSet=ContactSet::first();
		$country=ContactCountry::where('is_visible',1)->OrderBy('rank','asc')->get();
		foreach ($country as $key => $value) {
			$arr[$value->title]=ContactArea::where('is_visible',1)->where('country_id',$value->id)->OrderBy('rank',"asc")->get();
		}

        $seo=Seo::where('id',8)->first();
		//dd($arr);

        $pageBodyId="contact";
		return View::make( $locale.'.contact.index',
			[
				"country"=>$arr,
				"ContactSet"=>$ContactSet,
                'seo'=>$seo,
                'pageBodyId'=>$pageBodyId,
			]);
	}

	public function contactsubmit($locale ,Request $request)
	{
        $rules = ['captcha' => 'required|captcha'];



        $validator = Validator::make($request->all(), $rules);   

        //dd($request->all());

        if($validator->fails())
        {
            
            /*驗證碼錯誤*/
            if( $locale == 'zh-tw' ) 
            {
             
                echo '<script>'."alert('驗證碼錯誤！請重新輸入');window.history.back()".'</script>';  
            }
            else
            {
                echo '<script>'."alert('Please Fill In The Correct Validation Code.');window.history.back()".'</script>'; 
            }



        
        }
        else
        {

            $data = $request->input('data');
            $toaddarr=[];
            if(empty($data['name']))
            {
                if($locale=='zh-tw')
                {
                    array_push($toaddarr, "姓名");
                }
                else
                {
                    array_push($toaddarr, "name");
                }
                                
            }

           

            if(empty($data['phone']))
            {
                if($locale=='zh-tw')
                {
                    array_push($toaddarr, "電話");
                }
                else
                {
                    array_push($toaddarr, "phone");
                }
                                
            }

            if(empty($data['email']))
            {
                if($locale=='zh-tw')
                {
                    array_push($toaddarr, "電子信箱");
                }
                else
                {
                    array_push($toaddarr, "email");
                }
                                
            }

            $arrstr='';
            foreach($toaddarr as $value)
            {
                if(empty($arrstr ))
                {
                    $arrstr.=$value;
                }
                else
                {
                    $arrstr.=",".$value;
                }
                
            }

            if($arrstr!="")
            {
                if($locale=='zh-tw')
                    {
                        echo '<script>'."alert('請輸入".$arrstr." '); window.history.back();".'</script>';
                    }
                    else
                    {
                        echo '<script>'."alert('please enter".$arrstr."'); window.history.back();".'</script>';
                    }
            }

            // $str="";
            // foreach( $data1 as $key => $value )
         //    {
         //        $str.=$key." = ".$value."\n";
         //    }


            // $data = [
         //            'message' => $str,
         //            'formPass' => true
         //        ];

            
            //   $data = json_encode($data, JSON_UNESCAPED_UNICODE);

         //        return $data;

            $ContactUs =  "App\Http\Models\Contact\Contact";

            $new = new $ContactUs;

            foreach( $data as $key => $value )
            {
                $new->$key = $value;
            }
            
            if($new->save())
            {

                $set = [];
                $set['view'] = 'zh-tw.contact.mail';
                
                $Setting = Setting::first(); 
                $Setting = json_decode($Setting->content, true);
                
                $globalrecieveMails = explode(";" , $Setting['contact_emails']);

                $set['to'] =$globalrecieveMails; //收件者;

                //$set['to'] ='victor@wddgroup.com'; //收件者
                $set['to_name'] = '黃山石 - '.$new['name'];
                $set['subject'] = '聯絡我們通知信 - 黃山石 - '.$new['name'];
                
                $is_success=Mail::send( $set['view'], ['data' => $new], function ($m) use ( $set ) {
                        $m->from( "hiland.service@gmail.com" , $set['to_name'] );
                        $m->to( $set['to'])->subject( $set['subject'] );
                    });



                // if( $is_success > 0 )
                // {
                //     $new->is_mail=1;
                //     $new->save();

                // }
                // else
                // {
                //   $new->is_mail=0;
                //   $new->save();
                // }    
                    echo '<script>'."alert('我們已經收到您的表單 將盡快回復');window.location.replace(document.referrer)".'</script>';  
            } 
            else
            {
                /* $message = ( $locale == 'zh-tw' ) ? '聯絡表單送出失敗！請再填寫一次' : 'Your form is submited failed! please try again';
                    $data = [
                        'message' => '再填寫一次',
                        'formPass' => true
                    ];*/
                    echo '<script>'."alert('表單填寫有誤 再麻煩您重填一次');window.location.replace(document.referrer)".'</script>';  
            }

                
             
			}

		}



	
	

	
}
	