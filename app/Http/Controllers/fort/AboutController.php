<?php
namespace App\Http\Controllers\fort;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;


/*相關的Controller*/
use App\Http\Controllers\Product\FunctionController as ProductFunction;

use App\Http\Models\About\About;
use App\Http\Models\About\AboutSet;
use App\Http\Models\About\AboutCertificate;
use App\Http\Models\About\AboutYearData;
use App\Http\Models\About\AboutEquitment;
use App\Http\Models\Seo;


class AboutController extends BackendController {

	
	/*首頁========================================================*/
	public function index($locale)
	{
		
		
		
		$AboutSet=AboutSet::first();
		$seo=Seo::where('id',7)->first();
		$pageBodyId='about';
		
		

		



		return View::make( $locale.'.About.index',
			[
				"AboutSet"=>$AboutSet,
				'seo'=>$seo,
				'pageBodyId'=>$pageBodyId,
				
				
			]);
	}

	public function index_s1($locale)
	{
		
		
		
		$AboutSet=AboutSet::first();

		$AboutCertificate=AboutCertificate::where('is_visible',1)
								->OrderBy('rank','asc')								
								->get();


		$AboutYearData=AboutYearData::where('is_visible',1)
								->OrderBy('rank','asc')								
								->get();
		
		

		// $ContactUs =  "App\Http\Models\About\AboutSet";

           // $AboutSet = new $ContactUs;


		
		/*$About=About::where('is_visible',1)
								->OrderBy('rank','asc')
								->whereIn('category_id',$onCategory)
								->get();*/



		return View::make( $locale.'.About.index_s1',
			[
				"AboutSet"=>$AboutSet,
				"AboutYearData"=>$AboutYearData,
				"AboutCertificate"=>$AboutCertificate,
				
			]);
	}

	public function index_s2($locale)
	{
		
		$AboutSet=AboutSet::first();

		
		 // $ContactUs =  "App\Http\Models\About\AboutSet";

   //          $AboutSet = new $ContactUs;

		return View::make( $locale.'.About.index_s2',
			[
				"AboutSet"=>$AboutSet,
				
				
			]);
	}

	public function index_s3($locale)
	{
		
		
		
		$AboutSet=AboutSet::first();
		
		$AboutEquitment=AboutEquitment::where('is_visible',1)
										->OrderBy('rank','asc')								
										->get();
		 // $ContactUs =  "App\Http\Models\About\AboutSet";

   //          $AboutSet = new $ContactUs;

		return View::make( $locale.'.About.index_s3',
			[
				"AboutSet"=>$AboutSet,
				"AboutEquitment"=>$AboutEquitment,
				
				
			]);
	}


	
	

	
}
