<?php
namespace App\Http\Controllers\fort;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;


/*相關的Controller*/
use App\Http\Controllers\Product\FunctionController as ProductFunction;

use App\Http\Models\Faq\Faq;
use App\Http\Models\Faq\FaqSet;
use App\Http\Models\Faq\FaqCategory;
use App\Http\Models\Seo;

class FaqController extends BackendController {

	
	/*首頁========================================================*/
	public function index($locale)
	{
		
		$page_data=[];
		
		$FaqSet=FaqSet::first();

		$FaqCategory=FaqCategory::where('is_visible',1)
								->OrderBy('rank','asc')
								->get();
		$onCategory=[];
		foreach ($FaqCategory as $key => $value) {
			array_push($onCategory, $value->id);
		}
		

		$Faq=Faq::where('is_visible',1)
								->OrderBy('rank','asc')
								->whereIn('category_id',$onCategory)
								->get();


		$seo=Seo::where('id',14)->first(); 
		return View::make( $locale.'.faq.index',
			[
				'Faq'=>$Faq,
				'FaqCategory'=>$FaqCategory,
				'FaqSet'=>$FaqSet,
				'seo'=>$seo,
				
			]);
	}


	
	

	
}
