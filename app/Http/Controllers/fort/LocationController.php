<?php
namespace App\Http\Controllers\fort;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;


/*相關的Controller*/
use App\Http\Controllers\Product\FunctionController as ProductFunction;

use App\Http\Models\Location\Location;
use App\Http\Models\Location\LocationSet;
use App\Http\Models\Contact\ContactCountry;
use App\Http\Models\Contact\ContactArea;
use App\Http\Models\Seo;

class LocationController extends BackendController {

    
    /*首頁========================================================*/
    public function index($locale)
    {

        $country=ContactCountry::where('is_visible',1)->OrderBy('id','asc')->get();
        foreach ($country as $key => $value) {
            $arr[$value->title]=ContactArea::where('is_visible',1)->where('country_id',$value->id)->OrderBy('rank',"asc")->get();
        }
        
        $page_data=[];

        $Opendata="";
        if(!empty($_GET['Open']))
        {
          $Opendata=Location::where('is_visible',1)
                                ->where('id',$_GET['Open'])
                                ->first();

        } 
        
        $LocationSet=LocationSet::first();

        $Location=Location::where('is_visible',1)
                                ->OrderBy('rank','asc')                              
                                ->get();

        $seo=Seo::where('id',17)->first();

        $pageBodyId="location";

        return View::make( $locale.'.location.index',
            [
                'Location'=>$Location,
                'LocationSet'=>$LocationSet,
                'Opendata'=>$Opendata,
                "country"=>$arr,
                'seo'=>$seo,
                'pageBodyId'=>$pageBodyId,
                
            ]);
    }
    

    public function locationspage($locale,$id='')
    {
        $Location=Location::where('is_visible',1)
                                ->where('id',$id)
                                ->OrderBy('rank','asc')                             
                                ->first();

        $LocationSet=LocationSet::first();
        return View::make( $locale.'.location.locationdetail',
            [
                'Location'=>$Location,
                'LocationSet'=>$LocationSet,
               
            ]);
    }


    
    

    
}
