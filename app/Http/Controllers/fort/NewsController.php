<?php
namespace App\Http\Controllers\fort;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;
use DateTime;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;


/*相關的Controller*/
use App\Http\Controllers\Product\FunctionController as ProductFunction;

use App\Http\Models\News\News;
use App\Http\Models\News\NewsSet;
use App\Http\Models\Seo;


class NewsController extends BackendController {

	
	/*首頁========================================================*/
	public function index($locale)
	{
		
		$seo=Seo::where('id',5)->first();
		$Opendata="";
		$og_data=[];
	    if(!empty($_GET['Open']))
	    {
	      $Opendata=News::where('is_visible',1)
	                            ->where('id',$_GET['Open'])
	                            ->first();
	      
	      $og_data['web_title']=$Opendata->out_title;
	      $og_data['meta_description']=$Opendata->out_content;
	      $og_data['og_image']=$Opendata->out_img;
	      

	      //$seo['content']=$Opendata->content;

	    }  

		$page_data=[];
		
		$NewsSet=NewsSet::first();

		
		

		$News=News::where('is_visible',1)
								// ->where('st_date',"<=",Date("Y-m-d"))
		    		// 			->where('ed_date',">=",Date("Y-m-d"))
								->OrderBy('created_at','desc')		
								->limit(6)						
								->get();

		$earlyestnew=News::where('is_visible',1)					
					->OrderBy('date','asc')
					->first();


		


		return View::make( $locale.'.news.index',
			[
				'News'=>$News,				
				'NewsSet'=>$NewsSet,
				'Opendata'=>$Opendata, 
				'seo'=>$seo, 
				'earlyestnew'=>$earlyestnew,
				'og_data'=>$og_data,
				
			]);
	}


	public function newspage($locale,$id='')
	{	
		$News=News::where('is_visible',1)
								->where('id',$id)
								->OrderBy('rank','asc')								
								->first();

		$this_time=new DateTime($News->date);

		$this_time=$this_time->format("Y");

		$next='';
		$last='';

		$allnews=News::where('is_visible',1)
					->where('date','like',"%".$this_time."%")
					// ->where('st_date',"<=",Date("Y-m-d"))
		   //  		->where('ed_date',">=",Date("Y-m-d"))
					->OrderBy('rank')
					->get();

		
					//->toArray();
		
		foreach ($allnews as $key => $value) {
			
			if($id == $value['id'])
			{
				if(!empty($allnews[$key+1]) )
				{
					$next=$allnews[$key+1];	
				}
				else
				{
					$next=$allnews[0];
				}	

				if(!empty($allnews[$key-1]) )
				{
					$last=$allnews[$key-1];	
				}
				else
				{
					$last=$allnews[count($allnews)-1];
				}	
			}

		}
		

		
		return View::make( $locale.'.news.newsdetail',
			[
				'News'=>$News,
				'next'=>$next,
				'last'=>$last,
				
			]);
	}


public function ajaxnewsdata($locale,$year='',$num='')
{
		$num+=6;
		$News=News::where('is_visible',1)
								->where('date','like',"%".$year."%")
								->OrderBy('created_at','desc')
								->limit($num)						
								->get();


		foreach($News as $key => $value)
		{
			
			if($key < $num-6)
			{
				unset($News[$key]);
			}
		}
		if($year=='ALL')
		{
			$News=News::where('is_visible',1)
								->OrderBy('created_at','desc')
								->get();
		}

				

		return View::make( $locale.'.news.newsajax',
			[
				'News'=>$News,
				
			]);
}

	
	

	
}
