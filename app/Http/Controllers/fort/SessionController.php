<?php
namespace App\Http\Controllers\fort;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;
use Session;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;


/*相關的Controller*/
use App\Http\Controllers\Product\FunctionController as ProductFunction;

use App\Http\Models\Product\Code;


class SessionController extends BackendController {

	
	/*首頁========================================================*/
	public function session_edit($locale,$id='')
	{
		
		
		$sessiondata=Session::get('product_data');

		if($id=='show')
		{
			var_dump( Session::get('product_data') );
			die;
			
		}
		
		if(count($sessiondata) == 0)
		{
			
			$sessiondata=[];
		}

	
		//dd(Session::get('product_data'));
		if( !in_array($id, $sessiondata))
		{
			
			array_push($sessiondata, $id);
		}
		else
		{
			
			if(count($sessiondata) >0)
			{
				foreach($sessiondata as $key => $value)
				{
					if($value == $id)
					{
						unset($sessiondata[$key]);
					}
				}
			}
			
		}
		
		
		Session::forget('product_data');
		Session::put('product_data',$sessiondata);
		
		if($id=='ff')
		{
			Session::forget('product_data');
			
		}
		
		//dd(Session::get('product_data'));
		return Session::get('product_data');;
	}


	public function remove_session($locale,$id='')
	{
		$sessiondata=Session::get('product_data');

		foreach($sessiondata as $key => $value)
				{
					if($value == $id)
					{
						unset($sessiondata[$key]);
					}
				}
		Session::forget('product_data');
		Session::put('product_data',$sessiondata);

	}


	public function geturl($locale,$id='')
	{
		$arr=[];
		//組出大小寫Ａ－Ｚ　跟０－９
		for( $i=65; $i<91;$i++){
		
			array_push($arr, chr($i));
		}

		for( $i=97; $i<123;$i++){
			array_push($arr, chr($i));
		}


		for( $i=48; $i<58;$i++){
			array_push($arr, chr($i));
		}

		//組7個字的url

		$str="";
		for($p=0; $p<7; $p++)
		{
			$str.=$arr[rand(0,61)];
		}
		//避免重複
		if(!empty(Code::where('code',$str)->first()))
		{

			for($p=0; $p<7; $p++)
			{
				$str.=$arr[rand(0,61)];
			}
		}

		//準備存檔
		
		 $Code =  "App\Http\Models\Product\Code";

         $data = new $Code;


		$data->code=$str;

		$savedata='';
		$arrlala=Session::get('product_data');
		sort( $arrlala);
		
		//把arr便str放資料庫
		if(!empty(Session::get('product_data')))
			{
				foreach($arrlala as $value)
					{
						$savedata.=$value.",";
					}
			}

		//有相同結果 就不產
		if(!empty(Code::where('data',$savedata)->first()))
		{
			$temp=Code::where('data',$savedata)->first();
			$str=$temp->code;
		}
		//存檔
		else
		{
		$data->data=$savedata;

		$data->save();
		}
		//印結果
		return  ItemMaker::url("favorite/".$str);
		
	}
	
}
