<?php
namespace App\Http\Controllers\fort;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;


/*相關的Controller*/
use App\Http\Controllers\Product\FunctionController as ProductFunction;

use App\Http\Models\Style\Style;
use App\Http\Models\Style\StylePhoto;
use App\Http\Models\Style\StyleSet;
use App\Http\Models\Product\Product;
use App\Http\Models\Product\ProductCategory;
use App\Http\Models\Seo;


class StyleController extends BackendController {

	
	/*首頁========================================================*/
	public function index($locale)
	{
		
		
		$Styledata=Style::where('is_visible',1)->OrderBy('rank',1)->get();

		$StyleSet=StyleSet::first();

		$seo=Seo::where('id',4)->first();

		return View::make( $locale.'.style.index',
			[

				"Styledata"=> $Styledata,
				"StyleSet"=> $StyleSet,
				'seo'=>$seo, 

				
			]);
	}


	public function styledetail($locale, $id='')
	{
		
		
		$Styledata=Style::where('is_visible',1)->where('id',$id)->first();
		$StylePhoto=StylePhoto::where('style_id',$id)->OrderBy('rank','asc')->get();
		$ProductCategory=ProductCategory::where('is_visible',1)->get();
		$pcate=[];
		foreach($ProductCategory as $value)
		{
			$pcate[$value->id]=$value->title;
		}


		foreach($StylePhoto as $key => $value)
		{
			$proarr=[];
			$forpro=$value->toArray();

			for($i=1;$i<=9 ; $i++)
			{
				if(!empty($forpro['has_product'.$i] ))
				{
				
					$data=Product::where('id', $forpro['has_product'.$i])->first();	

					$dorposition='';
					if($i==1)
					{
						$dorposition='tl';
					}
					if($i==2)
					{
						$dorposition='tm';
					}
					if($i==3)
					{
						$dorposition='tr';
					}
					if($i==4)
					{
						$dorposition='ml';
					}
					if($i==5)
					{
						$dorposition='mm';
					}
					if($i==6)
					{
						$dorposition='mr';
					}
					if($i==7)
					{
						$dorposition='dl';
					}
					if($i==8)
					{
						$dorposition='dm';
					}
					if($i==9)
					{
						$dorposition='dr';
					}
					if($data != NULL){					
					$proarr[$dorposition]=$pcate[$data->category_id]."-".$data->pro_num."-".$data->category_id."-".$data->id."-".$data->for_night_img;
					}

					//array_push($proarr,$pcate[$data->category_id]."-".$data->pro_num);
				}
				else
				{
					
					array_push($proarr, "-");
				}

			}

			
			$StylePhoto[$key]->proarr=$proarr;


			

			//下面的上下頁
		



			
		}



		$next;
		$last;

		$allnews=Style::where('is_visible',1)					
					->OrderBy('rank')
					->get()
					->toArray();
		
		$allnewsforindex=[];
		foreach($allnews as $value)			
		{
			array_push($allnewsforindex, $value['id']);
		}
		
		foreach ($allnews as $key => $value) {
			
			if($id == $value['id'])
			{
				if(!empty($allnews[$key+1]) )
				{
					$next=$allnews[$key+1];	
				}
				else
				{
					$next=$allnews[0];
				}	

				if(!empty($allnews[$key-1]) )
				{
					$last=$allnews[$key-1];	
				}
				else
				{
					$last=$allnews[count($allnews)-1];
				}	
			}

		}

		
		$next['index']=array_search($next['id'],$allnewsforindex);

		$last['index']=array_search($last['id'],$allnewsforindex);

		//dd($StylePhoto);

		return View::make( $locale.'.style.styledetail',
			[

				"Styledata"=> $Styledata,
				"StylePhoto"=> $StylePhoto,
				"next"=> $next,
				"last"=> $last,
				"id"=> $id,
				

				
			]);
	}


	
	

	
}
