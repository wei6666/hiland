<?php

namespace App\Http\Controllers;

/**原生函式**/
use Illuminate\Http\Request;
use View;
use Config;
use Session;
use Route;
use App;
use Validator;
use Debugbar;
use App\Http\Controllers\Fantasy\MakeItemV2;
/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;

/**相關Models**/
use App\Http\Models\Seo;

class SeoController extends BackendController
{


	//批次修改路徑
    protected static $ajaxEditLink = 'Fantasy/SEO管理/ajax-list/';

    //批次修改顯示及編輯的欄位設定
    public static $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array( 
            "field" => "rank",
            "inputType" => "text", 
            "is_edit"=> true
        ),
        "標題" => Array( 
            "field" => "title",
            "inputType" => "text", 
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array( 
                "field" => "is_visible",
                "inputType" => "radio", 
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );
    public static $photoTable = [
        "排序" => "rank",
        "圖片" => "image",
        "名稱" => "title",
        "狀態" => "is_visible"
    ];
    public function __construct()
    {
        parent::__construct();

        View::share('photoTable', self::$photoTable);

        //系統訊息
        if(!empty(Session::get('Message')))
        {
            View::share('Message',Session::get('Message'));
        }else{
            View::share('Message','');
        }

    }

    public function getIndex()
    {
        $Datas = parent::findDataAndAssociate([
            "modelName" => 'Seo',
            "select" => ['id','rank','is_visible','title']
        ]);

        return view('Fantasy.Seo.index')
                    ->with('Datas', $Datas )
                    ->with('ajaxEditLink', self::$ajaxEditLink);
    }






    public function getCreate()
    {
        Debugbar::info('This is some useful information.');
        return view('Fantasy.Seo.edit',
            [
                'data'=>[],
                'actionUrl' => MakeItemV2::url('Fantasy/SEO管理/store')
            ]
        );
    }






    public function postStore(Request $request)
    {
        $resoult = json_decode(parent::addNew([
            "Datas" => $request->input('Seo') ,
            "modelName" => 'Seo',
            "routeFix" => 'SEO管理'
            ]), true );

        if( !empty( $resoult['redirect'] ) )
        {
            // parent::updateMulti( $request->input('Photo'), 'Photo', 'series_id', $resoult['parent_id']);

            return redirect( $resoult['redirect'] )->with('Message','新增成功, 轉到編輯頁面');
        }
    }







    public function getEdit($locale, $id)
    {

        // $Photo = Photo::where('series_id', $id)->get()->toArray();

        return view('Fantasy.Seo.edit',[
                'data' => Seo::find($id),
                'actionUrl' => MakeItemV2::url('Fantasy/SEO管理/update')
            ]);
    }






    public function postUpdate(Request $request)
    {
        if(!empty($request->input('method')) AND $request->input('method')=='ajaxEdit')
        {
            //echo "批次修改<br>";
            parent::updateOne( $request->input('Seo'), 'Seo', 'ajaxEdit');
        }
        else
        {
            $Datas = $request->input('Seo');
            parent::updateOne( $Datas, 'Seo', '');

            // parent::updateMulti($request->input('Photo'), 'Photo', 'series_id', $Datas['id']);
            return redirect( MakeItemV2::url('Fantasy/SEO管理/edit/'.$Datas['id']) )->with('Message','儲存成功！');

        }
    }








    public function postDestroy(Request $request)
    {
        $id = $request->input('id');

        $method = ( !empty( $request->input('method') ) )? $request->input('method') : '';
        //$method =  $request->input('method');

        if( empty( $method ) )
        {
            parent::deleteOne( 'Seo', $id );
        }
        else
        {
            if( !empty( $id ) AND count( $id ) > 0 )
            {
                foreach ($id as $row) {
                    parent::deleteOne($method, $row );
                }
            }
        }
        
    }
    /*==============jQuery Ajax ====================*/
    //批次修改
    public static function postAjaxList(Request $request)
    {
        $ids = $request->input("ids");
        //$works = Work::where('id','in',$ids)->get();
        $works = Array(); 
            foreach ($ids as $row) {
                $works[] = Seo::where('id','=',$row)
                            ->select('id', 'rank','title', 'is_visible')
                            ->get();
            }
        return view('Fantasy.Ajax.list')
                    ->with('ajaxEditList',self::$ajaxEditList)
                    ->with('modal','Seo')
                    ->with('update_link', MakeItemV2::url('Fantasy/SEO管理/update'))
                    ->with('datas',$works);
    }

    public function postChangeStatic(Request $request)
    {
        $datas = $request->all();   

        $res = parent::updataOneColumns([
                'modelName' => 'Seo',
                'id' => $datas['id'],
                'columns' => $datas['columns'],
                'value' => $datas['value']
            ]);

        return $res;

    }

}
