<?php

namespace App\Http\Models\About;

use Config;

use Illuminate\Database\Eloquent\Model;

class AboutYearData extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."about_yeardata");
        }else{
            $this->setTable("about_yeardata");
        }
    }

}
