<?php

namespace App\Http\Models\Faq;

use Config;

use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."faq_category");
        }else{
            $this->setTable("faq_category");
        }
    }

}
