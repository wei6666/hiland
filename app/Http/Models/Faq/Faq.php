<?php

namespace App\Http\Models\FAQ;

use Config;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."faq");
        }else{
            $this->setTable("faq");
        }
    }

}
