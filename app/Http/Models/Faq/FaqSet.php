<?php

namespace App\Http\Models\FAQ;

use Config;

use Illuminate\Database\Eloquent\Model;

class FaqSet extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."faq_set");
        }else{
            $this->setTable("faq_set");
        }
    }

}
