<?php

namespace App\Http\Models;

use Config;

use Illuminate\Database\Eloquent\Model;

class Menutitle extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."menu_photo");
        }else{
            $this->setTable("menu_photo");
        }
    }

}
