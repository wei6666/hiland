<?php

namespace App\Http\Models\Style;

use Config;

use Illuminate\Database\Eloquent\Model;

class StyleSet extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."style_set");
        }else{
            $this->setTable("style_set");
        }
    }

}
