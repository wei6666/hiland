<?php

namespace App\Http\Models\Style;

use Config;

use Illuminate\Database\Eloquent\Model;

class StylePhoto extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."style_photo");
        }else{
            $this->setTable("style_photo");
        }
    }

}
