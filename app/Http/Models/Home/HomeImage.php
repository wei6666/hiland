<?php

namespace App\Http\Models\Home;

use Config;

use Illuminate\Database\Eloquent\Model;

class HomeImage extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."home_img");
        }else{
            $this->setTable("home_img");
        }
    }

}
