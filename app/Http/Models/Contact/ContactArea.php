<?php

namespace App\Http\Models\Contact;

use Config;

use Illuminate\Database\Eloquent\Model;

class ContactArea extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."area");
        }else{
            $this->setTable("area");
        }
    }

}
