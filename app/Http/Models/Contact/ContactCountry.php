<?php

namespace App\Http\Models\Contact;

use Config;

use Illuminate\Database\Eloquent\Model;

class ContactCountry extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."country");
        }else{
            $this->setTable("country");
        }
    }

}
