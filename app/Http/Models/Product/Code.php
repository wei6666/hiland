<?php

namespace App\Http\Models\Product;

use Config;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."code");
        }else{
            $this->setTable("code");
        }
    }

}
