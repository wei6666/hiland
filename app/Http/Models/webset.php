<?php

namespace App\Http\Models;

use Config;
use Illuminate\Database\Eloquent\Model;

class webset extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."configs");
        }else{
            $this->setTable("configs");
        }
    }


}
