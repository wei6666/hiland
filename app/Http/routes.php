<?php
use App\Http\Controllers\Fantasy\BackendController as myBackEnd;
use ItemMaker as Items;
use Illuminate\Http\Request;

//轉到預設語系
Route::get('/',function(){
	return redirect( Items::url('/'.App::getLocale()) );
});

//轉到預設語系
Route::get('/Fantasy',function(){
	return redirect( Items::url('/'.App::getLocale().'/Fantasy') );
});

//轉到預設語系
Route::get('/home',function(){
	return redirect( Items::url('/'.App::getLocale().'/Fantasy') );
});



/*===後台===*/

Route::group(['prefix'=>'/{locale?}'],function(){

	Route::group(['prefix'=>'/Fantasy','middleware'=> 'auth'],function(){
		Route::group(['prefix'=>'/schema'],function(){
			Route::controller('/','schemaController');
		});
		Route::get('/','Fantasy\FantasyController@index');
				//{addRoute}
		Route::group(['prefix'=>'Contact'],function(){
			Route::controller('聯絡我們設定', 'Fantasy\Contact\ContactSetController');
			Route::controller('聯絡我們管理', 'Fantasy\Contact\ContactController');
			Route::controller('各縣市選單管理', 'Fantasy\Contact\ContactPlaceController');
		
		
		});

		Route::group(['prefix'=>'Faq'],function(){
			Route::controller('問與答設定', 'Fantasy\Faq\FaqSetController');
			Route::controller('問與答管理', 'Fantasy\Faq\FaqController');
			
		
		
		});


		Route::group(['prefix'=>'News'],function(){
			Route::controller('新聞基本設定', 'Fantasy\News\NewsSetController');
			Route::controller('新聞管理', 'Fantasy\News\NewsController');
			
		
		
		});
		Route::group(['prefix'=>'Location'],function(){
			Route::controller('銷售據點設定', 'Fantasy\Location\LocationSetController');
			Route::controller('銷售據點管理', 'Fantasy\Location\LocationController');
			
		
		
		});

		Route::group(['prefix'=>'Product'],function(){
			Route::controller('產品設定', 'Fantasy\Product\ProductSetController');
			Route::controller('產品類別管理', 'Fantasy\Product\ProductBigController');
			Route::controller('產品次類別管理', 'Fantasy\Product\ProductCategoryController');
			Route::controller('產品管理', 'Fantasy\Product\ProductController');
			
		});


		Route::group(['prefix'=>'Style'],function(){
			Route::controller('黃山石風格設定', 'Fantasy\Style\StyleSetController');
			Route::controller('黃山石風格管理', 'Fantasy\Style\StyleController');
			Route::controller('StyleLight', 'Fantasy\Style\StyleLightController');
			
		});

		Route::controller('/關於我們管理', 'Fantasy\About\AboutController');
		Route::controller('/網站基本設定', 'SettingController');
		Route::controller('SEO管理', 'SeoController');
		Route::controller('帳號管理/帳號列表', 'Fantasy\AccountController');
		Route::controller('帳號管理/權限', 'Fantasy\RoleController');
		Route::controller('/首頁管理', 'Fantasy\Home\HomeController');
		Route::controller('/我的最愛設定', 'Fantasy\Favorite\FavoriteController');
		Route::controller('/SearchPlace', 'Fantasy\Product\SearchPlaceController');

		
	});

	/*===前台================================================*/
	Route::group(['prefix'=>'/'], function(){
		
		Route::get('/contact', 'fort\ContactController@index');
		Route::post('/contactsubmit', 'fort\ContactController@contactsubmit');
		Route::get('/faq', 'fort\FaqController@index');
		Route::get('/news', 'fort\NewsController@index');
		Route::get('/newsdetail/{id?}', 'fort\NewsController@index');
		Route::get('/newspage/{id?}', 'fort\NewsController@newspage');
		Route::get('/newspageshare/{id?}', 'fort\NewsController@newspage');
		Route::get('/newsajax/{year?}/{num?}', 'fort\NewsController@ajaxnewsdata');
		Route::get('/location', 'fort\LocationController@index');
		Route::get('/locationdetail/{id?}', 'fort\LocationController@index');
		Route::get('/locationpage/{id?}', 'fort\LocationController@locationspage');
		Route::get('/productcategory', 'fort\ProductCategoryController@index');
		Route::get('/categorydetail/{id?}', 'fort\ProductCategoryController@categorydetail');
		Route::get('/productcategoryajax/{id?}', 'fort\ProductCategoryController@productajax');
		 Route::get('/productdetail/{id?}', 'fort\ProductCategoryController@categorydetail');
		 Route::get('/productpage/{id?}', 'fort\ProductCategoryController@productdetail');
		 Route::get('/style', 'fort\StyleController@index');
		 Route::get('/styledetail/{id?}', 'fort\StyleController@styledetail');
		 Route::get('/sessionedit/{id?}', 'fort\SessionController@session_edit');
		 Route::get('/removesession/{id?}', 'fort\SessionController@remove_session');
		 Route::get('/geturl', 'fort\SessionController@geturl');
		 Route::get('/favorite/{code?}', 'fort\FavoriteController@index');
		 Route::get('/menu', 'fort\FavoriteController@formenu');
		 Route::get('/about', 'fort\AboutController@index');
		 Route::get('/about_s1', 'fort\AboutController@index_s1');
		 Route::get('/about_s2', 'fort\AboutController@index_s2');
		 Route::get('/about_s3', 'fort\AboutController@index_s3');
		 Route::get('/home', 'fort\HomeController@index');
		 Route::get('/', 'fort\HomeController@index');
		 Route::get('/searchajax/{link?}', 'fort\ProductCategoryController@searchajax');
		 Route::post('/searchresult', 'fort\ProductCategoryController@searchresult');
		 Route::get('/searchresult', 'fort\ProductCategoryController@searchresult');
	     Route::get('/privacy', 'fort\HomeController@privacy');
	     Route::get('/aa', 'fort\HomeController@aa');
	     Route::get('/forfavoriteajax/{id?}', 'fort\ProductCategoryController@forfavoriteajax');
	     

		
		
		//Route::get('/footer',function(){return View::make( 'zh-tw/footer'); });
	});

	/*===前台==*/

	Route::controllers([
		'auth' => 'Auth\AuthController',
		'password' => 'Auth\PasswordController',
	]);

});


