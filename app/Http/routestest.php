<?php
use App\Http\Controllers\Fantasy\BackendController as myBackEnd;
use ItemMaker as Items;
use Illuminate\Http\Request;


//轉到預設語系
Route::get('/',function(){
	return redirect( Items::url('/'.App::getLocale()) );
});

//轉到預設語系
Route::get('/Fantasy',function(){
	return redirect( Items::url('/'.App::getLocale().'/Fantasy') );
});

//轉到預設語系
Route::get('/home',function(){
	//return redirect("/hartford/".App::getLocale());
	return redirect( Items::url('/'.App::getLocale().'/Fantasy') );
	//return redirect( App::getLocale() ); 
});



/*===後台===*/

Route::group(['prefix'=>'/{locale?}'],function(){

	Route::group(['prefix'=>'/Fantasy','middleware'=> 'auth'],function(){
		Route::group(['prefix'=>'/schema'],function(){
			Route::controller('/','schemaController');
		});
		// Route::get('/{className?}','Fantasy\FantasyController@index');
		// Route::get('/{dir?}/{className?}','Fantasy\FantasyController@index');
		Route::get('/','Fantasy\FantasyController@index');
		//{addRoute}
	});

	/*===前台================================================*/
	Route::group(['prefix'=>'/'], function(){
		Route::get('/', 'HomeController@index');
	});

	/*===前台==*/

	Route::controllers([
		'auth' => 'Auth\AuthController',
		'password' => 'Auth\PasswordController',
	]);

});


