///////////////////////////////////////
//            轉場href判斷           //
///////////////////////////////////////
var aHrefChangePageEffect = function(domClass) {

    var domClass = (domClass.length > 0) ? domClass : "a";

    $(domClass).click(function(a) {
        
        a.preventDefault();
        var aHref = $(this).attr("href");
        var aTarget = $(this).attr("target"); //存取路徑
        var aArr = aHref.split("/");
        // var aArr = aHref.indexOf("/")>0?aHref.split("/"):aHref;
        var aFilePath = aArr.pop();
        var aFilePrevPath = aArr.pop();
        var aTargetPath = aFilePath.substr(0, 1); //抓取字串第一字元

        var locHref = location.href; //存取目前網址
        var locArr = locHref.split("/");
        var locFilePath = locArr.pop();
        var locFilePrevPath = locArr.pop();
        var locTargetPath = aFilePath.substr(0, 1); //抓取字串第一字元

        if (aFilePath == "") { aFilePath = "index.html"; }
        if (locFilePath == "") { locFilePath = "index.html"; }
        if (aArr == "") { aFilePrevPath = locFilePrevPath; }


        if (aTarget == "_blank") {
            window.open(aHref); //傳遞另開視窗路徑至 window.open(aHref)
            aTarget = "";
        } else if (aHref.indexOf("jpg") >= 0) {
            // console.log("donotthing");
        } else if (aFilePrevPath != locFilePrevPath) {
            cgselector.random(aHref);
            aHref = "";
        } else if (aHref.indexOf("javascript") >= 0) {
            // console.log("donotthing");
        } else if (aFilePath != locFilePath && aTargetPath != "#" && locTargetPath != "#" && aHref != "") {
            cgselector.random(aHref);
            aHref = "";
        } else if (aTargetPath == "#") {
            //jquery.smooth-scroll//
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate(
                    { scrollTop: target.offset().top },
                    { duration: 900, easing: 'easeInOutCirc' }
                );
            } //end if
        } //end else
    }) //end click function

}

$(window).load(function() {
    aHrefChangePageEffect('main a');

    /*移除首頁footer的回到最上按鈕綁定轉場的事件，因為跟fullpage.js的事件衝突*/
    /*首頁的回到最上改由fullpage.js控制*/
    if ( $('body').hasClass('home') == true ) {
        
        $('footer a').unbind('click');

    }
}); //end ready

///////////////////////////////////////
//        Animlate selecor JQ        //
///////////////////////////////////////

var cgselector = {

    random: function(href) {
        var cgnumber = Math.floor((Math.random() * 3) + 1);
        var cgnumber = 1;
        switch (cgnumber) {
            case 1:
                pagechange.CG_A(href);
                aHref = "";
                break;
            case 2:
                pagechange.CG_B(href);
                aHref = "";
                break;
            case 3:
                pagechange.CG_C(href);
                aHref = "";
                break;
            default:
        }
    }
} //end function



///////////////////////////////////////
//           cgpagechangeA           //
///////////////////////////////////////

var cube_num = 16,
    cube_array = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];

for (var bg_num = 0; bg_num < cube_num; bg_num++) {

    if ( bg_num == 0 ) {

        var cube = '<div class="cube no_show endbg_' + bg_num + '"></div>';

    }else {

        var cube = cube + '<div class="cube no_show endbg_' + bg_num + '"></div>';        

    }

}

cube_array = shuffle(cube_array);

function shuffle(array) {

    var currentIndex = array.length, temporaryValue, randomIndex;
  
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
  
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
      
    }
  
    return array;

}


var pagechange = {

    //CG A////////////
    CG_A: function(href) {

        $("body").append('<div class="endbg_frame">' + cube + '</div>');

        var cube_eq = $('div.cube'),
            cube_show = $('div.cube.show'),
            cube_noshow = $('div.cube.no_show'),
            cube_frame = $('div.endbg_frame');

        setTimeout(function() {

            cube_frame.addClass('show');
            
        }, 1000);

        setTimeout(function() {

            for (var index = 0; index < cube_array.length; index++) {

                var time = 0;

                time = time + ( 0.175 * index );

                cube_eq.eq(cube_array[index]).addClass('show').removeClass('no_show');

                cube_eq.eq(cube_array[index]).css({
                    '-webkit-transition' : 'all ' + time + 's ease',
                    '-moz-transition'    : 'all ' + time + 's ease',
                    '-ms-transition'     : 'all ' + time + 's ease',
                    '-o-transition'      : 'all ' + time + 's ease',
                    'transition'         : 'all ' + time + 's ease'
                });
                
            }
            
        }, 100);


        $("body").animate({ }, {
            queue: true,
            easing: "easeOutQuad",
            complete: function() {
                setTimeout(function() {
                    window.location.assign(href);
                }, 1300);
            } //end complate
        }); //end last animate
    } //end CG_A
} //end function
