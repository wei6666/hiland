$(document).ready(function() {
    //header and footer

    $.ajax({
            url: '../../views/layout/header.html',
        })
        .done(function(data) {
            $("header").html(data);
            load.hd();
        })

    $.ajax({
            url: '../../views/layout/footer.html',
        })
        .done(function(data) {
            $("footer").html(data);
            load.ft();
        })



    //  暫時開燈箱



 // lbox location

function lbox1(url){
  $.ajax({
    type: "GET",
    url: url,
    beforeSend: function() {
    }
  })
    .done(function(data) {

        $("body").append("<article class='hide1'><div class='lbox lbox1'></div></article>");

        $(".lbox").html(data);

                load.lbox();

                /*給燈箱一個 open 讓動畫作動*/
                if ( $(".hide1").length > 0 ) {

                    setTimeout(function(){

                        $(".hide1").addClass('open');

                    }, 300);

                }
    })
}


    var lbox1_t = false;
/*news燈箱*/
    $('.news_items li').click(function() {
        if (lbox1_t === false) {
            lbox1_t = true;
            var url = "../../views/news/news_light_box.html";
            lbox1(url);
        }
    });

/*location燈箱*/
     $('.location_a_box a').click(function() {
        if (lbox1_t === false) {
            lbox1_t = true;
            var url = "../../views/location/location_light_box.html";
            lbox1(url);
        }
    });

     $('.about01_left , .about01_right').click(function() {
        if (lbox1_t === false) {
            lbox1_t = true;
            var url = "../../views/about/about.html";
            lbox1(url);
        }
    });

    $('.about02').click(function() {
        if (lbox1_t === false) {
            lbox1_t = true;
            var url = "../../views/about/innovation.html";
            lbox1(url);
        }
    });
    $('.about03').click(function() {
        if (lbox1_t === false) {
            lbox1_t = true;
            var url = "../../views/about/production.html";
            lbox1(url);
        }
    });

    var load = {
        hd: function() {
            //header浮
            $(window).scroll(function() {
                var banner = $('#banner')
                sticky = $('header'),
                    scroll = $(window).scrollTop();
                if (scroll >= $(banner).height()) sticky.addClass('fixed_header headerFadeIn');
                else sticky.removeClass('fixed_header headerFadeIn');
            });

            //menu 旁邊暗
           
            $( ".heaer_menu li" ).mouseover(function() {
              $( this ).siblings().addClass('dark');
            });
            $( ".heaer_menu li" ).mouseout(function() {
             $( this ).siblings().removeClass('dark');
              });
            //waypoin 

            $('header').each(function(index, element) {
              $('header').waypoint(function() {
                    $('header').addClass('show');
                  }, {
                     offset: '100%'
                   });
                });


            aHrefChangePageEffect('header a');
        },
        ft: function() {
            aHrefChangePageEffect('footer a');
        },
        lbox: function() {
            aHrefChangePageEffect('footer a');
            setTimeout(function timeout() { $('.lbox1').toggleClass('on') }, 100);
             /*close*/
             $('.close,.back , .out').on('click', function() {
                  $('.lbox1').removeClass("on");
               
                   setTimeout(function timeout() {
                       $('.hide1').remove();
                         lbox1_t = false;
                        }, 1000);
             });

           /*about lightbox slick*/ 
               $('.about_light04_slick ul').slick({
                    speed: 1000,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    appendArrows: $('.light04_btn'),
                    nextArrow: $('.light04_btn_right'),
                     responsive: [
                        {
                          breakpoint: 1500,
                          settings: {
                           slidesToShow: 3,
                          }
                        },
                        {
                          breakpoint: 650,
                          settings: {
                           slidesToShow: 2,
                          }
                        },
                         {
                          breakpoint: 430,
                          settings: {
                           slidesToShow: 1,
                          }
                        }
                      ]
                });
                 
                /*about02 生技 slick*/
                $('.production03_slick ul').slick({
                    infinite: true,
                    speed: 1000,
                    centerMode: true,
                    slidesToShow: 1,
                    centerPadding: '25%',
                     responsive: [
                         {
                          breakpoint: 1300,
                          settings: {
                             centerPadding: '13%',
                          }
                        },
                        {
                          breakpoint: 1024,
                          settings: {
                             centerPadding: '9%',
                          }
                        },
                         {
                          breakpoint: 500,
                          settings: {
                             centerPadding: '50px',
                          }
                        }
                      ]
                });
                  // 下拉
                  // 下拉
                // $('.a_id_box span').on('click', function() {
                //   $(this).parent().toggleClass('open');
                //  });
                // $('.a_id_box ul').on('click', function() {
                //   $(this).parent().removeClass('open');
                //  });
                // $('.a_id_box li').off().on('click', function() {
                 //var select_title = $(this).html();
                // $(this).parent().find("span");
                // var ele = $(".span_space");
                // $(ele).html($(this).html());
                // $(this).parents('.a_id_box').removeClass('open');
                // });

                /*about02 history slick*/
                 $('.hostory_slick').slick({
                      infinite: true,
                      speed: 1000,
                      dots: false,
                      slidesToShow: 2,
                      appendArrows: $('.histiry_btn'),
                      prevArrow: $('.histiry_btn_left'),
                      nextArrow: $('.histiry_btn_right'),
                       responsive: [
                          {
                            breakpoint: 500,
                            settings: {
                             slidesToShow: 1,
                            }
                          }
                        ]
                  });
           //close line
            $.fn.tinyMapConfigure({
                'key': 'AIzaSyCBdj38bLwCC5OJAwtfDCgAUuSUZLDjLVs'
             });
            $('.map').tinyMap({
                  
                        'center': ['22.687792', '120.351160'],
                        'zoom':15,
                         'marker': [
        {   'id':'location01',
            'addr': ['22.687792', '120.351160'],
            'text': '<strong>812高雄市仁武區鳳仁路95之32號</strong>',
            // 'newLabel': '文字標籤',
            'newLabelCSS': 'labels',
           
            // 動畫效果
            'animation': 'DROP',
          
        }
        
         ]
        });

        },
    }


    /*news*/

    $(".select_ul li").click(function() {
        $(this).siblings().removeClass('on')
        $(this).addClass('on');
    });
    /*location*/

    $(".con_content li").click(function() {
        $(this).siblings().removeClass('on')
        $(this).toggleClass('on');
    });
    /*faq*/

    $(".faq_content li").click(function() {
        $(this).siblings().removeClass('on');
        $(this).toggleClass('on');
    });


    /*favorite*/

    $(".sex div").click(function() {
        $(this).siblings().removeClass('on')
        $(this).addClass('on');
    });

    /*favorite 下拉*/      
    $(".contact_list a").click(function() {
        $('.favorite_line').addClass('on');
        $('.favorite_from_title').addClass('on');
        $('.favorite_from_content').addClass('on');
    });
   
    /*style*/

    /*style九宮格*/
    if ($(window).width() >= 1024) {

         $(".stylelist_nine_a").mouseenter(function(){
        $(this).addClass('on');
        $(this).siblings().addClass('da');
        $('.stylelist_bg').addClass('da');
           });
    $(".stylelist_nine_a").mouseleave(function(){
        $(this).removeClass('on');
        $(this).siblings().removeClass('da');
        $('.stylelist_bg').removeClass('da');
            });

    }else{
        $(".stylelist_nine_a").click(function() {
        $(this).toggleClass('on');
        $(this).siblings().removeClass('on');
        $(this).siblings().removeClass('da');
           });

    }


  
   if ($(window).width() >= 1024) {

    }else{
        $(".stylelist_slickinner").click(function() {
        $(this).children().addClass('da');
           });
    }
  

   
    /*style 九宮格 輪播*/
     $('.stylelist_slick ul').slick({
        dots: true,
        infinite: true,
        fade:true,
        // speed: 2000,
        // autoplay: true,
        dotsClass:$(".style_dotnumber"),
        centerMode: true,
        slidesToScroll: 1,
        appendDots:$(".style_dotnumber"),
        pauseOnDotsHover:true,
        appendArrows: $('.style_btn_box'),
        prevArrow: $('.style_btn_left'),
        nextArrow: $('.style_btn_right'),
        asNavFor: '.style_rwdnumber ul',
         responsive: [
            {
              breakpoint: 1200,
              settings: {
                dots: false,
              }
            }
          ]
    });
     
     $('.style_rwdnumber ul').slick({
        infinite: true,
        speed: 900,
        dots: false,
        centerMode: true,
        slidesToShow: 3,
        centerPadding: '0px',
        asNavFor: '.stylelist_slick ul',
    });

    
    /*style 九宮格 輪播 號碼*/
    var $style_dotnumber = $('.style_dotnumber  ul');
    $style_dotnumber.css({'position':'absolute','left':'calc( 50% - 25px)','transition':'.5s'}) 
    $('.stylelist_slick ul').on('beforeChange', function(event, slick, currentSlide, nextSlide){
    var move = nextSlide * 50
    $style_dotnumber.css('transform','translateX(-'+ move +'px)');
     });

    
    /*menu*/

     $('.openmenu_slick ul').slick({
        infinite: true,
        autoplay:true,
        speed: 1000,
        vertical:true,
        slidesToShow: 3,
    });

      /*rwdmenu下拉*/

      // 下拉
     $('.a_id_box span').on('click', function() {
         $(this).parent().toggleClass('open');
     });
      $('.a_id_box ul').on('click', function() {
         $(this).parent().removeClass('open');
     });
      $('.a_id_box li').off().on('click', function() {
        //var select_title = $(this).html();
        $(this).parent().find("span");
        var ele = $(".span_space");
        $(ele).html($(this).html());
        $(this).parents('.a_id_box').removeClass('open');
     });


});