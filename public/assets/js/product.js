$(document).ready(function() {

    /*masonry*/
    var grid = $('.grid');

    /*rwd時判斷螢幕尺寸決定是否啟動瀑布流*/
    $(window).resize(function(){

        if ( $(window).width() > 767 ) {
            
            active_masonry(); 
            
        }else {

            destory_masonry();

        }

    });


    /*畫面載入時判斷螢幕尺寸決定是否啟動瀑布流*/
    if ( $(window).width() > 767 ) {
        
        active_masonry(); 
        
    }

    /*啟動瀑布流 含式*/
    function active_masonry(){

        grid.masonry({
            itemSelector: '.grid-item',
            columnWidth: 1,
        });

        /*+一個class做為 瀑布流是否已經啟動依據*/
        grid.addClass('actived');
        
    }

    /*摧毀瀑布流 含式*/
    function destory_masonry(){

        /*判斷已啟動就刪除class避免重複執行 並摧毀瀑布流*/
        if ( grid.hasClass('actived') ) {

            grid.removeClass('actived');

            grid.masonry('destroy');
            
        }

    }



    /*我的最愛按鈕含式*/
    favor_btn();

});
