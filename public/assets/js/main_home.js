$(document).ready(function() {
 /*===================home==========*/
    /*home01*/
    $('.home_slick').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 900,
      autoplay: true,
      fade: true,
    });

     /*home02 number*/
     $('.slick02_number ul').slick({
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      asNavFor: '.home2_img_slick ul',
    });
    
     /*home02 圖*/
     $('.home2_img_slick ul').slick({
      infinite: true,
      fade:true,
      slidesToShow: 1,
      asNavFor: '.slick02_number ul',
      appendArrows: $('.s02_btn'),
      prevArrow: $('.s02_btn_left'),
       nextArrow: $('.s02_btn_right'),
    });
      /*home02 背景*/
     $('.home2_imgbg_slick ul').slick({
      infinite: true,
      fade:true,
      slidesToScroll: 1,
      asNavFor: '.home02_note ul',
    });
    
     /*home02 文字*/
     $('.home02_note ul').slick({
      infinite: true,
      fade:true,
      slidesToShow: 1,
      asNavFor: '.home2_imgbg_slick ul',
    });

    
   

/*home02產品/背景/文字/數字 連動 */

 $('.home2_imgbg_slick ul').on('afterChange', function(event, slick, direction) {
        var index = $(this).slick("getSlick").slickCurrentSlide();
        if ($('.slick02_number ul').slick('slickCurrentSlide') !== index) {
            $('.slick02_number ul').slick('slickGoTo', index);
        }
    });

    $('.slick02_number ul').on('afterChange', function(event, slick, direction) {
        var index = $(this).slick("getSlick").slickCurrentSlide();
        if ($('.home2_imgbg_slick ul').slick('slickCurrentSlide') !== index) {
            $('.home2_imgbg_slick ul').slick('slickGoTo', index);
        }
    });

     /*home02 number*/
     $('.slick04_number ul').slick({
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      asNavFor: '.home04_img_slick ul',
    });
    
     /*home02 圖*/
     $('.home04_img_slick ul').slick({
      infinite: true,
      fade:true,
      slidesToShow: 1,
      asNavFor: '.slick04_number ul',
      appendArrows: $('.s04_btn'),
      prevArrow: $('.s04_btn_left'),
       nextArrow: $('.s04_btn_right'),
    });
      /*home04 背景*/
     $('.home04_bg ul').slick({
      infinite: true,
      fade:true,
      slidesToScroll: 1,
      asNavFor: '.home04_note ul',
    });
    
     /*home04 文字*/
     $('.home04_note ul').slick({
      infinite: true,
      fade:true,
      slidesToShow: 1,
      asNavFor: '.home04_bg ul',
    });


/*home04產品/背景/文字/數字 連動 */

   $('.slick04_number ul').on('afterChange', function(event, slick, direction) {
        var index = $(this).slick("getSlick").slickCurrentSlide();
        if ($('.home04_bg ul').slick('slickCurrentSlide') !== index) {
            $('.home04_bg ul').slick('slickGoTo', index);
        }
    });

    $('.home04_bg ul').on('afterChange', function(event, slick, direction) {
        var index = $(this).slick("getSlick").slickCurrentSlide();
        if ($('.slick04_number ul').slick('slickCurrentSlide') !== index) {
            $('.slick04_number ul').slick('slickGoTo', index);
        }
    });


/*home03對分*/
var min = 3;
var max = 36000;
var mainmin = 3;

var window_w = $(window).width(),
    block_width = $('.home03_frame_block.up');

$('#split-bar, .bar_ball').mousedown(function (e) {

    e.preventDefault();

    $(document).mousemove(function (e) {
        e.preventDefault();
        var x = e.pageX - $('#sidebar').offset().left;
        
        if (x > min && x < max && e.pageX < ($(window).width() - mainmin)) {  
          $('#sidebar').css("width", x);
        }

        /*拖拉按鈕跟著滑鼠跑*/
        var drag_btn = e.pageX - 40;

        if ( drag_btn < window_w && drag_btn > 0 ) {

            $('#split-bar').css("left",drag_btn);
            
        }

        /*base區塊 往左滑時 a的點擊範圍不會跟著變大*/
        /*所以用JS讓拖拉開始後把右邊區塊寬度改0，讓兩邊區塊放大點擊區域也變大*/
        if ( block_width.hasClass('change_width') == false ) {

            block_width.addClass('change_width');
            
        }
    })

});

$(document).mouseup(function (e) {
    
    $(document).unbind('mousemove');

});


/* home05下拉*/
$(".a_id_box ul").mCustomScrollbar({
                 scrollButtons: {
                 enable: true
                  }
              });

});




/**********************************************************************************************/
/*首頁區塊滑動*/
$(document).ready(function() {

    /**/
    if ( $(window).width() > 1024 ) {

        $('#fullpage').fullpage({
            
            /*data-anchor 可以拿來做區塊點擊移動*/
            anchors: ['top',],
    
            /*滚动前的回调函数*/
            onLeave: function(index, nextIndex, direction){
    
                if ( $( '.home0' + index ).hasClass('no_remove_show') == false ) {
    
                    $( '.home0' + index ).removeClass('show');
    
                }
    
                $( '.home0' + nextIndex ).addClass('show');
    
            },
    
        });
        
    }
    
    
});
/**********************************************************************************************/