@extends('Fantasy.template')

@section('title',"類別項目修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			類別項目 <small>Newscatagories Manager</small> </h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">類別項目</a> <i class="fa fa-angle-right"></i>
				</li>
				<li>
					類別項目修改 </li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

					<!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 類別項目 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> Information </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'News[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

												
                                                  

                                                {{ItemMaker::radio_btn([
                                                        'labelText' => '是否顯示',
                                                        'inputName' => 'News[is_visible]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
                                                    ])}}

                                                {{ItemMaker::textInput([
                                                        'labelText' => '排序',
                                                        'inputName' => 'News[rank]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['rank']) )? $data['rank'] : ''
                                                    ])}}

                                              <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>列表頁設定</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                              </div>


                                               {{ItemMaker::textInput([
                                                        'labelText' => '標題',
                                                        'inputName' => 'News[out_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['out_title']) )? $data['out_title'] : ''
                                                    ])}}

                                                 {{ItemMaker::textArea([
                                                        'labelText' => '內容',
                                                        'inputName' => 'News[out_content]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['out_content']) )? $data['out_content'] : ''
                                                    ])}}

                                                 {{ItemMaker::photo([
                                                        'labelText' => '圖片(470 x 320)',
                                                        'inputName' => 'News[out_img]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['out_img']) )? $data['out_img'] : ''
                                                    ])}}

                                                {{ItemMaker::textInput([
                                                        'labelText' => 'hover時標題',
                                                        'inputName' => 'News[out_readmoretag]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['out_readmoretag']) )? $data['out_readmoretag'] : ''
                                                    ])}}

                                                 <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>詳細頁設定</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                              </div>

                                               {{ItemMaker::textInput([
                                                        'labelText' => '內頁標題',
                                                        'inputName' => 'News[in_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['in_title']) )? $data['in_title'] : ''
                                                    ])}}

                                                {{ItemMaker::photo([
                                                        'labelText' => '內頁大圖(1000 x 500)',
                                                        'inputName' => 'News[in_big_photo]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['in_big_photo']) )? $data['in_big_photo'] : ''
                                                    ])}}

                                              {{ItemMaker::textArea([
                                                        'labelText' => '大圖下內容',
                                                        'inputName' => 'News[in_content1]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['in_content1']) )? $data['in_content1'] : ''
                                                    ])}}

                                             {{ItemMaker::photo([
                                                        'labelText' => '文繞圖圖片(450 x 500)',
                                                        'inputName' => 'News[in_small_photo]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['in_small_photo']) )? $data['in_small_photo'] : ''
                                                    ])}}

                                             {{ItemMaker::textArea([
                                                        'labelText' => '文繞圖內容',
                                                        'inputName' => 'News[in_content2]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['in_content2']) )? $data['in_content2'] : ''
                                                    ])}}

                                            {{ItemMaker::textArea([
                                                        'labelText' => '最底部內容',
                                                        'inputName' => 'News[in_content3]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['in_content3']) )? $data['in_content3'] : ''
                                                    ])}}

                                            {{ItemMaker::DatePicker([
                                                        'labelText' => '文章日期',
                                                        'inputName' => 'News[date]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['date']) )? $data['date'] : ''
                                                    ])}}

                                            {{ItemMaker::DatePicker([
                                                        'labelText' => '文章開始日期',
                                                        'inputName' => 'News[st_date]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['st_date']) )? $data['st_date'] : ''
                                                    ])}}

                                            {{ItemMaker::DatePicker([
                                                        'labelText' => '文章結束日期',
                                                        'inputName' => 'News[ed_date]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['ed_date']) )? $data['ed_date'] : ''
                                                    ])}}

                                            



                                                </div>
                                            </div>


                                           





                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

		});

	</script>
@stop
