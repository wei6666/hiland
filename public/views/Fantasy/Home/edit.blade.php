@extends('Fantasy.template')

@section('title',"類別項目修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			類別項目 <small>Newscatagories Manager</small> </h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">類別項目</a> <i class="fa fa-angle-right"></i>
				</li>
				<li>
					類別項目修改 </li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

					<!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 類別項目 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> Information </a>
                                            </li>
                                             <li >
                                                <a href="#tab_genera2" data-toggle="tab"> 首頁banner產品照片輪播 </a>
                                            </li>

                                           
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'Home[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

												
                                                  

                                             {{--    {{ItemMaker::radio_btn([
                                                        'labelText' => '是否顯示',
                                                        'inputName' => 'Home[is_visible]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
                                                    ])}} --}}

                                                {{ItemMaker::photo([
                                                        'labelText' => '首頁banner上標籤<br>(80 x 80)',
                                                        'inputName' => 'Home[home_tag]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['home_tag']) )? $data['home_tag'] : ''
                                                    ])}}                                    


                                            {{--      {{ItemMaker::textInput([
                                                        'labelText' => '首頁banner標題',
                                                        'inputName' => 'Home[title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['title']) )? $data['title'] : ''
                                                    ])}} 


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '首頁banner副標題',
                                                        'inputName' => 'Home[sub_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['sub_title']) )? $data['sub_title'] : ''
                                                    ])}}   --}}

                                                     



                                                     <?php 

                                                use App\Http\Models\Style\Style;
                                              

                                              
                                               
                                                $allcate=[];

                                                
                                                    $datacate=Style::where("is_visible",1)
                                                    ->OrderBy('rank','asc')
                                                    ->get()
                                                    ->toArray();


                                                    foreach($datacate as $value1)
                                                    {
                                                       
                                                    $allcate[$value1['out_sub_title']]=$value1['id'];
                                                    }
                                                

                                                
                                                $productdata=( !empty( $data['style_id'] ) )?json_decode($data['style_id'], true) : [];
    
                                              
                                               
                                                ?>

                                                {{ItemMaker::selectMulti([
                                                        'labelText' => '黃山石風格<br>(圖片區塊會與風格<br>內頁之圖片區塊排序<br>最前圖片設定相同)',
                                                        'inputName' => 'Home[style_id][]',
                                                        'helpText' =>'使用者帳號與聯絡用信箱',
                                                        "options" => $allcate,
                                                        "type" => "Role",
                                                        'value' => ( !empty($productdata) )? $productdata : ''
                                                    ])}}


                                                 <?php 

                                                use App\Http\Models\Product\ProductBig;
                                              

                                              
                                               
                                                $allcate=[];

                                                
                                                    $datacate=ProductBig::where("is_visible",1)
                                                    ->OrderBy('rank','asc')
                                                    ->get()
                                                    ->toArray();


                                                    foreach($datacate as $value1)
                                                    {
                                                       
                                                    $allcate[$value1['title']]=$value1['id'];
                                                    }
                                                

                                                
                                                $productdata=( !empty( $data['big_id'] ) )?json_decode($data['big_id'], true) : [];
    
                                              
                                               
                                                ?>

                                                {{ItemMaker::selectMulti([
                                                        'labelText' => '首頁產品系列區塊設定',
                                                        'inputName' => 'Home[big_id][]',
                                                        'helpText' =>'使用者帳號與聯絡用信箱',
                                                        "options" => $allcate,
                                                        "type" => "Role",
                                                        'value' => ( !empty($productdata) )? $productdata : ''
                                                    ])}}



                                                 <?php 

                                                use App\Http\Models\Product\ProductCategory;
                                                use App\Http\Models\Product\LedCategory;

                                              
                                                $cate=ProductBig::where("is_visible",1)
                                                ->OrderBy('rank','asc')
                                                ->get();

                                                $allcate=[];

                                                foreach($cate as $key => $value)
                                                {
                                                    $datacate=ProductCategory::where("big_id",$value->id)
                                                    ->OrderBy('rank','asc')
                                                    ->get()
                                                    ->toArray();


                                                    foreach($datacate as $value1)
                                                    {
                                                       
                                                    $allcate[$value->title][$value1['title']]=$value1['id'];
                                                    }
                                                }

                                                
                                                $productdata=( !empty( $data['small_id'] ) )?json_decode($data['small_id'], true) : [];
    
                                              
                                               
                                                ?>

                                                {{ItemMaker::selectMulti([
                                                        'labelText' => '首頁產品次系列區塊設定',
                                                        'inputName' => 'Home[small_id][]',
                                                        'helpText' =>'使用者帳號與聯絡用信箱',
                                                        "options" => $allcate,
                                                        "type" => "Role",
                                                        'value' => ( !empty($productdata) )? $productdata : ''
                                                    ])}}


                                                    {{ItemMaker::photo([
                                                        'labelText' => '哪裡購買區塊背景圖<br>(1920 x 900)',
                                                        'inputName' => 'Home[where_photo]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['where_photo']) )? $data['where_photo'] : ''
                                                    ])}}   






                                                </div>
                                            </div>



                                            <div class="tab-pane  form-row-seperated" id="tab_genera2">
                                                <div class="form-body form_news_pad">

                                              {{ FormMaker::photosTable(
                                                    [
                                                        'nameGroup' => $routePreFix.'-HomeImage',
                                                        'datas' => ( !empty($parent['has']['HomeImage']) )? $parent['has']['HomeImage'] : [],
                                                        'table_set' =>  $bulidTable['HomeImage'],
                                                        'pic' => false
                                                    ]
                                                 ) }}
                                 

                                                   </div>
                                            </div>


                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

		});

        $("#itemTbody > tr > td > input").css("width",'250px');

	</script>
@stop
