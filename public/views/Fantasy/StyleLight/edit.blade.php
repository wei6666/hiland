@extends('Fantasy.template')

@section('title',"新聞內容項目修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    #itemTbody div.images_icon2 { display: none; }
    </style>
      <style type="text/css">
  .page-sidebar-wrapper, .page-header, .page-title, .page-bar { display: none; }
     .page-content-wrapper .page-content { margin: 0px; padding: 0px; }
     .page-content { min-height: 700px!important; }
     .page-header-fixed .page-container { margin: 0px!important; }
     .fantasy-selectImageBtn { display: none; }
     div.actions > a { display: none; }
     div.page-bar { display: none; }
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
            新聞內容項目 <small>NewsContent Manager</small> </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{ ItemMaker::url('Fantasy/')}}">About</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">新聞內容項目</a> <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    新聞內容項目修改 </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->


            <div class="row">
                <div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

                    <!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
                    {!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
                                            {{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 新聞內容項目 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> Information </a>
                                            </li>
                                           
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

                                                       {{ItemMaker::idInput([
                                                        'inputName' => 'StylePhoto[id]',
                                                        'value' => ( !empty($data['id']) )? $data['id'] : ''
                                                    ])}} 


                                                    {{ItemMaker::select([
                                                        'labelText' => '圖片區塊1產品內容',
                                                        'inputName' => 'StylePhoto[has_product1]',
                                                        //'required'  => true,
                                                        'options'   => $parent['belong']['Product'],
                                                        'value' => ( !empty($data['has_product1']) )? $data['has_product1'] : ''
                                                    ])}}


                                                     {{ItemMaker::select([
                                                        'labelText' => '圖片區塊2產品內容',
                                                        'inputName' => 'StylePhoto[has_product2]',
                                                        //'required'  => true,
                                                        'options'   => $parent['belong']['Product'],
                                                        'value' => ( !empty($data['has_product2']) )? $data['has_product2'] : ''
                                                    ])}}

                                                     {{ItemMaker::select([
                                                        'labelText' => '圖片區塊3產品內容',
                                                        'inputName' => 'StylePhoto[has_product3]',
                                                        //'required'  => true,
                                                        'options'   => $parent['belong']['Product'],
                                                        'value' => ( !empty($data['has_product3']) )? $data['has_product3'] : ''
                                                    ])}}

                                                     {{ItemMaker::select([
                                                        'labelText' => '圖片區塊4產品內容',
                                                        'inputName' => 'StylePhoto[has_product4]',
                                                        //'required'  => true,
                                                        'options'   => $parent['belong']['Product'],
                                                        'value' => ( !empty($data['has_product4']) )? $data['has_product4'] : ''
                                                    ])}}

                                                     {{ItemMaker::select([
                                                        'labelText' => '圖片區塊5產品內容',
                                                        'inputName' => 'StylePhoto[has_product5]',
                                                        //'required'  => true,
                                                        'options'   => $parent['belong']['Product'],
                                                        'value' => ( !empty($data['has_product5']) )? $data['has_product5'] : ''
                                                    ])}}

                                                     {{ItemMaker::select([
                                                        'labelText' => '圖片區塊6產品內容',
                                                        'inputName' => 'StylePhoto[has_product6]',
                                                        //'required'  => true,
                                                        'options'   => $parent['belong']['Product'],
                                                        'value' => ( !empty($data['has_product6']) )? $data['has_product6'] : ''
                                                    ])}}

                                                     {{ItemMaker::select([
                                                        'labelText' => '圖片區塊7產品內容',
                                                        'inputName' => 'StylePhoto[has_product7]',
                                                        //'required'  => true,
                                                        'options'   => $parent['belong']['Product'],
                                                        'value' => ( !empty($data['has_product7']) )? $data['has_product7'] : ''
                                                    ])}}

                                                     {{ItemMaker::select([
                                                        'labelText' => '圖片區塊8產品內容',
                                                        'inputName' => 'StylePhoto[has_product8]',
                                                        //'required'  => true,
                                                        'options'   => $parent['belong']['Product'],
                                                        'value' => ( !empty($data['has_product8']) )? $data['has_product8'] : ''
                                                    ])}}

                                                     {{ItemMaker::select([
                                                        'labelText' => '圖片區塊9產品內容',
                                                        'inputName' => 'StylePhoto[has_product9]',
                                                        //'required'  => true,
                                                        'options'   => $parent['belong']['Product'],
                                                        'value' => ( !empty($data['has_product9']) )? $data['has_product9'] : ''
                                                    ])}}

                                                    


                                                    


                                                </div>
                                            </div>




                                        </div>
                                    </div>
                                </div>
                            </div>
                        <iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
                    <!--</form>
                     END FORM-->
                     {!! Form::close() !!}
                </div>
            </div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

    <script>
        $(document).ready(function() {

            FilePicke();
            Rank();
            //資料刪除
            dataDelete();

            colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif



        });

    </script>
@stop
