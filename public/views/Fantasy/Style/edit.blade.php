@extends('Fantasy.template')

@section('title',"類別項目修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

       


            <div class="row">
                <div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

                    <!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
                    {!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
                                            {{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 類別項目 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> Information </a>
                                            </li>
                                             <li >
                                                <a href="#tab_genera2" data-toggle="tab"> 產品照片輪播 </a>
                                            </li>

                                           
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

                                                    {{ItemMaker::idInput([
                                                        'inputName' => 'Style[id]',
                                                        'value' => ( !empty($data['id']) )? $data['id'] : ''
                                                    ])}}

                                                <div class="form-group" style='background-color:lightblue; idth: 100%;  margin-left: 0;'>
                                                <div  ><h4><b>列表頁資料設定</b></h4></div>
                                                <div class="col-md-10"></div>
                                                </div>
                                                  

                                                {{ItemMaker::radio_btn([
                                                        'labelText' => '是否顯示',
                                                        'inputName' => 'Style[is_visible]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
                                                    ])}}

                                                {{ItemMaker::textInput([
                                                        'labelText' => '排序',
                                                        'inputName' => 'Style[rank]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['rank']) )? $data['rank'] : ''
                                                    ])}}
                                                   

                                                      {{ItemMaker::textInput([
                                                        'labelText' => '產品標題',
                                                        'inputName' => 'Style[out_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['out_title']) )? $data['out_title'] : ''
                                                    ])}}

                                                     {{ItemMaker::textInput([
                                                        'labelText' => '產品副標題',
                                                        'inputName' => 'Style[out_sub_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['out_sub_title']) )? $data['out_sub_title'] : ''
                                                    ])}}

                                                    

                                                    {{ItemMaker::textInput([
                                                        'labelText' => 'view more按鈕標題',
                                                        'inputName' => 'Style[more_btn_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['more_btn_title']) )? $data['more_btn_title'] : ''
                                                    ])}}


                                                    {{ItemMaker::photo([
                                                        'labelText' => '產品圖片<br>(730 x 900)',
                                                        'inputName' => 'Style[out_image]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['out_image']) )? $data['out_image'] : ''
                                                    ])}}


                                                    {{ItemMaker::photo([
                                                        'labelText' => '風格頁檢視更多圖片<br>(570 x 740)',
                                                        'inputName' => 'Style[more_image]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['more_image']) )? $data['more_image'] : ''
                                                    ])}}


                                                <div class="form-group" style='background-color:lightblue; idth: 100%;  margin-left: 0;'>
                                                <div  ><h4><b>詳細頁資料設定</b></h4></div>
                                                <div class="col-md-10"></div>
                                                </div>


                                                {{ItemMaker::textInput([
                                                        'labelText' => '標題',
                                                        'inputName' => 'Style[in_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['in_title']) )? $data['in_title'] : ''
                                                    ])}}


                                                    {{ItemMaker::textArea([
                                                        'labelText' => '副標題',
                                                        'inputName' => 'Style[in_sub_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['in_sub_title']) )? $data['in_sub_title'] : ''
                                                    ])}}

                                                <div class="form-group" style='background-color:lightblue; idth: 100%;  margin-left: 0;'>
                                                <div  ><h4><b>首頁資料設定<br><sub>該部分資料將顯示在首頁上的風格區塊<br>  若首頁沒有有選取這則資料則不用上資料</sub></b></h4></div>
                                                <div class="col-md-10"></div>
                                                </div>


                                                 {{ItemMaker::photo([
                                                        'labelText' => '切換時的背景圖<br>1920 x 1080',
                                                        'inputName' => 'Style[home_backgroung]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['home_backgroung']) )? $data['home_backgroung'] : ''
                                                    ])}}


                                                     {{ItemMaker::textInput([
                                                        'labelText' => '顯示字體顏色<br>(可搜尋色碼改變顏色<br>#000000->白色<br>#ffffff->黑色)',
                                                        'inputName' => 'Style[color]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['color']) )? $data['color'] : ''
                                                    ])}}


                                                     {{ItemMaker::textInput([
                                                        'labelText' => '首頁標題',
                                                        'inputName' => 'Style[home_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['home_title']) )? $data['home_title'] : ''
                                                    ])}}


                                                     {{ItemMaker::textInput([
                                                        'labelText' => '首頁副標題',
                                                        'inputName' => 'Style[home_sub_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['home_sub_title']) )? $data['home_sub_title'] : ''
                                                    ])}}

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '首頁內容',
                                                        'inputName' => 'Style[home_content]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['home_content']) )? $data['home_content'] : ''
                                                    ])}}

                                                     {{ItemMaker::textInput([
                                                        'labelText' => '首頁more按鈕標題',
                                                        'inputName' => 'Style[home_more_btn]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['home_more_btn']) )? $data['home_more_btn'] : ''
                                                    ])}}




                                                </div>
                                            </div>



                                            <div class="tab-pane  form-row-seperated" id="tab_genera2">
                                                <div class="form-body form_news_pad">

                                              {{ FormMaker::photosTable(
                                                    [
                                                        'nameGroup' => $routePreFix.'-StylePhoto',
                                                        'datas' => ( !empty($parent['has']['StylePhoto']) )? $parent['has']['StylePhoto'] : [],
                                                        'table_set' =>  $bulidTable['photo'],
                                                        'pic' => false
                                                    ]
                                                 ) }}
                                 

                                                   </div>
                                            </div>


                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
                    <!--</form>
                     END FORM-->
                     {!! Form::close() !!}
                </div>
            </div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

    <script>
        $(document).ready(function() {

            FilePicke();
            Rank();
            //資料刪除
            dataDelete();

            colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

        });

        $('input#go_news_content').off('click').on('click',function(){

            var id=$(this).parents('tr').children('td:first').find('input').eq(0).val();
         
            url='{{ ItemMaker::url('Fantasy/Style/StyleLight/edit') }}/'+id;
        
            console.log(url);

            $(this).colorbox({
                href: url,
                iframe:true,
                width:"80%",
                height:"80%",
                overlayClose: false,
                onClosed: function(){  
                    //location.reload();
                    
                },
                onComplete: function() {
                    $('#cboxClose, #cboxOverlay').on('click', function() {
                        $('html').css('overflow-y','auto');
                    });

                }
            });

            $('html').css('overflow-y','hidden');
        });

        // $('#text-add').attr('onclick','setTimeout(function(){ $("button[type=submit]").click(); }, 500);');

    </script>
@stop
