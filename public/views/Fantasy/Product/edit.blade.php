@extends('Fantasy.template')

@section('title',"類別項目修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			類別項目 <small>Newscatagories Manager</small> </h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">類別項目</a> <i class="fa fa-angle-right"></i>
				</li>
				<li>
					類別項目修改 </li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

					<!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 類別項目 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> Information </a>
                                            </li>
                                             <li >
                                                <a href="#tab_genera2" data-toggle="tab"> 產品照片輪播 </a>
                                            </li>

                                           
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'Product[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

												
                                                  

                                                {{ItemMaker::radio_btn([
                                                        'labelText' => '是否顯示',
                                                        'inputName' => 'Product[is_visible]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
                                                    ])}}

                                                {{ItemMaker::textInput([
                                                        'labelText' => '排序',
                                                        'inputName' => 'Product[rank]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['rank']) )? $data['rank'] : ''
                                                    ])}}
                                                    <?php  

                                                   
                                                    $imgtype=[
                                                        1=>['id'=>1 ,'value'=>1 ,'title'=>'正方形(大) 300 x 300'],
                                                        2=>['id'=>2 ,'value'=>2 ,'title'=>'長方形(長) 450 x 150'],
                                                        3=>['id'=>3 ,'value'=>3 ,'title'=>'長方形(短)300 x 150'],
                                                        4=>['id'=>4 ,'value'=>4 ,'title'=>'正方形(短)150 x 150'],
                                                        5=>['id'=>5 ,'value'=>5 ,'title'=>'長方形(細長)450 x 75'],
                                                        6=>['id'=>6 ,'value'=>6 ,'title'=>'長方形(細短)300 x 75'],
                                                        7=>['id'=>7 ,'value'=>7 ,'title'=>'六角形(細短)300 x 260'],
                                                        8=>['id'=>8 ,'value'=>8 ,'title'=>'長方形(大)450 x 225'],
                                                    ];


                                                     $imgshape=[
                                                        1=>['id'=>1 ,'value'=>1 ,'title'=>'正方形'],
                                                        2=>['id'=>2 ,'value'=>2 ,'title'=>'長方形'],
                                                        3=>['id'=>3 ,'value'=>3 ,'title'=>'六角形'],
                                                        
                                                      
                                                                                                   
                                                    ];

                                                    ?>
                                                


                                                 {{ItemMaker::select([
                                                        'labelText' => '所屬類別',
                                                        'inputName' => 'Product[category_id]',
                                                        'required'  => true,
                                                        'options'   => $parent['belong']['ProductCategory'],
                                                        'value' => ( !empty($data['category_id']) )? $data['category_id'] : ''
                                                    ])}}

                                                    {{ItemMaker::select([
                                                        'labelText' => '圖片尺寸',
                                                        'inputName' => 'Product[out_img_type]',
                                                        'required'  => true,
                                                        'options'   => $imgtype,
                                                        'value' => ( !empty($data['out_img_type']) )? $data['out_img_type'] : ''
                                                    ])}}


                                                    {{ItemMaker::select([
                                                        'labelText' => '圖片形狀',
                                                        'inputName' => 'Product[shape]',
                                                        'required'  => true,
                                                        'options'   => $imgshape,
                                                        'value' => ( !empty($data['shape']) )? $data['shape'] : ''
                                                    ])}}




                                                    {{ItemMaker::photo([
                                                        'labelText' => '列表頁圖片<br>(圖片尺寸參考所選的圖片尺寸)',
                                                        'inputName' => 'Product[out_image]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['out_image']) )? $data['out_image'] : ''
                                                    ])}}


                                                    {{--  {{ItemMaker::photo([
                                                        'labelText' => '詳細頁圖片',
                                                        'inputName' => 'Product[in_image]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['in_image']) )? $data['in_image'] : ''
                                                    ])}} --}}

                                                      {{ItemMaker::textInput([
                                                        'labelText' => '產品編號',
                                                        'inputName' => 'Product[pro_num]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['pro_num']) )? $data['pro_num'] : ''
                                                    ])}}
                                                      {{ItemMaker::textInput([
                                                        'labelText' => '產品尺寸<br>(300mm x 150mm x 9.5mm)<br>會影響篩選的尺寸選項<br>建議複製括號內格式改數字',
                                                        'inputName' => 'Product[size]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['size']) )? $data['size'] : ''
                                                    ])}}
                                                      {{ItemMaker::textInput([
                                                        'labelText' => '表面',
                                                        'inputName' => 'Product[face]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['face']) )? $data['face'] : ''
                                                    ])}}
                                                      {{ItemMaker::textInput([
                                                        'labelText' => '規格',
                                                        'inputName' => 'Product[spec]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['spec']) )? $data['spec'] : ''
                                                    ])}}
                                                      {{ItemMaker::textInput([
                                                        'labelText' => '厚度',
                                                        'inputName' => 'Product[thickness]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['thickness']) )? $data['thickness'] : ''
                                                    ])}}
                                                      {{ItemMaker::textInput([
                                                        'labelText' => '包裝數量',
                                                        'inputName' => 'Product[pack_num]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['pack_num']) )? $data['pack_num'] : ''
                                                    ])}}
                                                  
                                                      {{ItemMaker::textInput([
                                                        'labelText' => '換算數量',
                                                        'inputName' => 'Product[count_num]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['count_num']) )? $data['count_num'] : ''
                                                    ])}}
                                                      {{ItemMaker::textInput([
                                                        'labelText' => '施工用量',
                                                        'inputName' => 'Product[work_num]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['work_num']) )? $data['work_num'] : ''
                                                    ])}}


                                                     {{ItemMaker::photo([
                                                        'labelText' => '產品花紋<br>(400 x 250)<br>用於風格與首頁在輪播圖<br>九宮格時出現的花紋',
                                                        'inputName' => 'Product[for_night_img]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['for_night_img']) )? $data['for_night_img'] : ''
                                                    ])}}
                                                    

                                            



                                                </div>
                                            </div>



                                            <div class="tab-pane  form-row-seperated" id="tab_genera2">
                                                <div class="form-body form_news_pad">

                                              {{ FormMaker::photosTable(
                                                    [
                                                        'nameGroup' => $routePreFix.'-ProductPhoto',
                                                        'datas' => ( !empty($parent['has']['ProductPhoto']) )? $parent['has']['ProductPhoto'] : [],
                                                        'table_set' =>  $bulidTable['photo'],
                                                        'pic' => false
                                                    ]
                                                 ) }}
                                 

                                                   </div>
                                            </div>


                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

		});

	</script>
@stop
