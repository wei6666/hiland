@extends('Fantasy.template')

@section('title',"產品類別設定修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			產品類別設定 <small>Newscatagories Manager</small> </h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">產品類別設定</a> <i class="fa fa-angle-right"></i>
				</li>
				<li>
					產品類別設定修改 </li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

					<!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 產品類別設定 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> 問題與答案設定 </a>
                                            </li>

                                        
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'ProductCategory[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

												<div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>產品類別列表頁設定</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                                    </div>

                                                     {{ItemMaker::select([
                                                        'labelText' => '所屬類別',
                                                        'inputName' => 'ProductCategory[big_id]',
                                                        'required'  => true,
                                                        'options'   => $parent['belong']['ProductBig'],
                                                        'value' => ( !empty($data['big_id']) )? $data['big_id'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '排序',
                                                        'inputName' => 'ProductCategory[rank]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['rank']) )? $data['rank'] : ''
                                                    ])}}
                                                  

                                                   

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '標題',
                                                        'inputName' => 'ProductCategory[title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['title']) )? $data['title'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '副標題',
                                                        'inputName' => 'ProductCategory[sub_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['sub_title']) )? $data['sub_title'] : ''
                                                    ])}}

                                                    {{ItemMaker::photo([
                                                        'labelText' => '圖片<br>(400 x 400以內)',
                                                        'inputName' => 'ProductCategory[out_image]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['out_image']) )? $data['out_image'] : ''
                                                    ])}}


                                                    {{ItemMaker::radio_btn([
                                                        'labelText' => '啟用狀態',
                                                        'inputName' => 'ProductCategory[is_visible]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
                                                    ])}}

                                                    {{ItemMaker::radio_btn([
                                                        'labelText' => 'New標籤',
                                                        'inputName' => 'ProductCategory[is_new]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_new']) )? $data['is_new'] : ''
                                                    ])}}


                                                <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>產品類別詳細頁設定</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                                    </div>
                                                    {{-- 反ㄌ  所以要改這樣 --}}
                                                    {{ItemMaker::textInput([
                                                        'labelText' => '標題',
                                                        'inputName' => 'ProductCategory[in_sub_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['in_sub_title']) )? $data['in_sub_title'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '標題旁邊敘述',
                                                        'inputName' => 'ProductCategory[in_sub_title1]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['in_sub_title1']) )? $data['in_sub_title1'] : ''
                                                    ])}}


                                                     {{ItemMaker::textInput([
                                                        'labelText' => '副標題',
                                                        'inputName' => 'ProductCategory[in_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['in_title']) )? $data['in_title'] : ''
                                                    ])}}

                                                    {{ItemMaker::textArea([
                                                        'labelText' => '產品特性<br>(換行以區分每筆資料<br>段落內插入green<br> 可以把字體變成綠色<br>
                                                            段落內插入nodash <br>可以移除前面的-)',
                                                        'inputName' => 'ProductCategory[features]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['features']) )? $data['features'] : ''
                                                    ])}}

                                                    {{ItemMaker::textArea([
                                                        'labelText' => '產品工藝<br>(換行以區分每筆資料<br>段落內插入green<br> 可以把字體變成綠色<br>
                                                            段落內插入nodash <br>可以移除前面的-)',
                                                        'inputName' => 'ProductCategory[crafts]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['crafts']) )? $data['crafts'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '適用場所',
                                                        'inputName' => 'ProductCategory[place]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['place']) )? $data['place'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '型錄下載標題',
                                                        'inputName' => 'ProductCategory[download_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['download_title']) )? $data['download_title'] : ''
                                                    ])}}

                                                    {{ItemMaker::FilePicker([
                                                        'labelText' => '型錄下載檔案',
                                                        'inputName' => 'ProductCategory[download]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['download']) )? $data['download'] : ''
                                                    ])}}

                                                <div class="form-group" style='background-color:lightblue; idth: 100%;  margin-left: 0;'>
                                                <div  ><h4><b>首頁資料設定<br><sub>該部分資料將顯示在首頁上的類別區塊<br>  若首頁沒有有選取這則資料則不用上資料</sub></b></h4></div>
                                                <div class="col-md-10"></div>
                                                </div>




                                                     {{ItemMaker::photo([
                                                        'labelText' => '分類區塊右方輪播<br>(1150 x 930)',
                                                        'inputName' => 'ProductCategory[home_image]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['home_image']) )? $data['home_image'] : ''
                                                    ])}}


                                                    {{ItemMaker::photo([
                                                        'labelText' => '分類區塊背景<br>(1150 x 930)',
                                                        'inputName' => 'ProductCategory[home_image_bg]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['home_image_bg']) )? $data['home_image_bg'] : ''
                                                    ])}}


                                                     {{ItemMaker::textInput([
                                                        'labelText' => '首頁副標題',
                                                        'inputName' => 'ProductCategory[home_sub_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['home_sub_title']) )? $data['home_sub_title'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '顯示字體顏色<br>(可搜尋色碼改變顏色<br>#000000->白色<br>#ffffff->黑色)',
                                                        'inputName' => 'ProductCategory[color]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['color']) )? $data['color'] : ''
                                                    ])}}

                                                </div>
                                            </div>



                                 




                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

		});

	</script>
@stop
