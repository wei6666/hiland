@extends('Fantasy.template')

@section('title',"類別項目修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			類別項目 <small>Newscatagories Manager</small> </h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">類別項目</a> <i class="fa fa-angle-right"></i>
				</li>
				<li>
					類別項目修改 </li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

					<!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 類別項目 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> 關於我們列表頁 </a>
                                            </li>
                                             <li >
                                                <a href="#tab_general1" data-toggle="tab"> 關於我們區塊1詳細頁設定 </a>
                                            </li>
                                            <li >
                                                <a href="#tab_general2" data-toggle="tab"> 關於我們區塊2詳細頁設定 </a>
                                            </li>
                                            <li >
                                                <a href="#tab_general3" data-toggle="tab"> 關於我們區塊3詳細頁設定 </a>
                                            </li>

                                            <li >
                                                <a href="#tab_general4" data-toggle="tab"> 品質認證設定 </a>
                                            </li>

                                            <li >
                                                <a href="#tab_general5" data-toggle="tab"> 歷史事蹟設定 </a>
                                            </li>

                                            <li >
                                                <a href="#tab_general6" data-toggle="tab"> 先進設備設定 </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'AboutSet[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}


                                           

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '標題',
                                                        'inputName' => 'AboutSet[title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['title']) )? $data['title'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '選單標籤',
                                                        'inputName' => 'AboutSet[select_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['select_title']) )? $data['select_title'] : ''
                                                    ])}}

                                                    

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '選單1內容',
                                                        'inputName' => 'AboutSet[s1]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1']) )? $data['s1'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '選單2內容',
                                                        'inputName' => 'AboutSet[s2]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s2']) )? $data['s2'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '選單3內容',
                                                        'inputName' => 'AboutSet[s3]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s3']) )? $data['s3'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '選單4內容',
                                                        'inputName' => 'AboutSet[s4]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s4']) )? $data['s4'] : ''
                                                    ])}}



												 <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>區塊1設定</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                              </div>
                                                     {{ItemMaker::textArea([
                                                        'labelText' => '區塊1標題',
                                                        'inputName' => 'AboutSet[s1_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_title']) )? $data['s1_title'] : ''
                                                    ])}}

                                                    {{ItemMaker::photo([
                                                        'labelText' => '區塊1圖片1<br>( 600 x 380)',
                                                        'inputName' => 'AboutSet[s1_photo1]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_photo1']) )? $data['s1_photo1'] : ''
                                                    ])}}

                                                     {{ItemMaker::photo([
                                                        'labelText' => '區塊1圖片2<br>( 600 x 380)',
                                                        'inputName' => 'AboutSet[s1_photo2]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_photo2']) )? $data['s1_photo2'] : ''
                                                    ])}}


                                                    {{ItemMaker::textArea([
                                                        'labelText' => '區塊1圖片內容',
                                                        'inputName' => 'AboutSet[s1_title_area1]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_title_area1']) )? $data['s1_title_area1'] : ''
                                                    ])}}


                                                    {{ItemMaker::textArea([
                                                        'labelText' => '區塊1圖片2內容',
                                                        'inputName' => 'AboutSet[s1_title_area2]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_title_area2']) )? $data['s1_title_area2'] : ''
                                                    ])}}



                                            <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>區塊2設定</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                              </div>



                                                    {{ItemMaker::textArea([
                                                        'labelText' => '區塊2標題',
                                                        'inputName' => 'AboutSet[s2_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s2_title']) )? $data['s2_title'] : ''
                                                    ])}}


                                                    {{ItemMaker::photo([
                                                        'labelText' => '區塊2圖片<br>( 780 x 450)',
                                                        'inputName' => 'AboutSet[s2_photo]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s2_photo']) )? $data['s2_photo'] : ''
                                                    ])}}

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '區塊2圖片區塊內容',
                                                        'inputName' => 'AboutSet[s2_area_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s2_area_title']) )? $data['s2_area_title'] : ''
                                                    ])}}


                                                     <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>區塊3設定</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                              </div>




                                                    {{--   {{ItemMaker::textArea([
                                                        'labelText' => '區塊3標題',
                                                        'inputName' => 'AboutSet[s3_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s3_title']) )? $data['s3_title'] : ''
                                                    ])}} --}}


                                                    {{ItemMaker::photo([
                                                        'labelText' => '區塊3圖片<br>( 780 x 450)',
                                                        'inputName' => 'AboutSet[s3_photo]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s3_photo']) )? $data['s3_photo'] : ''
                                                    ])}}

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '區塊3圖片區塊內容',
                                                        'inputName' => 'AboutSet[s3_area_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s3_area_title']) )? $data['s3_area_title'] : ''
                                                    ])}}


                                                </div>
                                            </div>

                                             <div class="tab-pane  form-row-seperated" id="tab_general1">
                                                <div class="form-body form_news_pad">


                                            <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>詳細頁設定</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                              </div>

                                              <h1 style="text-align: center;">區塊1內頁設定</h1>
                                                <hr style="background-color: red; height:10px; width:100%;">

                                                {{ItemMaker::textArea([
                                                        'labelText' => '內頁標題',
                                                        'inputName' => 'AboutSet[s1_in_title1]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_in_title1']) )? $data['s1_in_title1'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '內頁錨點選單標題',
                                                        'inputName' => 'AboutSet[s1_in_select_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_in_select_title']) )? $data['s1_in_select_title'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '錨點標題1',
                                                        'inputName' => 'AboutSet[s1_in_tag1]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_in_tag1']) )? $data['s1_in_tag1'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '錨點標題2',
                                                        'inputName' => 'AboutSet[s1_in_tag2]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_in_tag2']) )? $data['s1_in_tag2'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '錨點標題3',
                                                        'inputName' => 'AboutSet[s1_in_tag3]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_in_tag3']) )? $data['s1_in_tag3'] : ''
                                                    ])}}

                                                     {{ItemMaker::photo([
                                                        'labelText' => '區塊1圖片<br>( 900 x 570)',
                                                        'inputName' => 'AboutSet[s1_in_photo1]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_in_photo1']) )? $data['s1_in_photo1'] : ''
                                                    ])}}

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '區塊1圖片內容',
                                                        'inputName' => 'AboutSet[s1_in_photo_content1]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_in_photo_content1']) )? $data['s1_in_photo_content1'] : ''
                                                    ])}}

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '區塊2標題',
                                                        'inputName' => 'AboutSet[s1_in_photo_titl2]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_in_photo_titl2']) )? $data['s1_in_photo_titl2'] : ''
                                                    ])}}


                                                     {{ItemMaker::photo([
                                                        'labelText' => '區塊2圖片1<br>( 800 x 500)',
                                                        'inputName' => 'AboutSet[s1_in_photo2_1]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_in_photo2_1']) )? $data['s1_in_photo2_1'] : ''
                                                    ])}}

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '區塊2圖片內容1',
                                                        'inputName' => 'AboutSet[s1_in_photo_content2_1]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_in_photo_content2_1']) )? $data['s1_in_photo_content2_1'] : ''
                                                    ])}}


                                                     {{ItemMaker::photo([
                                                        'labelText' => '區塊2圖片2<br>( 500 x 320)',
                                                        'inputName' => 'AboutSet[s1_in_photo2_2]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_in_photo2_2']) )? $data['s1_in_photo2_2'] : ''
                                                    ])}}

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '區塊2圖片內容2',
                                                        'inputName' => 'AboutSet[s1_in_photo_content2_2]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_in_photo_content2_2']) )? $data['s1_in_photo_content2_2'] : ''
                                                    ])}}

                                                    {{ItemMaker::textArea([
                                                        'labelText' => '區塊3標題',
                                                        'inputName' => 'AboutSet[s1_in_photo_content3]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_in_photo_content3']) )? $data['s1_in_photo_content3'] : ''
                                                    ])}}

                                                    {{ItemMaker::photo([
                                                        'labelText' => '區塊3圖片<br>( 600 x 350)',
                                                        'inputName' => 'AboutSet[s1_in_photo3]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_in_photo3']) )? $data['s1_in_photo3'] : ''
                                                    ])}}

                                                     {{ItemMaker::photo([
                                                        'labelText' => '公司mark(186 x 195)',
                                                        'inputName' => 'AboutSet[s1_in_company_tag]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_in_company_tag']) )? $data['s1_in_company_tag'] : ''
                                                    ])}}

                                                    {{ItemMaker::textArea([
                                                        'labelText' => '歷史區塊標題',
                                                        'inputName' => 'AboutSet[s1_in_history_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_in_history_title']) )? $data['s1_in_history_title'] : ''
                                                    ])}}

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '品質認證區塊標題',
                                                        'inputName' => 'AboutSet[s1_quality]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_quality']) )? $data['s1_quality'] : ''
                                                    ])}}




                                           
                                                </div>
                                            </div>


                                            <div class="tab-pane  form-row-seperated" id="tab_general2">
                                                <div class="form-body form_news_pad">


                                                <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>詳細頁設定</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                              </div>

                                              <h1 style="text-align: center;">區塊2內頁設定</h1>
                                                <hr style="background-color: red; height:10px; width:100%;">



                                                      {{ItemMaker::textArea([
                                                        'labelText' => '標題',
                                                        'inputName' => 'AboutSet[s2_in_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s2_in_title']) )? $data['s2_in_title'] : ''
                                                    ])}}

                                                    {{ItemMaker::photo([
                                                        'labelText' => '區塊1圖片<br>( 1000 x 500)',
                                                        'inputName' => 'AboutSet[s2_in_photo1]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s2_in_photo1']) )? $data['s2_in_photo1'] : ''
                                                    ])}}

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '區塊1內容',
                                                        'inputName' => 'AboutSet[s2_in_content1]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s2_in_content1']) )? $data['s2_in_content1'] : ''
                                                    ])}}


                                                     {{ItemMaker::photo([
                                                        'labelText' => '區塊2圖片<br>( 650 x 500)',
                                                        'inputName' => 'AboutSet[s2_in_photo2]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s2_in_photo2']) )? $data['s2_in_photo2'] : ''
                                                    ])}}

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '區塊2內容',
                                                        'inputName' => 'AboutSet[s2_in_content2]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s2_in_content2']) )? $data['s2_in_content2'] : ''
                                                    ])}}


                                                    {{ItemMaker::photo([
                                                        'labelText' => '區塊3圖片<br>( 1440 x 500)',
                                                        'inputName' => 'AboutSet[s2_in_photo3]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s2_in_photo3']) )? $data['s2_in_photo3'] : ''
                                                    ])}}

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '區塊3左邊內容',
                                                        'inputName' => 'AboutSet[s2_in_content3left]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s2_in_content3left']) )? $data['s2_in_content3left'] : ''
                                                    ])}}

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '區塊3右邊內容',
                                                        'inputName' => 'AboutSet[s2_in_content3right]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s2_in_content3right']) )? $data['s2_in_content3right'] : ''
                                                    ])}}



                                           
                                                </div>
                                            </div>


                                             <div class="tab-pane  form-row-seperated" id="tab_general3">
                                                <div class="form-body form_news_pad">


                                                <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>詳細頁設定</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                              </div>

                                              <h1 style="text-align: center;">區塊3內頁設定</h1>
                                                <hr style="background-color: red; height:10px; width:100%;">

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '標題',
                                                        'inputName' => 'AboutSet[s3_in_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s3_in_title']) )? $data['s3_in_title'] : ''
                                                    ])}}


                                                     {{ItemMaker::textArea([
                                                        'labelText' => '區塊1標題',
                                                        'inputName' => 'AboutSet[s3_in_s1title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s3_in_s1title']) )? $data['s3_in_s1title'] : ''
                                                    ])}}

                                                    {{ItemMaker::photo([
                                                        'labelText' => '區塊1圖片1<br>( 300 x 200)',
                                                        'inputName' => 'AboutSet[s3_in_img1]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s3_in_img1']) )? $data['s3_in_img1'] : ''
                                                    ])}}

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '區塊1圖片內容',
                                                        'inputName' => 'AboutSet[s3_in_img1_content]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s3_in_img1_content']) )? $data['s3_in_img1_content'] : ''
                                                    ])}}


                                                     {{ItemMaker::photo([
                                                        'labelText' => '區塊1圖片2<br>( 300 x 200)',
                                                        'inputName' => 'AboutSet[s3_in_img2]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s3_in_img2']) )? $data['s3_in_img2'] : ''
                                                    ])}}

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '區塊1圖片內容2',
                                                        'inputName' => 'AboutSet[s3_in_img2_content]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s3_in_img2_content']) )? $data['s3_in_img2_content'] : ''
                                                    ])}}



                                                     {{ItemMaker::photo([
                                                        'labelText' => '區塊1圖片3<br>( 300 x 200)',
                                                        'inputName' => 'AboutSet[s3_in_img3]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s3_in_img3']) )? $data['s3_in_img3'] : ''
                                                    ])}}

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '區塊1圖片內容3',
                                                        'inputName' => 'AboutSet[s3_in_img3_content]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s3_in_img3_content']) )? $data['s3_in_img3_content'] : ''
                                                    ])}}



                                                       {{ItemMaker::PHOTO([
                                                        'labelText' => '區塊2圖片<br>( 660 x 480)',
                                                        'inputName' => 'AboutSet[s3_in_photo1]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s3_in_photo1']) )? $data['s3_in_photo1'] : ''
                                                    ])}}


                                                        {{ItemMaker::textArea([
                                                        'labelText' => '區塊2內容',
                                                        'inputName' => 'AboutSet[s3_in_contnet1]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s3_in_contnet1']) )? $data['s3_in_contnet1'] : ''
                                                    ])}}

                                                      {{ItemMaker::textArea([
                                                        'labelText' => '設備標題',
                                                        'inputName' => 'AboutSet[s3_photo_slider_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s3_photo_slider_title']) )? $data['s3_photo_slider_title'] : ''
                                                    ])}}


                                                </div>
                                            </div>

                                              <div class="tab-pane  form-row-seperated" id="tab_general4">
                                                <div class="form-body form_news_pad">


                                              {{ FormMaker::photosTable(
                                                    [
                                                        'nameGroup' => $routePreFix.'-AboutCertificate',
                                                        'datas' => ( !empty($parent['has']['AboutCertificate']) )? $parent['has']['AboutCertificate'] : [],
                                                        'table_set' =>  $bulidTable['AboutCertificate'],
                                                        'pic' => false
                                                    ]
                                                 ) }}

                                                     </div>
                                            </div>


                                              <div class="tab-pane  form-row-seperated" id="tab_general5">
                                                <div class="form-body form_news_pad">



                                              {{ FormMaker::photosTable(
                                                    [
                                                        'nameGroup' => $routePreFix.'-AboutYearData',
                                                        'datas' => ( !empty($parent['has']['AboutYearData']) )? $parent['has']['AboutYearData'] : [],
                                                        'table_set' =>  $bulidTable['AboutYearData'],
                                                        'pic' => false
                                                    ]
                                                 ) }}

                                                     </div>
                                            </div>


                                              <div class="tab-pane  form-row-seperated" id="tab_general6">
                                                <div class="form-body form_news_pad">



                                              {{ FormMaker::photosTable(
                                                    [
                                                        'nameGroup' => $routePreFix.'-AboutEquitment',
                                                        'datas' => ( !empty($parent['has']['AboutEquitment']) )? $parent['has']['AboutEquitment'] : [],
                                                        'table_set' =>  $bulidTable['AboutEquitment'],
                                                        'pic' => false
                                                    ]
                                                 ) }}

                                                     </div>
                                            </div>




                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

		});

	</script>
@stop
