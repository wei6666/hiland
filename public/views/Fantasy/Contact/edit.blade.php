@extends('Fantasy.template')

@section('title',"類別項目修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    </style>

@stop

@section('content')


<a href="" id="toFileManager"></a>

		
		<h3 class="page-title">
			類別項目 <small>Newscatagories Manager</small> </h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">類別項目</a> <i class="fa fa-angle-right"></i>
				</li>
				<li>
					類別項目修改 </li>
			</ul>
		</div>
	

			<div class="row">
				<div class="col-md-12">

               
					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 類別項目 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> Information </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'Contact[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

													{{ItemMaker::radio_btn([
														'labelText' => '回復狀態',
														'inputName' => 'Contact[is_visible]',
														'options' => $StatusOption,
														'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
													])}}

                                                  

                                                    {{ItemMaker::textInput([
  														'labelText' => '姓名',
  														'inputName' => 'Contact[name]',
                                                        "disabled" => "disabled",
  														//'helpText' =>'前台列表顯示之排序',
  														'value' => ( !empty($data['name']) )? $data['name'] : ''
  													])}}

                                                    

                                                     {{ItemMaker::textInput([
                                                        'labelText' => '性別',
                                                        'inputName' => 'Contact[gender]',
                                                        "disabled" => "disabled",
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['gender']) )? $data['gender'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
  														'labelText' => '電話',
  														'inputName' => 'Contact[phone]',
                                                        "disabled" => "disabled",
  														'value' => ( !empty($data['phone']) )? $data['phone'] : ''
  													])}}

                                                  


                                                    {{ItemMaker::textInput([
                                                        'labelText' => 'email',
                                                        'inputName' => 'Contact[email]',
                                                        "disabled" => "disabled",
                                                        'value' => ( !empty($data['email']) )? $data['email'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '地址',
                                                        'inputName' => 'Contact[link]',
                                                        "disabled" => "disabled",
                                                        'value' => ( !empty($data['address']) )? $data['address'] : ''
                                                    ])}}


                                                      {{ItemMaker::textArea([
                                                        'labelText' => '意見內容',
                                                        'inputName' => 'Contact[message]',
                                                        "disabled" => "disabled",
                                                        'value' => ( !empty($data['message']) )? $data['message'] : ''
                                                    ])}}



                                                 {{--    {{ItemMaker::textInput([
                                                        'labelText' => '產品連結',
                                                        'inputName' => 'Contact[link]',
                                                        "disabled" => "disabled",
                                                        'value' => ( !empty($data['link']) )? $data['link'] : ''
                                                    ])}} --}}


                                                    <div class="form-group">
                                                    <label class="col-md-2 control-label">產品連結
                                                        
                                                    </label>
                                                    <div class="col-md-10">
                                                       <br>
                                                       @if(!empty($data['link']))
                                                       <a href="{{ $data['link'] }}" target="_blank">{{ $data['link'] }}</a>
                                                       @else
                                                       -
                                                       @endif
                                                    </div>
                                                </div>

                                                    



                                                  

                                                



                                                 

                                                   
                                                   {{-- {{ItemMaker::textInput([
                                                        'labelText' => '連結',
                                                        'inputName' => 'Newscatagorys[link]',
                                                        //'helpText' => '針對首頁使用',
                                                        'value' => ( !empty($data['link']) )? $data['link'] : ''
                                                    ])}}--}}


                                                   

                                                </div>
                                            </div>


                                 




                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

		});

	</script>
@stop
