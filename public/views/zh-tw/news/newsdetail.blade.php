<div class="lbox_inner ">
    <section class="new_detail_bigbox">
        <div class="news_detail_header">
            <div class="close">
                <span class="x_close"></span>
                <span class="green"></span>
                <span class="yellow"></span>
                <img src="../../assets/img/logo.svg" alt="">
            </div>
            <h5>{{ $News->in_title }}</h5>
            <?php $thispageurl="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>
            <div class="news_detail_icon">
                <a href="http://twitter.com/home/?status={{ $thispageurl }}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <a href="http://www.facebook.com/share.php?u={{ $thispageurl }}" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            </div>
            <h6>{{ $News->st_date }} -  {{ $News->ed_date }}</h6>
        </div>
        <div class="news_detail_content">
            <div class="detail_box_style01">
                <!-- 圖寬1000*500 -->
                <img src="{{ $News->in_big_photo }}" alt="">
                <div class="news_detail_note">
                    {!! $News->in_content1 !!}
                </div>
            </div>
            <div class="detail_box_style02">
                <div class="detail_box_img">
                    <!-- 圖寬450*500 -->
                    <img src="{{ $News->in_small_photo }}" alt="">
                    <p class="img_p">{!! $News->img_small_tag !!}</p>
                </div>
                <div class="news_detail_note">
                    {!! $News->in_content2 !!}
                    
                </div>
            </div>
            <div class="detail_box_style03 news_detail_note">
                {!! $News->in_content3 !!}
                
            </div>
        </div>
        <div class="half_color_box">
            <a href="javascript:;" onclick="$('.news_detail_header .x_close').click()">
                <div class="half_color ">
                    <h5>back</h5>
                </div>
            </a>
        </div>
        <div class="more_news">
            <a href="javascript:;"><h5>{{ $News->more_news }}</h5></a>
        </div>
    </section>
    <section class="new_detail_bottombox">
        <div class="news_deatil_bottom">
            <div class="news_pre">
                <a class='opendetailNews'  href="{{ ItemMaker::url('newsdetail/'.$last->id.'?Open='.$last->id) }}" last="{{ ItemMaker::url('news') }}">
                    <!-- 圖寬1000*500 -->
                    <div class="button_img_box">
                        <div class="button_img" style="background: url({{ $last->out_img }}) center center /cover no-repeat"></div>
                    </div>
                    <?php
                        $next_this_time=new DateTime($next->date);
                        $last_this_time=new DateTime($last->date);

                    ?>
                    <div class="button_note_right">
                        <div class="border_box01">
                            <span class="b_20_b">{{ $last->out_title }}</span>
                        </div>
                        <div class="border_box">
                            <span class="op1_16">{{ $last_this_time->format('F') }} - {{ $last_this_time->format('d') }}</span>
                            <span class="dg_b_40">{{ $last_this_time->format('Y') }}</span>
                        </div>
                        <h5 class="next_word">
			 		prev news
			 	    </h5>
                    </div>
                </a>
            </div>
            <div class="news_next">
                <a class='opendetailNews'  href="{{ ItemMaker::url('newsdetail/'.$next->id.'?Open='.$next->id) }}" last="{{ ItemMaker::url('news') }}">
                    <div class="button_img_box">
                        <div class="button_img" style="background: url({{ $next->out_img }}) center center /cover no-repeat"></div>
                    </div>
                    <div class="button_note_left">
                        <div class="border_box01"><span class="b_20_b">{{ $next->out_title }}</span></div>
                        <div class="border_box">
                            <span class="op1_16">{{ $next_this_time->format('F') }} - {{ $next_this_time->format('d') }}</span>
                            <span class="dg_b_40">{{ $next_this_time->format('Y') }}</span>
                        </div>
                        <h5 class="next_word">
			 		next news
			 	</h5>
                    </div>
                </a>
            </div>
        </div>
    </section>
</div>