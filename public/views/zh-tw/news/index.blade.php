
@extends('template')

    <!--RWD Setting-->
@section('css')

@stop
@section('content')

@include($locale.'.header') 
	<main>
	   <!-- 大標題 -->
		<section class="big_title" id="top">
			<div class="banner_inner">
				<span>{!! $NewsSet->title !!}</span>
				<div class="banner_note">
					{!! $NewsSet->sub_title !!}
				</div>
			</div>
			<a href="#two" class="scroll_line_box">
				<span>s</span>
				<span>c</span>
				<span>r</span>
				<span>o</span>
				<span>l</span>
				<span>l</span>
				<div class="down_page">
					<span></span>
					<div class="angle"></div>
				</div>
			</a>
		</section>
		<!-- 內容 -->
		<section class="content_box new01" id="two">
			<div class="select_box">
			    <!-- 電腦menu -->
			   <div class="select_inner">
			   	<? 
		   	    	$a=Date("Y");
		   	    	$earlyestnewyear=new DateTime($earlyestnew->date);
		   	    	$earlyestnewyear=$earlyestnewyear->format('Y');
		   	    	
	   	    	?>
					<span class="select_style">{{ $NewsSet->cate_title }}</span>
					<ul class="select_ul">
						<li class="on"><a href="javascript:;">ALL</a></li>
						@while($a >= $earlyestnewyear)
						<li ><a href="javascript:;">{{ $a }}</a></li>
						<?php $a --;?>
			   	    	@endwhile
						
					</ul>
			  </div>
			     <!-- rwd menu -->
			     <? 
		   	    	$a=Date("Y");
		   	    	$earlyestnewyear=new DateTime($earlyestnew->date);
		   	    	$earlyestnewyear=$earlyestnewyear->format('Y');
		   	    	
	   	    	?>
			    <div class="a_id_box">
			        <span class="span_space">{{ $a }} </span><i class="fa fa-caret-down" aria-hidden="true"></i>
			   	    <ul>
			   	    	
			   	    	@while($a >= $earlyestnewyear)
			   	    	<li><a href="javascript:;">{{ $a }}</a></li>
			   	    	<?php $a --;?>
			   	    	@endwhile
			   	    	
			   	    </ul>
			    </div>
			  
			</div>
			<div class="content_inner">

				<ul class="news_items">
					@foreach($News as $value)
					<li> <!-- 圖寬470 高320 -->
						<a class='openNews'  href="{{ ItemMaker::url('newsdetail/'.$value->id.'?Open='.$value->id) }}" last="{{ ItemMaker::url('news') }}">
							<div class="new_inner">
								<div class="new_img">
								    <div style="background: url({{ $value->out_img }})" class="w47_32">
				                         <div class="w47_cover">
				                          	 <span></span>
				                          	 <h5 class="y_b_14">{{ $value->out_readmoretag }}</h5>
				                          </div>
									</div>
								</div>
								<div class="news_note">
								  <div class=" title01">
								   			<span class="b_20_b">{!! $value->out_title !!}</span>
								   </div>
								   <div class="title02">
									   <span class="op8_16 ">{!! $value->out_content !!}</span>
		                           </div>
								   <div class="title03">
								   	<?php
										$this_time=new DateTime($value['date']);

									?>
								   	  <span class="op1_16">{{ $this_time->format("F") }} - {{ $this_time->format("d") }}</span>
								   	  <span class="dg_b_40">{{ $this_time->format("Y") }}</span>
								   </div>
								</div>
							</div>
						</a>
					</li>
					@endforeach					
				</ul>
			</div>
			<div class="square_more newssquare_more">
			   <a href="javascript:;">
				<span></span>
				</a>
			</div>
		</section>
	</main>
	@include($locale.'.footer') 
	 @if(!empty($_GET['Open']))

	 <a  id='OpenTheBox' class='openNews' href="{{ ItemMaker::url('newsdetail/'.$Opendata->id.'?Open='.$Opendata->id) }}" last="{{ ItemMaker::url('news') }}">

      </a>
    @endif
@section('script')  
    <!--主要jQuery-->
    
    <script >
     // 	$('.select_box li').on('click',function(){
    	// 	var cate=$(this).attr('cate');
    	// 	$('.allques').hide();
    	// 	var co=1;
    	// 	$('.allques').each(function(){
    			
    	// 		if($(this).attr('cate')==cate || cate=='ALL' && $(this).attr('cate')!="nodata")
    	// 		{
    	// 			$(this).show();
    	// 			var num='';
    	// 			if(num<10)
    	// 			num="O ";
	    // 			num+=co;
    	// 			num+=' -';
    	// 			$(this).find('span').eq(0).text(num);
    	// 			co++;
    	// 		}
    	// 	});
    	// 	if(co ==1)
    	// 		$('.nodata').show();
    	// });

   

    // $('.news_items li a').click(function() {
    // 	var url = "/newsdetail";
    //      lbox1(url);
    //    /* if (lbox1_t === false) {
    //         lbox1_t = true;
    //         //
    //     }*/
    // });

    $(document).on('ready',function(){

        if($('#OpenTheBox').length >0)
        {
        	
            $('#OpenTheBox').click();
            
        }

        $('body').attr('id','news');

     });

    </script>
@stop


@stop