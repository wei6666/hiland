@if(count($News)>0)
@foreach($News as $value)
					<li> <!-- 圖寬470 高320 -->
						<a class='openNews' href="{{ ItemMaker::url('newsdetail/'.$value->id.'?Open='.$value->id) }}" last="{{ ItemMaker::url('news') }}">
							<div class="new_inner">
								<div class="new_img">
								    <div style="background: url({{ $value->out_img }})" class="w47_32">
				                         <div class="w47_cover">
				                          	 <span></span>
				                          	 <h5 class="y_b_14">{{ $value->out_readmoretag }}</h5>
				                          </div>
									</div>
								</div>
								<div class="news_note">
								  <div class=" title01">
								   			<span class="b_20_b">{!! $value->out_title !!}</span>
								   </div>
								   <div class="title02">
									   <span class="op8_16 ">{!! $value->out_content !!}</span>
		                           </div>
								   <div class="title03">
								   	<?php
										$this_time=new DateTime($value['date']);

									?>
								   	  <span class="op1_16">{{ $this_time->format("F") }} - {{ $this_time->format("d") }}</span>
								   	  <span class="dg_b_40">{{ $this_time->format("Y") }}</span>
								   </div>
								</div>
							</div>
						</a>
					</li>
					@endforeach	
				@else
				<li>
					<div class="news_note">
								  <div class=" title01">
								   			<span class="b_20_b" style="width: 100%;
    text-align: right;">nodata </span></div></div>

				 

				</li>
				@endif