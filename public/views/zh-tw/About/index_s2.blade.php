<div class="lbox_inner ">
    <div class="about_lightbox02">
        <div class="light_box_header">
            <div class="close">
                <span class="x_close"></span>
                <span class="green"></span>
                <span class="yellow"></span>
                <img src="../../assets/img/logo.svg" alt="">
            </div>
        </div>
        <section  class="about_light_titile">
        	{!! $AboutSet->s2_in_title !!}
	    	
	    </section>
	    <section class="innovation01">
	     	<img src="{!! $AboutSet->s2_in_photo1 !!}" alt="">
	     	<div class="about_note02">
				{!! $AboutSet->s2_in_content1 !!}
			</div>
	    </section>
	    <section class="production02">
	        <div class="innovation02_img"> 
	           <!--  圖寬650xp * 500 -->
	           <div class="displaynone_img"><img src="{!! $AboutSet->s2_in_photo2 !!}" alt=""></div>
	           
	        </div>
	        <div class="innovation02_note">
	        	<div class="about_note02">
					{!! $AboutSet->s2_in_content2 !!}
			    </div>
	        </div>	    
	    </section>
	    <section class="innovation03">
	        <div class="innovation03_img">
	            <img src="{!! $AboutSet->s2_in_photo3 !!}" alt="">
	        </div>
	        <div class="innovation03_note">
	        	<div class="about_note02">
	        	    <div class="innovation03_right">
					   {!! $AboutSet->s2_in_content3left !!}
	        	    </div>
	        	    <div class="innovation03_left">
	        	    	{!! $AboutSet->s2_in_content3right !!}
	        	    </div>
			    </div>
	        </div>
	    </section>
	     <div class="lightbox_aa">
            <div class="half_color_box ">
                <a href="javascript:;" class="out">
                    <div class="half_color ">
                        <span>back</span>
                    </div>
                </a>
            </div>
        </div>
	 </div>   
</div>