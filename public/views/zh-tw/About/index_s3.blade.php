<div class="lbox_inner ">
    <div class="about_lightbox03">
        <div class="about_lightbox03_inner">
	        <div class="light_box_header">
	            <div class="close">
	                <span class="x_close"></span>
	                <span class="green"></span>
	                <span class="yellow"></span>
	                <img src="../../assets/img/logo.svg" alt="">
	            </div>
	        </div>
	        <section  class="about_light_titile">
	        	{!! $AboutSet->s3_in_title !!}
		    	
		    </section>
		    <section class="production01">
		    	<div class="about_note02">
		    		{!! $AboutSet->s3_in_s1title !!}
					
				</div>
				<ul>
					<li>
					    <div class="production01_img">
					    	<img src="{!! $AboutSet->s3_in_img1 !!}" alt="">
					    	<div class="production_note">
					    		{!! $AboutSet->s3_in_img1_content !!}
							   
						    </div>
					    	<div class="next_icon">
			                   <i class="fa fa-chevron-right  slick-arrow"></i>
			                </div> 
					    </div>
						
					</li>
					<li>
						 <div class="production01_img">
					    	<img src="{!! $AboutSet->s3_in_img2 !!}" alt="">
					    	<div class="production_note">
					    		{!! $AboutSet->s3_in_img2_content !!}
							   
						    </div>
					    	<div class="next_icon">
			                   <i class="fa fa-chevron-right  slick-arrow"></i>
			                </div> 
					    </div>
						
					</li>
					<li>
					    <div class="production01_img">
					    	<img src="{!! $AboutSet->s3_in_img3 !!}" alt="">
					    	<div class="production_note">
					    		{!! $AboutSet->s3_in_img3_content !!}
							   
						    </div>
					    	<div class="next_icon">
			                   <i class="fa fa-chevron-right  slick-arrow"></i>
			                </div> 
					    </div>
						
						
	                   
					</li>
				</ul>
		    </section>
		    <section class="production02">
		        <div class="innovation02_img"> 
		           <!--  圖寬650xp * 500 -->
		            <img src="{!! $AboutSet->s3_in_photo1 !!}" alt="">
		        </div>
		        <div class="innovation02_note">
		        	<div class="about_note02">
		        		{!! $AboutSet->s3_in_contnet1 !!}
						
				    </div>
		        </div>	    
		    </section>
		</div>
		    <section class="production03">
		    	<div class="about_note02">
		    		{!! $AboutSet->s3_photo_slider_title !!}
					
				</div>
				<div class="production03_slick">
					<ul>
						@foreach($AboutEquitment as $key => $value)
						<li>
						    <div class="production03_slick_inner">
						    	<img src="{{ $value->image }}" alt="">
							    <div class="production03_note">
									<h5>o{{  $key+1 }}</h5>
									<span>- {{ $value->title }}  </span>
							    </div>
						    </div>
						</li>

						@endforeach
						<!-- <li>
						    <div class="production03_slick_inner">
						    	<img src="../../upload/about/product05.jpg" alt="">
							    <div class="production03_note">
									<h5>o4</h5>
									<span>- 成型生坏進入乾燥製程  </span>
							    </div>
						    </div>
						</li>
						<li>
							<div class="production03_slick_inner">
						    	<img src="../../upload/about/product05.jpg" alt="">
							    <div class="production03_note">
									<h5>o4</h5>
									<span>- 成型生坏進入乾燥製程</span>
							    </div>
						    </div>
						</li>
						<li>
							<div class="production03_slick_inner">
						    	<img src="../../upload/about/product05.jpg" alt="">
							    <div class="production03_note">
									<h5>o4</h5>
									<span>- 成型生坏進入 乾燥製程成型 生坏進入乾 燥製程成  </span>
							    </div>
						    </div>
						</li> -->
					</ul>
				</div>
		    </section>
		     <div class="lightbox_aa">
	            <div class="half_color_box ">
	                <a href="ajvascript:;" class="out">
	                    <div class="half_color ">
	                        <span>back</span>
	                    </div>
	                </a>
	            </div>
	        </div>

	</div>
</div>