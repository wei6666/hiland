<div class="lbox_inner ">
    <div class="about_lightbox01">
        <div class="light_box_header">
            <div class="close">
                <span class="x_close"></span>
                <span class="green"></span>
                <span class="yellow"></span>
                <img src="../../assets/img/logo.svg" alt="">
            </div>
        </div>
        <section>

            <div class="about_light_titile">
                {!! $AboutSet->s1_in_title1 !!}
            </div>
        </section>
        <section class="content_box">
            <div class="select_box light_select">
                <!-- 電腦menu -->
                <div class="select_inner">
                    <span class="select_style">{!! $AboutSet->s1_in_select_title !!}</span>
                    <ul class="select_ul">
                        <li class="on"><a href="#a01"   data="a01">{!! $AboutSet->s1_in_tag1 !!}</a></li>
                        <li><a href="#a02"   data="a02">{!! $AboutSet->s1_in_tag2 !!}</a></li>
                        <li><a href="#a03"   data="a03">{!! $AboutSet->s1_in_tag3 !!}</a></li>
                    </ul>
                </div>
                <!-- rwd menu -->
                <div class="a_id_box">
                    <span class="span_space">2016 </span><i class="fa fa-caret-down" aria-hidden="true"></i>
                    <ul>
                        <li><a href="#a01"   data="a01">{!! $AboutSet->s1_in_tag1 !!}</a></li>
                        <li><a href="#a02"   data="a02">{!! $AboutSet->s1_in_tag2 !!}</a></li>
                        <li><a href="#a03"   data="a03">{!! $AboutSet->s1_in_tag3 !!}</a></li>
                    </ul>
                </div>
            </div>
            
        </section>
        <section class="about_light01">
            <div class="about_light01_img">
                <img src="{!! $AboutSet->s1_in_photo1 !!}" alt="">
            </div>
            <div class="about_light01_note">
               {!! $AboutSet->s1_in_photo_content1 !!}
            </div>
        </section>

        <section class="about_light02" id="a01">
            <div class="about_light02_header">
                <div class="light02_img">
                    <img src="../../assets/img/about_logo.svg" alt="">
                </div>
                <div class="light02_note">
                   {!! $AboutSet->s1_in_photo_titl2 !!}
                </div>
            </div>
            <div class="about_light02_content">
                <img src="{!! $AboutSet->s1_in_photo2_1 !!}" alt="">
                <div class="about_light02_note">
                   {!! $AboutSet->s1_in_photo_content2_1 !!}
                </div>
            </div>
            <div class="about_light02_content02">
                <div class="about_light02_note">
                    {!! $AboutSet->s1_in_photo_content2_2 !!}
                </div>
                <div class="about_content02_img">
                    <img src="{!! $AboutSet->s1_in_photo2_2 !!}" alt="">
                </div>
            </div>
        </section>

        <section class="about_light03" id="a02">
	        <div class="about_light03_inner">
	            <div class="about_light03_header">
	                <div class="about_note">
	                    {!! $AboutSet->s1_in_photo_content3 !!}
	                </div>
	                <div class="about_light03_img" style="background: url({!! $AboutSet->s1_in_photo3 !!}) center center /cover no-repeat;">
	                    <div class="about_light03_logo">
	                        <img src="../../assets/img/hiland_logo_gray.jpg" alt="">
	                    </div>
	                </div>
	            </div>
	            <div class="about_light03_conheaer">
	                <div class="about_note">
	                     {!! $AboutSet->s1_in_history_title !!}
	                </div>
	                <div class="number_bouttn  histiry_btn">
	                    <i class="fa fa-chevron-left histiry_btn_left slick-arrow"></i>
	                    <i class="fa fa-chevron-right histiry_btn_right slick-arrow"></i>
	                </div>
	            </div>
                 <div class="half_color_box">
            
            </div>
	            <div class="about_light03_content">
	                <ul class="hostory_slick">
                        @foreach($AboutYearData as $value)
	                    <li>
                            
                            <span>{{ $value->year }}</span>
                            <i class="fa fa-stop" aria-hidden="true">
                            <p>{!! $value->skill !!}</p>
                            </i>

                           
	                        
	                       
	                    </li> 
                        @endforeach
	                   
	                </ul>
	            </div>
	        </div>
        </section>

        <section class="about_light04" id="a03">
	        <div class="about_note">
	          {!! $AboutSet->s1_quality !!}
	        </div>

	        <div class="about_light04_slick">
	            <ul>
                    @foreach($AboutCertificate as $value)
	                <li>
                        <div class="slick04_img">
                            <!-- 寬160px*225  -->
                            <img src="{{ $value->image }}" alt="">
                            <span>{{ $value->title }}</span>
                        </div>
	                </li>
                    @endforeach
	                <li>
                        <div class="slick04_img">
                             <!-- 寬160px*225  -->
                            <img src="../../upload/about/pager.jpg" alt="">
                            <span>BSMI 正字標記證書</span>  
                        </div>
	                </li>
	                <li>
                        <div class="slick04_img">
                             <!-- 寬160px*225  -->
                            <img src="../../upload/about/pager.jpg" alt="">
                            <span>ISO 9001 URS</span>
                        </div>
	                </li>
                    <li>
                        <div class="slick04_img">
                             <!-- 寬160px*225  -->
                            <img src="../../upload/about/pager.jpg" alt="">
                            <span>ISO 9001 URS</span>
                        </div>
                    </li>
                    <li>
                        <div class="slick04_img">
                             <!-- 寬160px*225  -->
                            <img src="../../upload/about/pager.jpg" alt="">
                            <span>ISO 9001 URS</span>
                        </div>
                    </li>
                     <li>
                        <div class="slick04_img">
                             <!-- 寬160px*225  -->
                            <img src="../../upload/about/pager.jpg" alt="">
                            <span>ISO 9001 URS</span>
                        </div>
                    </li>
	                
	            </ul>
	            <div class="number_bouttn  light04_btn">
	                <i class="fa fa-chevron-right light04_btn_right slick-arrow"></i>
	            </div>

	        </div>
            

        </section> 
        <div class="lightbox_aa">
            <div class="half_color_box ">
                <a href="ajvascript:;" class="out">
                    <div class="half_color ">
                        <span>back</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<script>
    $(".about_lightbox01").css('position','relative');
    $(".a_id_box a , .light_select .select_ul a").click(function(e){
        e.preventDefault();
        

         var a = $(this).attr('data');
        
        togo=$('#' + a).position().top;
        $('.lbox_inner').animate({ scrollTop: togo}, 1000);  
    });
</script>