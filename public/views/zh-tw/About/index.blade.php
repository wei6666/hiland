
@extends('template')

    <!--RWD Setting-->
@section('css')

@stop
@section('content')

@include($locale.'.header') 
    <main >
       <!-- 大標題 -->
        <section class="big_title" id="top">
            <div class="banner_inner">
                {{-- <span>Brand Story</span>
                <div class="banner_note">
                    <h5>品牌故事</h5>
                    <span>提供各式各樣的磁磚，滿足您對磁磚、藝術空間的所有渴望</span>
                </div> --}}
                {!! $AboutSet->title !!}
            </div>
            <a href="#two" class="scroll_line_box">
                <span>s</span>
                <span>c</span>
                <span>r</span>
                <span>o</span>
                <span>l</span>
                <span>l</span>
                <div class="down_page">
                    <span></span>
                    <div class="angle"></div>
                </div>
            </a>
        </section>
        <!-- 內容 -->
        <section class="content_box " id="two">
            <div class="select_box main_select">
                <!-- 電腦menu -->
                <div class="select_inner">
                    <span class="select_style">{!! $AboutSet->select_title !!}</span>
                    <ul class="select_ul">
                        <li class="on"><a href="#about01">{!! $AboutSet->s1 !!}</a></li>
                        <li ><a href="#about01">{!! $AboutSet->s2 !!}</a></li>
                        <li><a href="#about02">{!! $AboutSet->s3 !!}</a></li>
                        <li><a href="#about03">{!! $AboutSet->s4 !!}</a></li>
                    </ul>
                 </div>
                <div class="a_id_box">
                    <span class="span_space">{!! $AboutSet->s1 !!}</span><i class="fa fa-caret-down" aria-hidden="true"></i>
                    <ul>
                        <li><a href="#about01">{!! $AboutSet->s1 !!}</a></li>
                        <li ><a href="#about01">{!! $AboutSet->s2 !!}</a></li>
                        <li><a href="#about02">{!! $AboutSet->s3 !!}</a></li>
                        <li><a href="#about03">{!! $AboutSet->s4 !!}</a></li>
                    </ul>
                </div>
            </div> 
             <!-- rwd menu -->  
            
        </section>
        <section class="about01 addshow" id="about01"  href="{{ ItemMaker::url('about?s=1') }}" last="{{ ItemMaker::url('about') }}">
            <div class="about_title">
               {!! $AboutSet->s1_title !!}
            </div>
           
            <div class="about_content" >
                <!-- 圖寬600*380 -->
                <div class="about01_left">
                    <div class="about01_img">
                        <img src="{!! $AboutSet->s1_photo1 !!}" alt="">
                        <div class="about01_logo">
                            <img src="/assets/img/about_logo.svg" alt="">
                        </div>
                    </div>
                    <div class="about_note">
                        {!! $AboutSet->s1_title_area1 !!}
                    </div>
                </div>
                <div class="about01_right">
                    <div class="about01_img">
                        <img src="{!! $AboutSet->s1_photo2 !!}" alt="">
                        <div class="about01_logo">
                            <img src="/assets/img/hiland_logo.jpg" alt="">
                        </div>
                    </div>
                    <div class="about_note">
                        {!! $AboutSet->s1_title_area2 !!}
                    </div>
                </div>
            </div>
        
        </section>
       
        <section class="about02 addshow" id="about02" href="{{ ItemMaker::url('about?s=2') }}" last="{{ ItemMaker::url('about') }}">
            <div class="about_title">
                {!! $AboutSet->s2_title !!}
            </div>
            
            <div class="about_content">
                <div class="about02_img">
                <!-- 圖寬780*450 -->
                <img src="{!! $AboutSet->s2_photo !!}" alt="">
                    <div class="about_note">
                        <div class="about_note_relative">
                            {!! $AboutSet->s2_area_title !!}
                            <div class="div_cover">
                                <span class="yellow_an"></span>
                                <span class="green_an"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
        </section>

        <section class="about03 addshow" id="about03" href="{{ ItemMaker::url('about?s=3') }}" last="{{ ItemMaker::url('about') }}">            
                <div class="about_content">
                    <div class="about02_img">
                    <!-- 圖寬780*450 -->
                        <img src="{!! $AboutSet->s3_photo !!}" alt="">
                        <div class="about_note">
                            <div class="about_note_relative">
                               {!! $AboutSet->s3_area_title !!}
                                <div class="div_cover">
                                    
                                </div>
                            </div>
                        </div>                    
                    </div>
                </div>
            </section>        
    </main>
    @include($locale.'.footer') 
@section('script')  
    <!--主要jQuery-->
    
    <script >
        @if(!empty($_GET['s']))
            @if($_GET['s']==1)
            $("#about01").click()
            @endif
            @if($_GET['s']==2)
            $("#about02").click()
            @endif
            @if($_GET['s']==3)
            $("#about03").click()
            @endif
        @endif

    </script>


@stop


@stop