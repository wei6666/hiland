
	    <div class="footer_top">
	        <!-- rwd時的icon -->
	        <div class="rwd_icon">
			
			</div>
			<div class="footer_icon">
             <!-- icon大小50*50 -->
                <img src="../../assets/img/footer_icon02.png" alt="">
                <img src="../../assets/img/footer_icon01.png" alt="">
				<img src="../../assets/img/footer_icon.png" alt="">
				<img src="../../assets/img/footer_icon03.png" alt="">
				<img src="../../assets/img/footer_icon04.png" alt="">
			</div>
			
			@if(!empty($is_home))
			<div class="gototop">
				<a href="javascript:;" onclick='$.fn.fullpage.moveTo(1);'<h5>top</h5></a>
			</div>

			@else
			<div class="gototop">
				<a href="javascript:;" onclick='$("html, body").animate({ scrollTop: 0 }, "slow");'<h5>top</h5></a>
			</div>

			@endif
			
			
		</div>
		<div class="footer_bottom_box">
		   <div></div>
			<ul class="footer_menu">
				<li><a href="{{ ItemMaker::url("about") }}">關於黃山石</a></li>
				<li><a href="{{ ItemMaker::url("home") }}"  target="_blank">黃山石瓷磚</a></li>
				<li><a href="javascript:;" class='privacy_inter' id='private'>隱私權政策</a></li>
				<li><a href="{{ ItemMaker::url("contact") }}">聯絡我們</a></li>
				<li><a href="{{ ItemMaker::url("favorite") }}">我的最愛</a></li>
			</ul>
			 <div class="follow">
		     	 <span>FOLLOW US</span>
		     	 <a href="@if(!empty($Setting['company_fb'])) {{ $Setting['company_fb'] }} @else javascript:; @endif" target="_blank"> <i class="fa fa-facebook" aria-hidden="true"></i></a>
		     	  <a href="@if(!empty($Setting['company_youtube'])) {{ $Setting['company_youtube'] }} @else javascript:; @endif" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
		     </div>
			 <div class="footer_contact">
			 	  <span>與我們聯絡</span>
			 	  <a href="{{ ItemMaker::url("contact") }}" target="_blank"><i class="icon-meassage"></i></a>
			 	  
			 </div>
			<div class="footer_bottom">
				 <span>
				 © Hiland Co., Ltd.  ALL RIGHTS RESERVED. <br> Designed by 
				 <a href="http://www.wddgroup.com/">WDD</a>
				 </span>
			</div>
			
		</div>
@include($locale.'.private')
