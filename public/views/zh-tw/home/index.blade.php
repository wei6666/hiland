@extends('template')

    <!--RWD Setting-->
@section('css')
	<link rel="stylesheet" href="/assets/js/vendor/fullPage/jquery.fullPage.css">
@stop
@section('content')

@include($locale.'.header') 
    <main id="fullpage">
        
        <!-- 首頁第一 -->
        <section class="home01 section" id="top">             
            <header id="header">
            </header>
            <!-- 圖寬2150*1000 -->
            <ul class="home_slick" >
            	@foreach($HomeImage as $value)
                <li>
                    <div class="home_slick_inner" style="background: url({{ $value->image }}) center center /cover no-repeat;"> </div>
                    <div class="home_banner_title" style="color: {{  $value->color }}">
                        <!-- 大標題和小標題客戶選擇自行斷行  -->
                        <!-- 小標和大標可選擇黑色或白色顏色 -->
                        <h4>{!! $value->title !!}</h4>
                        <span>{!! $value->sub_title !!}</span>
                    </div>
                </li>
                @endforeach             
            </ul>
            <div class="home_logo">
                <a href="javascript:;">
                    <div class="logo_img">
                        <div class="logo_line"><img src="{{ $Home->home_tag }}" alt=""></div>
                        <div class="logo_img_box"> <img src="{{ $Home->home_tag }}" alt=""></div>
                    </div>
                    <div class="logo_w">
                        <img src="/assets/img/logo01.svg" alt="">
                        <img src="/assets/img/logo02.svg" alt="">
                        <img src="/assets/img/logo03.svg" alt="">
                        <img src="/assets/img/logo04.svg" alt="">
                        <img src="/assets/img/logo05.svg" alt="">
                    </div>
                </a>
            </div>
            <div class="home_logo_rwd">
              <a href="../home/index.html">
                <img src="/assets/img/logo.svg" alt="">
                <img src="/assets/img/logo_word_rwd.svg" alt="">
              </a>
            </div>
            <div class="home_banner_menu">
                <div class="hamburger_box open_menu">
                    <h5>menu</h5>
                    <div class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
            <span class="home_span"></span>
        </section>
 
        <section class="home02 section">
            <div class="home02_inner">
                <!--  圖slick -->
                <!--  連結有九個可以選擇   顏色可換黑白 預設是白色 如果要黑色 class 加 a_black
            上左 class= "home02_tl"
            上中 class= "home02_tm"
            上右 class= "home02_tr"
            中左 class= "home02_ml"
            中中 class= "home02_mm"
            中右 class= "home02_mr"
            下左 class= "home02_dl"
            下中 class= "home02_dm"
            下右 class= "home02_dr"-->
                <div class="home2_img_slick">
                    <ul> 
                    	@foreach($Style as $value)
                        <li>
                            <div class="home2_img_abox">
                        <img src="{{ $value->image->image }}" alt="">
                    		@foreach( $value->image->proarr as $key => $value1)
                                @if( !is_int($key))
                                <?php $tt=explode('-', $value1); ?>                                
		                                <div class="home02_a  home02_{{ $key }}">
                                             <a class="openProduct detail_lbox_btn" href="{{ ItemMaker::url('productdetail/'.$tt[2].'?Open='.$tt[3]) }}" last="{{ ItemMaker::url('home') }}">
		                                    {{-- <a href="ajvascript:;" id='{{ $tt[3] }}'> --}}
		                                        <span class="small_yellow_ball"></span>
		                                        <div class="product_number">
		                                            <span>{{ $tt[1] }}</span>
		                                            <span>{{ $tt[0] }} </span>
		                                        </div>  
		                                    </a>
		                                </div>
		                           
                                @endif
	                        @endforeach
                            </div>
                        </li> 
                        @endforeach

                    </ul>
                </div>
                <!--  note slick -->
                <div class="slick_right">
                    <div class="slick02_number">
                        <ul><?php $co=1;?>
                        	@foreach($Style as $value)
	                            <li><span>o{{ $co }}</span></li>
	                            <?php $co++;?>
                            @endforeach
                        </ul>
                    </div>
                    <div class="home02_note">
                        <ul>
                        	@foreach($Style as $value)
                            <li>
                                <!-- 自行選擇斷行 -->
                                <span class="b_title" style="color:{{ $value->color }}">{!! $value->home_title !!}</span>
                                <div class="note_snote">
                                    <!-- 自行選擇斷行 -->
                                    <font color='{{ $value->color }}'>
                                        <h5 class="m_title" >{!! $value->home_sub_title !!}
                                        </h5>
                                    </font>
                                    <font color='{{ $value->color }}'>
                                        <p class="s_content" style="color:{{ $value->color }}">
                                            {!! $value->home_content !!}
                                        </p>
                                    </font>
                                </div>
                                <a href="{{ ItemMaker::url('styledetail/'.$value->id) }}" class="more01" tabindex="0">
                                    <span class="a_w">{!! $value->home_more_btn !!}</span>
                                </a>
                            </li>
                            @endforeach                          
                        </ul>
                    </div>
                    <div class="number_bouttn  s02_btn">
                            <i class="fa fa-chevron-left s02_btn_left slick-arrow"></i>
                            <i class="fa fa-chevron-right s02_btn_right slick-arrow"></i>
                    </div>  
                  <!--   進場方塊 -->
                    <div class=" number_bouttn button_area">
                        <div class="square_box0">
                                <div class="green_sb"></div>
                                <div class="yellow_sb"></div>
                        </div>
                        <div class="square_box01">
                                <div class="green_sb"></div>
                                <div class="yellow_sb"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- 背景 -->
            <div class="home2_imgbg_slick">
                <ul>
                	@foreach($Style as $value)
                    <li>
                        <div class="home02_bg"   style="background:url('{{ $value->home_backgroung }}') center center /cover no-repeat;">
                        </div>
                    </li>
                    @endforeach
                    
                </ul>
            </div>
            <!-- 圖片 -->
        </section>
        <!--對分-->
        <section class="home03 section">
            <!-- 圖寬1920高1080 -->
            <div class="home03_frame">

                <!-- 右區塊 -->
                <div class="home03_frame_block base">

                    <!-- 圖片 -->
                    <div class="base_img">
                        
                        <div class="bg_img" style="background: url({{ $ProductBig[1]->home_image }}) center center /cover no-repeat;"></div>
                    
                        <!-- 文字 -->
                        <a class="text_base" href="{{ ItemMaker::url('productcategory?cl='. $ProductBig[1]->id) }}">
                            <div class="half_a_inner">
                                <div class="content_300">
                                    <font color='{!! $ProductBig[1]->color !!}'>
                                    <h6>{!! $ProductBig[1]->home_title !!}</h6>
                                    <p>{!! $ProductBig[1]->home_sub_title !!}</p>
                                    <span>{!! $ProductBig[1]->title !!}</span></font>
                                </div>
                            </div>
                        </a>
                        
                    
                    </div>

                </div>
    
                <!-- 左區塊 -->
                <div class="home03_frame_block up">

                    <!-- 圖片 -->
                    <div id="sidebar" class="up_img">
                        
                        <div class="bg_img" style="background: url({!! $ProductBig[0]->home_image !!}) center center /cover no-repeat;">

                            <!-- 文字 -->
                            <a class="text_up" href="{{ ItemMaker::url('productcategory?cl='. $ProductBig[0]->id) }}">
                                <div class="half_a_inner">
                                    <div class="content_300">
                                        <font color='{!! $ProductBig[0]->color !!}'>
                                        <h6>{!! $ProductBig[0]->home_title !!}</h6>
                                        <p>{!! $ProductBig[0]->home_sub_title !!}</p>
                                        <span>{!! $ProductBig[0]->title !!}</span>
                                    </font>
                                    </div>
                                </div>
                            </a>

                        </div>
                    
                    </div>

                </div> 


                <!-- 拖曳按鈕 -->
                <div id="split-bar" class="split-bar">
                    <div class="bar_ball hvr-ripple-out02">
                        <div class="home03_square">
                            <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                            <span class="square45"></span>
                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                        </div>
                        <span class="drag_w"> drag</span>
                    </div>
                </div>

            </div>
            
            <!-- rwd_home03 -->
            <div class="home03_1024">
                <div class="home03_rwd_left" style="background: url({!! $ProductBig[0]->home_image !!})center center /cover no-repeat;">
                    <a href="../style/index.html">
                        <div class="home03_rwd_text">
                            <h6>{!! $ProductBig[0]->home_title !!}</h6>
                            <p>{!! $ProductBig[0]->home_sub_title !!}</p>
                            <span>{!! $ProductBig[0]->title !!}</span>
                        </div>
                    </a>
                </div>
                <div class="home03_rwd_right" style="background: url({!! $ProductBig[1]->home_image !!}) center center /cover no-repeat;">
                    <a href="../style/index.html">
                        <div class="home03_rwd_text">
                            <h6>{!! $ProductBig[1]->home_title !!}</h6>
                            <p>{!! $ProductBig[1]->home_sub_title !!}</p>
                            <span>{!! $ProductBig[1]->title !!}</span>
                        </div>
                    </a>
                </div>
            </div>
        </section>
        <section class="home04 section">
            <div class="home04_inner">
                <!--  產品圖 -->
                <div class="slick_left">
                    <div class="slick04_left_inner">
                        <div class="slick04_number">
                            <ul>
                                <?php $co=1;?>
                                @foreach($ProductCategory as $value)
                                <li><span>o{{ $co }}</span></li>
                                <?php  $co++; ?>
                                @endforeach
                              
                            </ul>
                        </div>
                        <div class="home04_note">
                                <ul>

                                @foreach($ProductCategory as $value)
                                <li>                                   
                                    <!-- 自行選擇斷行 -->
                                    <span class="b_title">{!! $value->title !!}</span>
                                    <div class="note_snote">
                                        <!-- 自行選擇斷行 -->
                                        <h5 class="m_title">{{ $value->download_title }}</h5>
                                        <p class="s_content">{!! $value ->home_sub_title !!}</p>
                                    </div>
                                    <a href="{{ $value->download }}" target="_blank" class="more01" tabindex="0">
                                        <span class="a_b">dowload</span>
                                    </a>
                                </li>
                                @endforeach
                                
                            </ul>
                        </div>
                        <div class="number_bouttn  s04_btn">
                            <i class="fa fa-chevron-left s04_btn_left slick-arrow"></i>
                            <i class="fa fa-chevron-right s04_btn_right slick-arrow"></i>
                        </div>
                             <!--   進場方塊 -->
                        <div class=" number_bouttn button_area04">
                            
                            <div class="square_box0">
                                    <div class="green_sb"></div>
                                    <div class="yellow_sb"></div>
                            </div>
                            <div class="square_box01">
                                    <div class="green_sb"></div>
                                    <div class="yellow_sb"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="home04_img_slick">
                    <ul>
                        @foreach($ProductCategory as $value)
                        <li style="background: url({{ $value->home_image }}) center center /cover no-repeat;"></li>
                         @endforeach
                        
                        
                    </ul>
                </div>
                <div class="home04_bg">
                    <ul>
                       @foreach($ProductCategory as $value)
                        <li style="background: url({{ $value->home_image_bg }}) center center /cover no-repeat;"></li>
                         @endforeach
                    </ul>
                </div>
            </div>
        </section>
        <section class="home05 section no_remove_show">
            <!-- 圖寬1920高900 -->
            <div class="home05_bg" style="background: url({{ $Home->where_photo  }}) center center /cover no-repeat; ">
                <div class="half_h5">
                    <h6 class="b_title">Where To Buy</h6>
                    <h5 class="m_title">前往購買</h5>
                    <div class="home05_dropdown">
                        <div class="a_id_box">
                            <span>選擇你所在的地區</span><i class="fa fa-caret-down" aria-hidden="true"></i>
                            <ul>
                                @foreach($ContactCountry as $value)
                                <li style="padding:  0;" ><a style="display: block;
    padding: 10px 0;" href="{{ ItemMaker::url('location?lo='.$value->title) }}">{{ $value->title }}</a></li>
                                @endforeach
                              
                               
                            </ul>
                        </div>
                    </div>
                </div>     
            </div>

        </section>
       <footer id="footer" class="section fp-auto-height">
            @include($locale.'.footer1') 
        </footer>
        
    </main> 
    @include($locale.'.private')

@section('script')  
 	<script src="/assets/js/vendor/fullPage/jquery.fullPage.js"></script>
    <script src="/assets/js/main_home.js"></script> 
    <script >

    $("body").keydown(function(e) {
        setTimeout(function(){
        if(e.keyCode == 38 || e.keyCode == 33) 
        { 

            if ( $('.home01').hasClass("active") == true ) 
            {
                $('header').removeClass('fixed_header headerFadeIn');
            }
            else
            {
                $('header').addClass('fixed_header headerFadeIn');
            }


            }
            else 
            { 

                $('header').removeClass('fixed_header headerFadeIn');

            }
        },200);
    });

    </script>
   
@stop


@stop