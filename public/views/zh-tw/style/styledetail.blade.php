
@extends('template')

    <!--RWD Setting-->
@section('css')

@stop
@section('content')

@include($locale.'.header') 
    <main>
        <section class="title_1440box addshow" id="top">
            <div class="title_inner">

                <span>{{ $Styledata['in_title'] }}</span>
                <div class="banner_note">
                    {!! $Styledata['in_sub_title'] !!}
                </div>
            </div>
            <a href="{{ ItemMaker::url('style') }}" class="back_line">back</a>
        </section>
        <section class="stylelist01 addshow">
            <div class="stylelist_slick">
                <ul>
                    @foreach($StylePhoto as $value)
                    <li>
                        <div class="stylelist_slickinner">
                            <div class="stylelist_bg"><img src="{{  $value->image }}" alt=""></div>
                            <article class="slicklist_abox">
                                <div class="slicklist_ainner">
                                    <!-- 字可以白色和黑色 -->

                                    @foreach($value->proarr as $key1 => $value1)
                                        @if( !is_int($key1))
                                        <?php  $tt=explode('-', $value1); ?>
                                            <div class="stylelist_nine_a stylelist_{{ $key1 }}">
                                                <div class="style_nine_rela">
                                                    <div class="nine_bg" style="background:url('{{$tt[4]}}') center center /cover no-repeat; "></div>
                                                   

                                                    <a class="openProduct detail_lbox_btn" href="{{ ItemMaker::url('productdetail/'.$tt[2].'?Open='.$tt[3]) }}" last="{{ ItemMaker::url('styledetail/'.$id) }}">

                                                        <span class="small_yellow_ball"></span>
                                                        <div class="product_number colorfff">
                                                            
                                                            <span>{{ $tt[0] }}</span>
                                                            <span>{{ $tt[1] }}</span>
                                                        </div>  
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                  @endforeach  
                                </div>
                            </article>
                        </div>
                    </li>
                    @endforeach
                   
                </ul>
                <div class="style_page_dotnumber style_dotstyle">
                </div>
                <!-- rwd號碼換成五個輪播 -->
                
                <div class="number_bouttn style_btn_box">
                    <i class="fa fa-chevron-left style_btn_left slick-arrow"></i>
                    <i class="fa fa-chevron-right style_btn_right slick-arrow"></i>
                </div>
            </div>
            <div class="style_rwdnumber">
                    <ul>
                        <?php  $count =1;?>
                        @foreach($StylePhoto as $value)
                            <li>O{{ $count  }}</li>
                            <?php $count++;?>
                        @endforeach
                        
                    </ul>
                </div>
            <div class="half_color_box stylelist_button">
                <a href="{{ ItemMaker::url('style') }}">
                    <div class="half_color ">
                        <span>back</span>
                    </div>
                </a>
            </div>
            <div class="more_space">
                <h5>more space</h5>
            </div>
            <div class="stylelist_more">
                <!-- 圖570*740 -->
                <div class="stylelist_m_l">
                    <a href="{{ItemMaker::url('styledetail/'.$last['id'])}}"><img src="{{ $last['more_image'] }}" alt=""></a>
                    <div class="stylelist_morenumer">
                        <span>O{{ $last['index']/1+1 }}</span>
                        <div class="stylelist_nextnote">
                            <h5>{{ $last['out_title'] }}</h5>
                            <h6>{{ $last['out_sub_title'] }}</h6>
                        </div>
                    </div>
                </div>
                <div class="stylelist_m_r">
                    <a href="{{ItemMaker::url('styledetail/'.$next['id'])}}">
                        <img src="{{ $next['more_image'] }}" alt="">
                    </a>
                    <div class="stylelist_morenumer">
                        <span>O{{ $next['index']/1+1 }}</span>
                        <div class="stylelist_nextnote">
                            <h5>{{ $next['out_title'] }}</h5>
                            <h6>{{ $next['out_sub_title'] }}</h6>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
        <!-- 內容 -->
    </main>
    @include($locale.'.footer') 
    
@section('script')
<script >
    
$('.detail_lbox_btn').on('click',function(){
    
    $('.stylelist_more').css('opacity',1)
});

</script>
@stop
@stop