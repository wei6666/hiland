
@extends('template')

    <!--RWD Setting-->
@section('css')

@stop
@section('content')

@include($locale.'.header') 
	<main >
	   <!-- 大標題 -->

		<section class="big_title" id="top">
			<div class="banner_inner">
				
				<span>{{ $StyleSet->title }}</span>
				<div class="banner_note">
						{!! $StyleSet->sub_title !!}
				</div>
			</div>
			<a href="#two" class="scroll_line_box">
				<span>s</span>
				<span>c</span>
				<span>r</span>
				<span>o</span>
				<span>l</span>
				<span>l</span>
				<div class="down_page">
					<span></span>
					<div class="angle"></div>
				</div>
			</a>
		</section>
		<!-- 內容 -->
		<section class="stylemenu01" id="two">
		  	<div class="select_box">
			    <!-- 電腦menu -->
			    <div class="select_inner">
					<span class="select_style">select：</span>
					<ul class="select_ul">
						<?php $count=1; ?>
						@foreach($Styledata as $value)
							<li><a href="#s0{{$count}}">{{ $value->out_sub_title }}</a></li>
							<?php  $count++;?>
						@endforeach
						
					</ul>
			    </div>
			     <!-- rwd menu -->
			    <div class="a_id_box">
			        <span >@if(!empty($Styledata[0]->out_sub_title)){{ $Styledata[0]->out_sub_title }}@endif </span><i class="fa fa-caret-down" aria-hidden="true"></i>
			   	    <ul>
			   	    	<?php $count=1; ?>
						@foreach($Styledata as $value)
							<li><a href="#s0{{$count}}">{{ $value->out_sub_title }}</a></li>
							<?php  $count++;?>
						@endforeach
			   	    </ul>
			    </div>
			</div>
			<ul class="stylemenu_content addshow">
				<?php $count=1; ?>
				@foreach($Styledata as $value)
				<li id="s0{{  $count }}">
					<div class="stylemenu_img">
					    <!-- 圖寬730px 高900 -->
					    <a href="{{ItemMaker::url('styledetail/'.$value->id)}}">
						    <img src="{{ $value->out_image }}" alt="">
						</a>
					</div>
					<div class="stylemenu_title">
						<div class="stylemenu_title_inner">
						    <div class="stylemenu_number">
						    	<h6>O{{  $count }}</h6>
						    </div>  
                            <div class="stylemenu_note">
                                <h5>{{ $value->out_title }}</h5>
                                <span>{{ $value->out_sub_title }}</span>
                                <div class="half_color_box">
								     <a href="{{ItemMaker::url('styledetail/'.$value->id)}}">
										<div class="half_color ">
											<span>view more</span>
										</div>
									</a>
	                            </div>
                            </div>							
						</div>
					</div>
				</li>
				<?php  $count++;?>
				@endforeach	
			</ul>
		</section>
        
		
	</main>
	@include($locale.'.footer') 
	
@section('script')  

@stop
@stop