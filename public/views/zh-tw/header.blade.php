<!-- header_menu -->
<header id='header'>
	  <header >
  		<div class="header_logo">
  		  <a href="{{ ItemMaker::url('home') }}" >
  				<img src="/assets/img/logo_word.svg" alt="">
  				<img src="/assets/img/logo.svg" alt="">
  			     <h1>黃山石磁磚</h1>
  		  </a>
  		</div>
		
		<div class="right_menu_box">
  		<ul class="heaer_menu">
  			<li><span>產品系列</span><a href="{{ ItemMaker::url('productcategory') }}"></a></li>
  			<li><span>黃山石風格</span><a href="{{ ItemMaker::url('style') }}"></a></li>
  			<li><span>常見問題</span><a href="{{ ItemMaker::url('faq') }}"></a></li>
  			<li><span>經銷據點</span><a href="{{ ItemMaker::url('location') }}"></a></li>
  			<li><span>新聞與活動</span><a href="{{ ItemMaker::url('news') }}"></a></li>
  			<li><span>我的最愛</span><a href="{{ ItemMaker::url('favorite') }}"></a></li>
  			<li class="rwd_on open_menu">
  				<div class="hamburger_box">
  		        	<h5>menu</h5>
  		        	<div class="hamburger">
  		        		<span></span>
  		        		<span></span>
  		        		<span></span>
  		        	</div>
          		</div>
  			</li>
  		</ul>
        
    </div>
    </header>

    </header>