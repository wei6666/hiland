

   <section class="location_lightbox">
      <div class="location_inner">
     <!--  地圖 -->
       <div class="location_map">
            <div class="map"></div>
            <!--  關掉-->
          <div class="location_close_icon">
            <div class="close">
                <span class="x_close"></span>
            </div>
          </div>
       </div>
     <!--  資訊 -->
       <div class="location_i">
         <div class="location_i_header">
           <span>{{ $LocationSet->in_title }}</span>
           <p>{{ $LocationSet->in_sub_title }}</p>
        </div>
        <div class="location_i_content">
           <span class="city">{{ $Location->city }} </span>
           <span class="company">{{ $Location->connect_info }}</span>
           <p>@if($Location->is_show_center ==0) 建材行 @else 展示中心@endif</p>
           <div class="location_adress">
             <h6>{!! $Location->address !!}</h6>
              <h5><b>T </b> {!! $Location->phone !!}</h5>
           </div>
          
        </div>
         
       </div>
      </div>
   </section>
