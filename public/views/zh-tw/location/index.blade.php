
@extends('template')

    <!--RWD Setting-->
@section('css')

@stop
@section('content')

@include($locale.'.header') 
    <main>
        <!-- 大標題 -->
        <section class="big_title" id="top">
            <div class="banner_inner">
                <span>{!! $LocationSet->title !!}</span>
                <div class="banner_note">
                   {!! $LocationSet->sub_title !!}
                </div>
            </div>
            <a href="#two" class="scroll_line_box">
                <span>s</span>
                <span>c</span>
                <span>r</span>
                <span>o</span>
                <span>l</span>
                <span>l</span>
                <div class="down_page">
                    <span></span>
                    <div class="angle"></div>
                </div>
            </a>
        </section>
        <!-- 內容 -->
        <section class="content_box location01" id="two">
            <div class="contact_header">
                <div class="chose_location">
                    <div class="chose_inner">
                        <div class="small_title">
                            <i class="icon-search"></i>
                            <h6> {!! $LocationSet->select_title !!}</h6>
                        </div>
                        <div class="see_all_menu">
                        
                            <div class="location_menu">
                                <select class="dropdown" >
                                    <option value="1">-</option>
                                    @foreach($country as $key => $value)
                                    <option value="1">{{ $key }}</option>
                                    @endforeach
                                </select>
                            </div>

                        @foreach($country as $key => $value)
                            <div class="store_menu" country="{{$key}}" >
                                <select class="dropdown">
                                        <option value="1">-</option>                                        
                                        @foreach($value as $key1 => $value1)
                                                <option  country="{{ $key }}">{{$value1->title}}</option>
                                        @endforeach
                                    </select>
                            </div>
                        @endforeach
                            <div class="ok_box">
                                <a href="javascript:;">
                                     <img src="../../assets/img/ok.png" alt="">
                                </a>
                               
                            </div>
                        </div>
                    </div>
                </div>
                <div class="location_see_all">
                    <div class="location_all_inner">
                        <div class="small_title">
                            <i class="icon-custom"></i>
                            <h6>{{ $LocationSet->show_center_title }}</h6>
                        </div>
                        <p>
                            {!! $LocationSet->show_center_content !!}
                        </p>
                    </div>
                </div>
            </div>
            <div class="contact_bigbox">
                <!--   標題 -->
                <ul class="location_header">
                    <li>
                        
                    {!! $LocationSet->area_title !!}</li>
                    <li>
                        
                    {!! $LocationSet->company_title !!}</li>
                    <li>
                        {!! $LocationSet->map_title  !!}</li>
                </ul>
                <!--   內容 -->
                <div class="loaction_rwd_header">
                    <span>高雄 </span>
                </div>
                <ul class="con_content">
                    <?php  $co=0;?>
                    @foreach($Location as $value)
                    <li @if($co>5) style='display: none;' @else class='toshow' @endif <?php $co++;?> >
                        <!-- 地區 -->
                        <div class="location_li01">
                            <span>{{ $value->city }}</span>
                        </div>
                        <!--    location 店家資訊 -->
                        <div class="location_detial">
                            <div class="location_store">
                                <div class="location_store_inner">
                                    <span>{{ $value->connect_info }}</span>
                                    <p>@if($value->is_show_center ==0) 建材行 @else 展示中心@endif</p>
                                </div>
                            </div>
                            <div class="location_adress_box">
                                <div class="location_a_box">
                                    <a href="javascript:;" target="_self">   
                                  <i class="icon-location"></i>
                                   <span>{{ $LocationSet->map_clicker_title }}</span>
                                  </a>
                                </div>
                                <div class="location_adress">
                                    <h6>{!! $value->address !!}</h6>
                                    <h5><b>T </b>{!! $value->phone !!}</h5>
                                </div>
                            </div>
                        </div>
                        <!--    location icon -->
                        <div class="ul_icon ">
                            <div class="location_a_box">
                                <a class='openlocation' href="{{ ItemMaker::url('locationdetail/'.$value->id.'?Open='.$value->id) }}" last="{{ ItemMaker::url('location') }}" title="{{ $value->connect_info }}" x='{{ $value->map_x }}' y='{{ $value->map_y }}'>
                                    <i class="icon-location"></i>
                                   <span>{{ $LocationSet->map_clicker_title }}</span>
                                  </a>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
                @if(0)
                 <div class="loaction_rwd_header">
                    <span>南投,台中,彰化</span>
                </div>
                <ul class="con_content">
                    <li>
                        <!-- 地區 -->
                        <div class="location_li01">
                            <span>南投,台中,彰化</span>
                        </div>
                        <!--    location 店家資訊 -->
                        <div class="location_detial">
                            <div class="location_store">
                                <div class="location_store_inner">
                                    <span>純泰企業有限公司</span>
                                    <p>展示中心</p>
                                </div>
                            </div>
                            <div class="location_adress_box">
                                <div class="location_a_box">
                                    <a href="javascript:;" target="_self"> 
                                  <i class="icon-location"></i>
                                   <span>LOCATION</span>
                                  </a>
                                </div>
                                <div class="location_adress">
                                    <h6>812 高雄市仁武區鳳仁路95之32號</h6>
                                    <h5><b>T </b> 07-3717965s</h5>
                                </div>
                            </div>
                        </div>
                        <!--    location icon -->
                        <div class="ul_icon ">
                            <div class="location_a_box">
                                <a href="javascript:;" target="_self">  
                                  <i class="icon-location"></i>
                                   <span>LOCATION</span>
                                  </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <!-- 地區 -->
                        <div class="location_li01">
                            <span>南投,台中,彰化</span>
                        </div>
                        <!--    location 店家資訊 -->
                        <div class="location_detial">
                            <div class="location_store">
                                <div class="location_store_inner">
                                    <span>純泰企業有限公司</span>
                                    <p>展示中心</p>
                                </div>
                            </div>
                            <div class="location_adress_box">
                                <div class="location_a_box">
                                    <a href="javascript:;" target="_self">         
                                  <i class="icon-location"></i>
                                   <span>LOCATION</span>
                                  </a>
                                </div>
                                <div class="location_adress">
                                    <h6>812 高雄市仁武區鳳仁路95之32號</h6>
                                    <h5><b>T </b>07-3717965s</h5>
                                </div>
                            </div>
                        </div>
                        <!--    location icon -->
                        <div class="ul_icon ">
                            <div class="location_a_box">
                                <a href="javascript:;" target="_self"> 
                                  <i class="icon-location"></i>
                                   <span>LOCATION</span>
                                  </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <!-- 地區 -->
                        <div class="location_li01">
                            <span>南投,台中,彰化</span>
                        </div>
                        <!--    location 店家資訊 -->
                        <div class="location_detial">
                            <div class="location_store">
                                <div class="location_store_inner">
                                    <span>純泰企業有限公司</span>
                                    <p>展示中心</p>
                                </div>
                            </div>
                            <div class="location_adress_box">
                                <div class="location_a_box">
                                    <a href="javascript:;" target="_self"> 
                                  <i class="icon-location"></i>
                                   <span>LOCATION</span>
                                  </a>
                                </div>
                                <div class="location_adress">
                                    <h6>812 高雄市仁武區鳳仁路95之32號</h6>
                                    <h5><b>T </b> 07-3717965s</h5>
                                </div>
                            </div>
                        </div>
                        <!--    location icon -->
                        <div class="ul_icon ">
                            <div class="location_a_box">
                                <a href="javascript:;" target="_self">  
                                  <i class="icon-location"></i>
                                   <span>LOCATION</span>
                                  </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <!-- 地區 -->
                        <div class="location_li01">
                            <span>南投,台中,彰化</span>
                        </div>
                        <!--    location 店家資訊 -->
                        <div class="location_detial">
                            <div class="location_store">
                                <div class="location_store_inner">
                                    <span>純泰企業有限公司</span>
                                    <p>展示中心</p>
                                </div>
                            </div>
                            <div class="location_adress_box">
                                <div class="location_a_box">
                                    <a href="javascript:;" target="_self">         
                                  <i class="icon-location"></i>
                                   <span>LOCATION</span>
                                  </a>
                                </div>
                                <div class="location_adress">
                                    <h6>812 高雄市仁武區鳳仁路95之32號</h6>
                                    <h5><b>T </b>07-3717965s</h5>
                                </div>
                            </div>
                        </div>
                        <!--    location icon -->
                        <div class="ul_icon ">
                            <div class="location_a_box">
                                <a href="javascript:;" target="_self"> 
                                  <i class="icon-location"></i>
                                   <span>LOCATION</span>
                                  </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <!-- 地區 -->
                        <div class="location_li01">
                            <span>南投,台中,彰化</span>
                        </div>
                        <!--    location 店家資訊 -->
                        <div class="location_detial">
                            <div class="location_store">
                                <div class="location_store_inner">
                                    <span>純泰企業有限公司</span>
                                    <p>展示中心</p>
                                </div>
                            </div>
                            <div class="location_adress_box">
                                <div class="location_a_box">
                                    <a href="javascript:;" target="_self"> 
                                  <i class="icon-location"></i>
                                   <span>LOCATION</span>
                                  </a>
                                </div>
                                <div class="location_adress">
                                    <h6>812 高雄市仁武區鳳仁路95之32號</h6>
                                    <h5><b>T </b> 07-3717965s</h5>
                                </div>
                            </div>
                        </div>
                        <!--    location icon -->
                        <div class="ul_icon ">
                            <div class="location_a_box">
                                <a href="javascript:;" target="_self">  
                                  <i class="icon-location"></i>
                                   <span>LOCATION</span>
                                  </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <!-- 地區 -->
                        <div class="location_li01">
                            <span>南投,台中,彰化</span>
                        </div>
                        <!--    location 店家資訊 -->
                        <div class="location_detial">
                            <div class="location_store">
                                <div class="location_store_inner">
                                    <span>純泰企業有限公司</span>
                                    <p>展示中心</p>
                                </div>
                            </div>
                            <div class="location_adress_box">
                                <div class="location_a_box">
                                    <a href="javascript:;" target="_self">         
                                  <i class="icon-location"></i>
                                   <span>LOCATION</span>
                                  </a>
                                </div>
                                <div class="location_adress">
                                    <h6>812 高雄市仁武區鳳仁路95之32號</h6>
                                    <h5><b>T </b>07-3717965s</h5>
                                </div>
                            </div>
                        </div>
                        <!--    location icon -->
                        <div class="ul_icon ">
                            <div class="location_a_box">
                                <a href="javascript:;" target="_self"> 
                                  <i class="icon-location"></i>
                                   <span>LOCATION</span>
                                  </a>
                            </div>
                        </div>
                    </li>
                </ul>
                @endif
                 <!-- 電腦版的結束線 -->
                <ul class="end_line_1920">
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
           
           <input type='hidden' id='city'  value='-'>
           <input type='hidden' id='area'  value='-'>
            <div class="square_more">
                <a href="javascript:;">
                <span></span>
                </a>
            </div>
        </section>
    </main>
    @include($locale.'.footer') 

    @if(!empty($_GET['Open']))

     <a  id='OpenTheBox' class='openlocation' href="{{ ItemMaker::url('locationdetail/'.$Opendata->id.'?Open='.$Opendata->id) }}" last="{{ ItemMaker::url('location') }}" title="{{ $Opendata->connect_info }}" x='{{ $Opendata->map_x }}' y='{{ $Opendata->map_y }}'>
      </a>
    @endif
@section('script')  
    <!--主要jQuery-->

    <script src="/assets/js/vendor/map/jquery.tinyMap.min.js"></script>

    <script >
        $(document).on('ready',function(){

            if($('#OpenTheBox').length >0)
            {
                $('#OpenTheBox').click();
                
            }


            @if(!empty($_GET['lo']))
              {
                var lo ="{{$_GET['lo']}}";
                $(".location_menu li").each(function(){
                    if(lo==$(this).text())
                    {
                        console.log(lo+'--'+$(this).text());
                        $(this).click();
                        $(this).click();
                    }
                });

                $('.ok_box').click();
              }
            @endif

        });

        
                $(".location_menu").on('click',"li",function(){
                    $('.store_menu').hide();

                    var select_city=$(this).text();
                    $('.store_menu').each(function(){
                       $(this).find("ul li").eq(0).click();
                        if($(this).attr('country') == select_city)
                        {
                         
                         $(this).show();
                         $('#city').val(select_city);
                         
                        }
                    });
                     $('.dropdown').easyDropDown({
                       cutOff: 5,
                     });

            });

     $(".store_menu").on('click',"li",function(){
                    var select_city=$(this).text();
                    $('#area').val(select_city);
             });

      $(document).ready(function(){
            $('.store_menu').hide();
      });


      $('.ok_box,.square_more').on('click',function(){

        $('.con_content li').hide();

        

        if($(this).hasClass('ok_box'))
        {
            
            $('.con_content li').removeClass('toshow');

            if($('#area').val()=='-')
            {
                $('#area').val('nodata');
            }
        }

        length=$('.toshow').length;
        shownum=length+6;
        length=0;
            
            
            
            $('.con_content li').each(function(){
               
            this_city=$(this).find('div').eq(0).find('span').text();
            this_area=$(this).find('div').eq(1).find('.location_adress').text();
            s_city=$('#city').val();
            s_area=$('#area').val();
            if(s_area.length > 2)
            {
            s_area=s_area.replace('區',"");
            }
                //  if(this_city.indexOf(s_city) !=-1 && s_area =='-' && length <shownum || this_city.indexOf(s_city) !=-1 && this_area.indexOf(s_area) != -1 && length <shownum)
                // {
                //     //console.log('2>>'+this_city+"--"+this_area+"--"+length+"--"+shownum);
                //     $(this).show();
                //     $(this).addClass('toshow');
                //     length++;

                // }

                //  else if(this_area.indexOf(s_area) != -1  && length <shownum)
                // {
                  
                //     //console.log('3>>'+this_city+"--"+this_area+"--"+length+"--"+shownum);
                //     $(this).show();
                //     $(this).addClass('toshow');
                //     length++;
                // }
                //  else  if(this_city.indexOf(s_city) !=-1  && $(this).find('div').eq(1).text().indexOf(s_area) !=-1 && length <shownum || s_area=='-' && s_city=='-' && length <shownum)
                // {
                //     //console.log('1>>'+this_city+"--"+this_area+"--"+length+"--"+shownum);
                //     $(this).show();
                //     $(this).addClass('toshow');
                    
                //     length++;
                // }
                // else if(this_city==s_city  && length <shownum)
                // {
                  
                //     //console.log('4>>'+this_city+"--"+this_area+"--"+length+"--"+shownum);
                //     $(this).show();
                //     $(this).addClass('toshow');
                //     length++;
                // }

                if($(this).find('div').eq(1).text().indexOf(s_area) != -1 && length <shownum)
                {
                    $(this).show();
                    $(this).addClass('toshow');
                    
                    length++;
                }
                else if(this_city.indexOf(s_city) > -1  && length <shownum)
                {
                  //console.log(s_area);
                    if(s_area!='nodata')
                    {
                        if($(this).find('div').eq(1).text().indexOf(s_area) != -1)
                        {
                             $(this).show();
                             $(this).addClass('toshow');

                        }
                    }
                    else
                    {
                         $(this).show();
                         $(this).addClass('toshow');
                    }
                   
                    length++;
                }
               

                
            });
      });
      

      
    </script>
@stop


@stop