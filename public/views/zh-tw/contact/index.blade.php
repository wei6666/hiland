
@extends('template')

    <!--RWD Setting-->
@section('css')

@stop
@section('content')

@include($locale.'.header') 
    <main>
        <!-- 大標題 -->
        <section class="big_title contact00" id="top"> 
            <div class="banner_inner">
                <div class="contact_information">
                    
                   <span class="contact_bigtitle">{{ $ContactSet->top_title }}</span>
                   <div class="banner_note">
                        <h5>{{ $ContactSet->title }}</h5>
                        <span>{!! $ContactSet->sub_title !!}
                        </span>
                   </div>
                    <div class="contact_ad">
                        
                       <span>{!! $Setting['company_address'] !!}</span>
                       <span>{!! $Setting['company_email'] !!}</span>
                       
                    </div>
                    <div class="contact_tlt">
                        <span>{!! $Setting['company_phone'] !!}</span>
                        <span>{!! $Setting['company_fax'] !!}</span>
                    </div>
                    <div class="contact_icon">
                        
                        <i class="fa fa-facebook" aria-hidden="true" onclick="window.open('{{ $Setting['company_fb'] }}')"></i>
                        <i class="fa fa-youtube-play" aria-hidden="true" onclick="window.open('{{ $Setting['company_youtube'] }}')"></i>
                      {{--   <i class="icon-meassage" onclick="mailto:{{ $Setting['contact_emails'] }}"></i> --}}
                    </div>
                </div>
            </div>
            
        </section>
        <!-- 內容 -->
        <section class="contact01">
  
            <div class="contact_list">
            <form action="{{ ItemMaker::url('contactsubmit')}}" method='POST' id='gosubmit'>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div class="favorite_from_content">
                <ul>
                    <li>
                        <div class="favorite_name">
                            <span class="favorite_title">姓　　名</span>
                            <input type="text" name='data[name]' placeholder="黃山石">
                        </div>
                        <div class="favorite_male">
                            <span class="favorite_title">性　　別</span>
                            <input type='hidden' id='gender' value='男' name='data[gender]'>
                            <div class="sex">
                                <div class="sex_b">
                                    <i class="fa fa-stop" aria-hidden="true"></i>
                                    <span>男</span>
                                </div>
                                <div class="sex_g">
                                    <i class="fa fa-stop" aria-hidden="true"></i>
                                    <span>女</span>
                                </div>
                                
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="favorite_tele">
                            <span class="favorite_title">電　　話</span>
                            <input type="text" name='data[phone]' placeholder="0912 - 345 - 678">
                        </div>
                        <div class="favorite_email">
                            <span class="favorite_title">E - mail</span>
                            <input type="text" name='data[email]' placeholder="service@hiland.com.tw">
                        </div>
                    </li>
                    <li>
                         <div class="favorite_city_box">
                            <span class="favorite_title">地址</span>
                            <input type="text" name='data[address]' placeholder="">
                        </div>
                        @if(0)
                        <div class="favorite_city_box">
                            <div class="favorite_city ">
                                <span class="favorite_title">所在區域</span>
                                <input type='hidden'  id='city'  name='data[city]'>
                                <input type='hidden' id='area' name='data[area]'>
                                <div class="favorite_city_s">
                                    <select class="dropdown">
                                        <option value="1">選擇您的縣市</option>
                                        @foreach($country as $key => $value)
                                        <option value="2">{{$key}}</option>
                                        @endforeach
                                        <!-- <option value="2">台北</option>
                                        <option value="3">台中 彰化 南投</option>
                                        <option value="4">新竹</option> -->
                                    </select>
                                </div>
                            </div>
                            
                            <?php $co=0; ?>
                            @foreach($country as $key => $value)
                            
                            <div class="favorite_home_box " country="{{$key}}" @if(/*$co>0*/1)  @endif>
                                <div class="favorite_home">
                                    <select class="dropdown">
                                        <option value="1">鄉鎮市區</option>
                                        
                                            @foreach($value as $key1 => $value1)
                                        <option  value="2">{{$value1->title}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                            @endforeach
                            
                        </div>
                        @endif
                    </li>
                    <li>
                        <div class="favorite_need">
                            <span class="favorite_title">特殊需求</span>
                            <textarea rows="8" cols="100" name='data[message]'  placeholder="需求內容"></textarea>
                        </div>
                    </li>
                    <li>
                        <div class="favorite_code">
                            <span class="favorite_title">驗 &nbsp;  證  &nbsp; 碼</span>
                            <input type="text" name="captcha" class="requied" placeholder="請填入驗證碼">
                            <div class="f_code_box">
                                <div class="reset"><img src="/assets/img/code.png" alt=""></div>
                               <div id="changeCap">{!! Captcha::img() !!}</div>
                            </div>
                        </div>
                    </li>
                     
                    <li></li>
                </ul>
                <div class="favorite_button_box js_send">
                    <div class="half_color_box green_btn">
                        <a href="javascript:;" >
                            <div class="half_color ">
                                <span>SEND<span>
							</div>
						</a>
		            </div>
		            <div class="half_color_box on js_clear">
					    <a href="javascript:;">
							<div class="half_color ">
								<span>CANCEL</span>
                            </div>
                        </a>
                    </div>
                </div>
                </form>
            </div>
       
           
        </section>
    </main>
    @include($locale.'.footer') 
@section('script')  
    <!--主要jQuery-->
    
    <script >
        $(document).ready(function(){
            @if(!empty($_GET))
                $('#linknotalert').click();
                $('html,body').animate({ scrollTop: $('#gosubmit').offset().top}, 1000);
            @endif

            $('.favorite_home_box').hide();
                $(".favorite_city").on('click',"li",function(){
                    $('.favorite_home_box').hide();

                    var select_city=$(this).text();
                    $('.favorite_home_box').each(function(){
                       
                        if($(this).attr('country') == select_city)
                        {
                         
                         $(this).show();
                         $('#city').val(select_city);
                         
                        }
                    });

            });

                $(".favorite_home").on('click',"li",function(){
                    var select_city=$(this).text();
                    $('#area').val(select_city);
             });


            $('.js_send div').eq(0).off().on('click',function(){
                
                alltext='';
                $('.favorite_from_content input,.favorite_need textarea').each(function(e){

                    if($(this).attr('id')=="city" && $(this).val() =="")
                    {
                        alltext+=$(this).siblings('span').eq(0).text()+"縣市\n";
                    }
                    else if($(this).attr('id') =="area" && $(this).val() =="")
                    {
                        alltext+=$(this).siblings('span').eq(0).text()+"區域\n";
                    }
                    else if($(this).val() =="")
                    {
                        alltext+=$(this).siblings('span').eq(0).text()+"\n";
                    }

                });

                if(alltext=="")
                {
                    $('#gosubmit').submit();
                }
                else
                {
                    alert("請填寫\n"+alltext+"\n 欄位");
                }

                
                
            });

            $('.js_clear').off().on('click',function(){
              $(".favorite_from_content input ,.favorite_from_content textarea").val("");
                //window.location.href=$("base").attr("href")+"/favorite?cl=1";
            });

            $(".reset").click(function(){
                $("#changeCap").click();
            });

            $('.favorite_male i').on('click',function(){

                $('#gender').val($(this).siblings('span').text());
            });

               /*驗證碼點擊更換*/

            var cap = document.querySelector('div[id="changeCap"]');

            cap.addEventListener("click", function () {



                var d = new Date(),

                    img = this.childNodes[0];



                img.src = img.src.split('?')[0] + '?t=' + d.getMilliseconds();



            });
        });
    </script>
@stop


@stop