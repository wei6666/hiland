<style>
    .search_options ul li span.clicked:before {
    background: #000;



}
</style>
 <div class="lbox_inner searcharea" @if(/*!empty($color)*/1) style="background:rgba(0 , 0 , 0 , 0.3)" @endif>
 	 	<section class="search_box" style="margin-top: 3vh">
 	 	 	<div class="search_header">
 	 	 		<h5>Search</h5>
                <div class="close_search">
                    <span></span>
                </div>
                <div class="select_those  getselect_those">
                    <span class="select_btn_span">已選條件</span>
                    <ul>
                       {{--  <li>
                            <div class="select_those_inner">
                                <div class="close_x"></div>
                                <span>客廳</span>
                            </div>
                        </li>
                        <li>
                            <div class="select_those_inner">
                                <div class="close_x"></div>
                                <span>695 x 147</span>
                            </div>
                        </li>
                        <li>
                            <div class="select_those_inner">
                                <div class="close_x"></div>
                                <span>695 x 147</span>
                            </div>
                        </li>
                        <li>
                            <div class="select_those_inner">
                                <div class="close_x"></div>
                                <span>695 x 147</span>
                            </div>
                        </li>
                        <li>
                            <div class="select_those_inner">
                                <div class="close_x"></div>
                                <span>599 x 147</span>
                            </div>
                        </li> --}}
                    </ul>
                </div>
 	 	 	</div>
 	 	 	<article>
 	 	 		<div class="search_options pro_option">
                    <h5>產品型號</h5>
                    <div class="search_option_box">
                        <input type="text"  id='num' placeholder="   ex : 75983">
                        <span>(若以型號搜尋，搜尋結果將以產品型號為優先)</span>
                    </div>
                </div>
                <div class="search_options place_option">
                    <h5>適用場所</h5>
                    <div class="search_option_box">
                        <ul>
                            <?php $count=0;?>
                            @foreach($place as $value)
                            <li><span class='place' type='place' id='place_{{ $count }}'>{{ $value->place_title }}</span></li>
                            <?php $count++;?>
                            @endforeach
                           
                        </ul>
                    </div>
                </div>
                <div class="search_options size_option">
                    <h5>規格尺寸</h5>
                     <div class="search_option_box">
                        @if(count($sizesq)>0)
                        <div class="search_option_li">
                            <div class="p_icon_box p_square">
                                <span></span>
                            </div>
                            <ul>
                                <?php $count=0;?>
                                @foreach($sizesq as $value)
                                 <?php  $qq=explode(' ', $value->size);?>
                                <li><span class='size' type='size' id='sizesq_{{ $count }}'>{{  $qq[0]." ".$qq[1]." ".$qq[2] }}</span></li>
                                <?php $count++;?>
                                @endforeach
                               
                            </ul>
                        </div>
                        @endif
                        @if(count($sizere)>0)
                        <div class="search_option_li">
                            <div class="p_icon_box rectangle">
                                <span></span>
                            </div>
                             <ul>
                                <?php $count=0;?>
                              @foreach($sizere as $value) 
                                 <?php  $qq=explode(' ', $value->size);?>
                                <li><span class='size' type='size' id='sizere_{{ $count }}'>{{  $qq[0]." ".$qq[1]." ".$qq[2] }}</span></li>
                                <?php $count++;?>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        @if(count($sizeha)>0)
                        <div class="search_option_li">
                             <div class="p_icon_box Pentagon">
                                <img src="../../assets/img/Pentagon.png" alt="">
                             </div>
                             <ul>
                                <?php $count=0;?>
                                @foreach($sizeha as $value) 
                                  <?php  $qq=explode(' ', $value->size);?>
                                <li><span class='size' type='size' id='sizeha_{{ $count }}'>{{  $qq[0]." ".$qq[1]." ".$qq[2] }}</span></li>
                                <?php $count++;?>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>
                </div>
                 <div class="search_options mati_option">
                     <h5>表面材質</h5>
                     <div class="search_option_box">
                        <ul>    
                            <?php $count=0;?>                        
                            @foreach($face as $value) 
                                @if(!empty($value->face))
                                <li><span class='face'  type='face' id='face_{{ $count }}'>{{ $value->face }}</span></li>
                                <?php $count++;?>
                                @endif
                            @endforeach
                        </ul>
                     </div>
                </div>
                <div class="search_button_box">
                    <div class="half_color_box show">
                        <a href="javascript:;" class='send'>
                            <div class="half_color ">
                                 <span>送出</span>
                            </div>
                        </a>
                    </div>
                    <div class="half_color_box green_btn show">
                        <a href="javascript:;" class='clean'>
                            <div class="half_color ">
                                <span>清除全部<span>
                                </span></span>
                            </div>
                        </a>
                    </div>    
                </div>
 	 	 		
 	 	 	</article>
            <form action="{{ ItemMaker::url('searchresult')}}" method='POST'   id='submit_go'>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type='hidden' id="place"  name="place">
                <input type='hidden' id="size"  name="size">
                <input type='hidden' id="face"  name="face">
                <input type='hidden' id="forurl"  class='forurl' name="forurl">
                <input type='hidden' id="pro_num"  name="pro_num">

            </form>
 	 	  	    
 	 	</section>

 </div>
