@extends('template')

    <!--RWD Setting-->
@section('css')

@stop
@section('content')

@include($locale.'.header')
	<main>

		<!-- 大標題 -->
		<section class="big_title" id="top">
			<div class="banner_inner">
				<a href="{{ ItemMaker::url('productcategory') }}">
					BACK
				</a>

				<span>{{ $pagedata->in_title }}</span>
				<div class="banner_note">
					<h5>{!! $pagedata->in_sub_title !!}</h5>
					<span>{!! $pagedata->in_sub_title1 !!}</span>
				</div>
			</div>
		</section>

		<!-- 內容 -->
		<section class="product_detail" id="two">

			<div class="select_box">
				<!-- 電腦menu -->
				<div class="select_inner">
					<ul class="select_ul">
						<li index='1' class="on"><a href="javascript:void(0)">{{ $ProductSet->left_btn_title }}</a></li>
						<li index='2' ><a href="javascript:void(0)">{{ $ProductSet->right_btn_title }}</a></li>
					</ul>
				</div>
					<!-- rwd menu -->
				<div class="a_id_box">
					<span > {{ $ProductSet->left_btn_title }}</span><i class="fa fa-caret-down" aria-hidden="true"></i>
					<ul>
						<li><a href="javascript:void(0)">{{ $ProductSet->left_btn_title }}</a></li>
						<li><a href="javascript:void(0)">{{ $ProductSet->right_btn_title }}</a></li>
					</ul>
				</div>
			</div>

			<div class="product_detail_wall">
				<ul class="grid">
					
					@foreach($productdata as $value)					
					<li pro_id='{{ $value->id }}' class="grid-item grid-item-width{{ $value->out_img_type }}">
						<img src="{{ $value->out_image }}" alt="">
						<a class="openProduct detail_lbox_btn" href="{{ ItemMaker::url('productdetail/'.$cateid.'?Open='.$value->id) }}" last="{{ ItemMaker::url('categorydetail/'.$cateid) }}">
							<span></span>
							<h3>{{ $value->pro_num }}</h3>
							<p>{{ $value->size }}</p>
							<div class="favor">
								<i class="@if($value->is_favorite=='no')fa fa-heart-o @else  fa fa-heart @endif" aria-hidden="true" pro_id='{{ $value->id }}'></i>
							</div>
						</a>
					</li>
					@endforeach
				
				</ul>
			</div>

			<!-- table -->
			<section class="product_table">
				<ul class="product_table_ul">
					<li>
						<div class="tltle">
							<div>
								{!! $ProductSet->features_title !!}
							</div>
						</div>
						<div class="narrative">
							<ul>
								<?php 
								$thedata=explode("\r\n",$pagedata->features);

								?>
								@foreach($thedata as $value)
									@if(strpos($value,"green"))
										<?php  $value=str_replace("green", "", $value);?>
										<li class="green_word">
											<p>{!! $value !!}</p>
										</li> 
									@elseif(strpos($value,"nodash"))
									<?php  $value=str_replace("nodash", "", $value);?>
										<li class="dash">
											<span>{!! $value !!}</span>
										</li>
									@else
										<li class="dash">
											<p>{!! $value !!}</p>
										</li>
									@endif
								@endforeach								
							</ul>
						</div>
					</li>
					@if(0){{-- 客戶說要拿掉 --}}
					<li>
						<div class="tltle">
							<div>
								{!! $ProductSet->crafts_title !!}
							</div>
						</div>
						<div class="narrative">
							<ul>
								<?php 
								$thedata=explode("\r\n",$pagedata->crafts);

								?>
								@foreach($thedata as $value)
									@if(strpos($value,"green"))
										<?php  $value=str_replace("green", "", $value);?>
										<li class="green_word">
											<p>{!! $value !!}</p>
										</li> 
									@elseif(strpos($value,"nodash"))
									<?php  $value=str_replace("nodash", "", $value);?>
										<li class="dash">
											<span>{!! $value !!}</span>
										</li>
									@else
										<li class="dash">
											<p>{!! $value !!}</p>
										</li>
									@endif
								@endforeach	
							</ul>
						</div>
					</li>
					@endif
					<li>
						<div class="tltle">
							<div>
								{!! $ProductSet->place_title !!}
							</div>
						</div>
						<div class="narrative">
							<ul>
								<li>
									<p>{!! $pagedata->crafts !!}</p>
								</li>
							</ul>
						</div>
					</li>
					<li>
						<div class="tltle">
							<div>
								{!! $ProductSet->download_title !!}
							</div>
						</div>
						<div class="narrative">
							<ul>
								<li class="download green_word">
									<a href="{{ $pagedata->download }}" target="_blank">
										<img src="/assets/img/files.png" alt="">
										<p>{!! $pagedata->download_title !!}</p>
									</a>
								</li>
							</ul>
						</div>
					</li>
				</ul>
			</section>

			<!-- 系列規格表 -->
			<section class="compare_list" style='display: none;'>
				<ul class="compare_list_frame">
					<li class="compare_list_head">
						<p class="w_l">
							{!! $ProductSet->table_1 !!}
						</p>
						<p class="w_m">
							{!! $ProductSet->table_2 !!}
						</p>
						<p class="w_m">
							{!! $ProductSet->table_3 !!}
						</p>
						<p class="w_m">
							{!! $ProductSet->table_4 !!}
						</p>
						<p class="w_m">
							{!! $ProductSet->table_5 !!}
						</p>
					</li>
					<li class="compare_list_content">
						@foreach($productlist as $value)
						<ul>
							<li class="w_l">
								<div>
									<img src="{{ $value->out_image }}" alt="">
								</div>
								<p class="head">
									{!! $ProductSet->table_1 !!}
								</p>
								<p class="data">
									<span>{{ $value->spec }}</span>
								</p>
							</li>
							<li class="w_m">
								<p class="head">
									{!! $ProductSet->table_2 !!}
								</p>
								<p class="data">
									<span>{{ $value->thickness }}</span>
								</p>
							</li>
							<li class="w_m">
								<p class="head">
									{!! $ProductSet->table_3 !!}
								</p>
								<p class="data">
									<span>{{ $value->pack_num }}</span>
								</p>
							</li>
							<li class="w_m">
								<p class="head">
									{!! $ProductSet->table_4 !!}
								</p>
								<p class="data">
									<span>{{ $value->count_num }}</span>
								</p>
							</li>
							<li class="w_m">
								<p class="head">
									{!! $ProductSet->table_5 !!}
								</p>
								<p class="data">
									<span>{{ $value->work_num }}</span>
								</p>
							</li>
						</ul>	
						@endforeach					
					</li>					
				</ul>
				
			</section>

			<!-- 返回鍵 -->
			<div class="half_color_box show">
				<a href="{{ ItemMaker::url('productcategory') }}">
					<div class="half_color ">
						<span>BACK</span>
					</div>
				</a>
			</div>

		</section>
        
	</main>
	<input type='hidden' id='favorite_data'>
	@if(!empty($_GET['Open']))
	<a id='OpenTheBox' class="openProduct detail_lbox_btn" href="{{ ItemMaker::url('productdetail/'.$cateid.'?Open='.$Opendata->id) }}" last="{{ ItemMaker::url('categorydetail/'.$cateid) }}">
      </a>
    @endif
@include($locale.'.footer') 
	
@section('script')  
    <!--主要jQuery-->
    
   <script >
   	
   $('.select_ul li').on('click',function(){
   	
   	if($(this).attr('index')==1)
   	{
   		$('.product_detail_wall').show();
   		$('.product_table').show();
   		$('.compare_list').hide();
   	}
   	else
   	{
   		
   		$('.product_detail_wall').hide();
   		$('.product_table').hide();
   		$('.compare_list').show();
   	}

   });

    $(document).on('ready',function(){

        if($('#OpenTheBox').length >0)
        {
        	
            $('#OpenTheBox').click();
            
        }

        //$('body').attr('id','news');

     });

    </script>
    <script src="/assets/js/vendor/masonry/imagesloaded.pkgd.min.js"></script>
	<script src="/assets/js/vendor/masonry/masonry.pkgd.js"></script>
	<script src="/assets/js/product.js"></script>
	
@stop


@stop