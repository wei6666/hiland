<?php  $count=0;?>
				@foreach($allcate as $value)
					@if($count ==0)
					<ul class="out_1 out_frame">	
					@endif

					@if($count ==3)
					</ul>
					@endif

					@if($count ==3)
					<ul class="out_2 out_frame">	
					@endif
					
					<li class="inner @if(!empty($value->is_new) && $value->is_new!= 0) new @endif " {{ $count }}>
						<a @if($count  ==0)	style="transform: translate(0,0);" 
						@elseif($count %6 ==0)	style="transform: translate(0,70%);"
						@elseif($count %5 ==0)	style="transform: translate(0,10%);" 
						@elseif($count %4 ==0)	style="transform: translate(0,30%);"
						@elseif($count %3 ==0)	style="transform: translate(0,0);" 
						@elseif($count %2 ==0)	style="transform: translate(0,20%);" 
						@elseif($count %1 ==0)	style="transform: translate(0,50%);"
						@endif href="{{ ItemMaker::url('categorydetail/'.$value->id) }}">
							<div class="content">
								<div class="content_pic">
									<picture>
										<img src="{{ $value->out_image }}" alt="">
									</picture>
								</div>
								<div class="content_name">
									<p class="e_name">{{ $value->sub_title }}</p>
									<p class="m_name">{{ $value->title }}</p>
								</div>
							</div>
						</a>
					</li>

				

					

					@if($count ==6)
					</ul >
					<?php  $count=0; ?>
					@else
					<?php $count++;?>
					@endif
				
				@endforeach
				@if($count <3 || $count  > 3 && $count <= 7)
					</ul>
				@endif