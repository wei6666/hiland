@if(!empty($productlist))
@foreach($productlist as $value)	
					<li pro_id='{{ $value->id }}' class="grid-item grid-item-width{{ $value->out_img_type }}">
						<img src="{{ $value->out_image }}" alt="">
						<a class="openProduct detail_lbox_btn" href="{{ ItemMaker::url('productdetail/'.$cateid.'?Open='.$value->id) }}" last="{{ ItemMaker::url('categorydetail/'.$cateid) }}">
							<span></span>
							<h3>{{ $value->pro_num }}</h3>
							<p>{{ $value->size }}</p>
							<div class="favor">
								<i class="@if($value->is_favorite=='no')fa fa-heart-o @else  fa fa-heart @endif" aria-hidden="true" pro_id='{{ $value->id }}'></i>
							</div>
						</a>
					</li>
@endforeach
@endif