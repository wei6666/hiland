<div class="lbox_inner ">
    <section class="product_detail_bigbox">
        <div class="product_detail_header">
            <div class="close">
                <span class="x_close"></span>
                <span class="green"></span>
                <span class="yellow"></span>
                <img src="../../assets/img/logo.svg" alt="">
            </div>
        </div>
        <div class="product_detail_content">
            <section class="product_detail_carousel">
                <ul class="img">

                    @foreach($productphoto as $value)
                    <li>
                        <img class='unload' src="{{ $value->image }}" alt="">
                    </li>
                    @endforeach
                   
                </ul>
                <div class="style_dotnumber">
                </div>
            </section>
            <section class="product_detail_borad">
                <div class="product_detail_numbering">
                    <h2>{{ $pagedata->pro_num }}</h2>
                    <p>{{ $pagedata->size }}</p>
                </div>
                <div class="product_detail_img">
                    <p>{{ $pagedata->face }}</p>
                    <img src="{{ $pagedata->out_image }}" alt="">
                </div>
                <div class="product_detail_specifi">
                    <h2>{{ $ProductSet->in_table_title }}</h2>
                    <div class="specifi_table">
                        <p>{{ $ProductSet->in_table_sub_title }}</p>
                        <div class="favor">
                            <i class="fa @if($pagedata->is_favorite=='no') fa-heart-o @else fa-heart @endif" aria-hidden="true" pro_id='{{ $pagedata->id }}'></i>
                        </div>
                    </div>
                    <ul>
                        <li>
                            <p class="head">
                                {!! $ProductSet->table_1 !!}
                            </p>
                            <p class="data">
                                <span>{{ $pagedata->spec }}</span>
                            </p>
                        </li>
                        <li>
                            <p class="head">
                               {!! $ProductSet->table_2 !!}
                            </p>
                            <p class="data">
                                <span>{{ $pagedata->thickness }}</span>
                            </p>
                        </li>
                        <li>
                            <p class="head">
                               {!! $ProductSet->table_3 !!}
                            </p>
                            <p class="data">
                                <span>{{ $pagedata->pack_num }}</span>
                            </p>
                        </li>
                        <li>
                            <p class="head">
                               {!! $ProductSet->table_4 !!}
                            </p>
                            <p class="data">
                                <span>{{ $pagedata->count_num }}</span>
                            </p>
                        </li>
                        <li>
                            <p class="head">
                               {!! $ProductSet->table_5 !!}
                            </p>
                            <p class="data">
                                <span>{{ $pagedata->work_num }}</span>
                            </p>
                        </li>
                    </ul>
                </div> 
            </section>
        </div>
        <div class="half_color_box">
            <a href="javascript:void(0)">
                <div class="half_color ">
                    <h5>back</h5>
                </div>
            </a>
        </div>
    </section>
</div>