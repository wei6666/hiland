
@extends('template')

    <!--RWD Setting-->
@section('css')

@stop
@section('content')

@include($locale.'.header') 
    <main>

        <section class="title_1440box addshow" id="top">
            <div class="title_inner">
                <span>Find Products</span>
                <div class="banner_note">
                    <h5>篩選結果</h5>
                    <span>提供多樣豐富，美觀且耐用的瓷磚組合</span>
                </div>
            </div>
            <a href="javascript:;"  onclick="window.history.back();" class="back_line">back</a>
        </section>
        <section class="search_result">
            <div class="select_btn">
                <div class="select_btn_inner black">
                    <i class="icon-search"></i>
                    <span class="select_btn_span">篩選條件</span>
                    <i class="fa fa-chevron-down" aria-hidden="true"></i>
                </div>
               
            </div>
            <div class="select_those" allselect="@if(!empty($allcondition)){{ str_replace('"',"", $allcondition) }} @endif">
                <span class="select_btn_span">已選條件</span>
                <ul>
                    @if(!empty($allcondition))
                    <?php $datal=explode(",",$allcondition);?>
                        @foreach($datal as $value)
                            @if(!empty($value))
                                <li>
                                    <div class="select_those_inner">
                                       {{--  <div class="close_x"></div> --}}
                                        <span>{{ $value }}</span>
                                    </div>
                                   
                                </li>
                            @endif
                        @endforeach
                    @endif
                    

                    
                </ul>
            </div>
            <div class="select_pic_box">
                <ul>
                    @if(count($getproquery) >0)
                        @foreach($getproquery as $value)
                        <li>
                            <a href="{{ ItemMaker::url('categorydetail/'.$value->category_id) }}" class="p_more_series">{{ $categoryarr[$value->category_id] }}</a>
                            <a class="openProduct detail_lbox_btn" href="{{ ItemMaker::url('productdetail/'.$value->category_id.'?Open='.$value->id) }}" last="{{ "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}">
                            <div class="item_inner">
                                <picture>
                                    <img src="{{ $value->out_image }}" alt="">
                                </picture>
                                <div class="ball_icon">
                                    <span></span>
                                    <h3>{{ $value->pro_num }}</h3>
                                    <p>{{ $value->size }}</p>
                                </div>
                            </div>
                            </a>
                        </li>
                        @endforeach 
                    
                    @else
                        <div class="item_inner" style="border: solid 5px; width:100%; text-align:center; " >
                            <marquee scrollAmount='10' ><h1  style="padding: 50px; text-align:center; ">NONE PRODUCT FOUND</h1></marquee>
                        </div>
                    @endif
                </ul>
            </div>
        </section>
         
        @if(!empty($forurl))
        <input type='hidden' id="forurl" class="forurl"  name="forurl" value={{  $forurl }}>
        @endif        
        
        <!-- 內容 -->
    </main>
    @include($locale.'.footer') 
    
@section('script')  
    @if(!empty($forurl))
        <script >          
            
            window.history.pushState("", "", $("#forurl").val());
            
        </script>
    @endif
  
@stop


@stop