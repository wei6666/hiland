
@extends('template')

    <!--RWD Setting-->
@section('css')

@stop
@section('content')

@include($locale.'.header') 
	<main id='product'>

		<!-- 大標題 -->
		<section class="big_title" id="top">
			<div class="banner_inner">
				<span>{{ $ProductSet->title }}</span>
				<div class="banner_note">
					{!! $ProductSet->sub_title !!}
					
				</div>
			</div>
			<a href="#two" class="scroll_line_box">
				<span>s</span>
				<span>c</span>
				<span>r</span>
				<span>o</span>
				<span>l</span>
				<span>l</span>
				<div class="down_page">
					<span></span>
					<div class="angle"></div>
				</div>
			</a>
		</section>

		<!-- 內容 -->
		<section class="product_wall" id="two">
			<div class="select_box">
				<!-- 電腦menu -->
				<div class="select_inner">
					<span class="select_style">select：</span>
					<ul class="select_ul product_select_ul">
						<li class='on'><a  href='javascript:;' link="{{ ItemMaker::url('productcategoryajax/all') }}">ALL</a></li>

						@foreach($allbig as $value)
						
						@if(strcasecmp($value->title, 'new') ==0 )
						<li class='cl{{ $value->id }}'><a href='javascript:;' link="{{ ItemMaker::url('productcategoryajax/news') }}">{{ $value->title }}</a></li>
						@else
						<li class='cl{{ $value->id }}'><a href='javascript:;' link="{{ ItemMaker::url('productcategoryajax/'.$value->id) }}">{{ $value->title }}</a></li>
						@endif

						@endforeach
						
					</ul>
				</div>
					<!-- rwd menu -->
				<div class="a_id_box">
					<span >ALL</span><i class="fa fa-caret-down" aria-hidden="true"></i>
					<ul>
						<li><a href="{{ ItemMaker::url('productajax/all') }}">ALL</a></li>
						@foreach($allbig as $value)
						<li><a href="{{ ItemMaker::url('productajax/'.$value->id) }}">{{ $value->title }}</a></li>
						@endforeach
					</ul>
				</div>
				<div class="select_btn product_option">
                <div class="select_btn_inner">
                    <i class="icon-search"></i>
                    <span class="select_btn_span">篩選條件</span>
                    <i class="fa fa-chevron-down" aria-hidden="true"></i>
                </div>
            </div>
			</div>
			<div class="wall_frame">
				<?php  $count=0;?>
				@foreach($allcate as $value)
					@if($count ==0)
					<ul class="out_1 out_frame">	
					@endif

					@if($count ==3)
					</ul>
					@endif

					@if($count ==3)
					<ul class="out_2 out_frame">	
					@endif
					
					<li class="inner @if(!empty($value->is_new) && $value->is_new!= 0) new @endif" {{ $count }}>
						
						<a @if($count  ==0)	style="transform: translate(0,0);" 
						@elseif($count %6 ==0)	style="transform: translate(0,70%);"
						@elseif($count %5 ==0)	style="transform: translate(0,10%);" 
						@elseif($count %4 ==0)	style="transform: translate(0,30%);"
						@elseif($count %3 ==0)	style="transform: translate(0,0);" 
						@elseif($count %2 ==0)	style="transform: translate(0,20%);" 
						@elseif($count %1 ==0)	style="transform: translate(0,50%);"
						@endif href="{{ ItemMaker::url('categorydetail/'.$value->id) }}">
							<div class="content">
								<div class="content_pic">
									<picture>
										<img src="{{ $value->out_image }}" alt="">
									</picture>
								</div>
								<div class="content_name">
									<p class="e_name">{{ $value->sub_title }}</p>
									<p class="m_name">{{ $value->title }}</p>
								</div>
							</div>
						</a>
					</li>

				

					

					@if($count ==6)
					</ul >
					<?php  $count=0; ?>
					@else
					<?php $count++;?>
					@endif
				
				@endforeach
				@if($count <3 || $count  > 3 && $count <= 7)
					</ul>
				@endif


			</div>
		</section>
        
	</main>
	@include($locale.'.footer') 
	 @if(!empty($_GET['Open']))

	 <a  id='OpenTheBox' class='openNews' href="{{ ItemMaker::url('newsdetail/'.$Opendata->id.'?Open='.$Opendata->id) }}" last="{{ ItemMaker::url('news') }}">

      </a>
    @endif
@section('script')  
    <!--主要jQuery-->
    
    <script >
	@if(!empty($_GET['cl']))
	{
		$(document).ready(function(){
			//setTimeout(function(){
			$('.cl{{$_GET['cl']}}').click();
			$('.cl{{$_GET['cl']}} a').click();
			//alert('cl');
			//},5000);
		});
	}
	@endif

    </script>
@stop


@stop