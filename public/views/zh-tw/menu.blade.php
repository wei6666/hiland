<div>
    <nav class="nav_menu" style="position: absolute;">
        <div class="openmenu_logo">
          <img src="/assets/img/logo.svg" alt="">
          <img src="/assets/img/open_menu.svg" alt="">
        </div>
        <div class="openmenu_menu">
          <div class="right_menu_inner">
              <ul>

                <li>
                    <a href="{{ ItemMaker::url('productcategory') }}">
                      <div class="e_menu">Collection</div>
                      <div class="c_menu">產品系列</div>
                    </a>
                </li>
                <li>
                    <a href="{{ ItemMaker::url('style') }}">
                      <div class="e_menu">Huangshan Style</div>
                      <div class="c_menu">黃山石風格</div>
                    </a>
                </li>
                <li>
                    <a href="{{ ItemMaker::url('faq') }}">
                      <div class="e_menu">FAQ</div>
                      <div class="c_menu">常見問題</div>
                    </a>
                </li>
                <li>
                    <a href="{{ ItemMaker::url('location') }}">
                      <div class="e_menu">Where To Buy</div>
                      <div class="c_menu">經銷據點</div>
                    </a>
                </li>
                <li>
                    <a href="{{ ItemMaker::url('news') }}">
                      <div class="e_menu">News & Activity</div>
                      <div class="c_menu">新聞與活動</div>
                    </a>
                </li>
                <li>
                    <a href="{{ ItemMaker::url('favorite') }}">
                      <div class="e_menu">My Wishlist</div>
                      <div class="c_menu">我的最愛</div>
                    </a>
                </li>
                <li>
                    <a href="{{ ItemMaker::url('about') }}">
                      <div class="e_menu">Brand Story</div>
                      <div class="c_menu">品牌故事</div>
                    </a>
                </li>
                <li>
                    <a href="{{ ItemMaker::url('contact') }}">
                      <div class="e_menu">Contact Us</div>
                      <div class="c_menu">聯絡我們</div>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" id='test' class="privacy_inter">
                      <div class="e_menu">Legal Policy</div>
                      <div class="c_menu">隱私權政策</div>
                    </a>
                </li>
              </ul>
              
          </div>
          <div class="open_menu_icon">
            <a href="@if(!empty($Setting['company_fb'])) {{ $Setting['company_fb'] }} @else javascript:; @endif" target="_blank" target="_blank">
              <i class="fa fa-facebook" aria-hidden="true"></i>
            </a>
            <a href="@if(!empty($Setting['company_youtube'])) {{ $Setting['company_fb'] }} @else javascript:; @endif" target="_blank" target="_blank">
              <i class="fa fa-youtube-play" aria-hidden="true"></i>  
            </a>
            <a href="{{ ItemMaker::url('contact') }}" target="_blank">
              <i class="icon-meassage"></i> 
            </a>
          </div>
          <div class="openmenu_close outmenu">
            <span></span>
          </div>
        </div>
        <div class="openmenu_slick">
          <!--  圖輪播寬640*340 -->
            <ul style="width:33.3vw">

              @foreach($Menutitle as $value)
              <li class='unload' style="width:33.3vw">
                <a href="{{ $value->link }}">
                    <img src="{{ $value->image }}"  class='menuimg' alt="">
                    <div class="menu_slick_title">
                      <h5>{{ $value->title }}</h5>
                      <p>{{ $value->sub_title }}</p>
                    </div>
                </a>
              </li>
              @endforeach
            </ul>
        </div>
    </nav>
</div>