
@extends('template')

    <!--RWD Setting-->
@section('css')

@stop
@section('content')

@include($locale.'.header') 
	<main >
	   <!-- 大標題 -->
		<section class="big_title" id="top">
			<div class="banner_inner">
				<span>FAQ</span>
				<div class="banner_note">
					<h5>常見問題</h5>
				</div>
			</div>
			<a href="ajvascript:;" class="scroll_line_box">
				<span>s</span>
				<span>c</span>
				<span>r</span>
				<span>o</span>
				<span>l</span>
				<span>l</span>
				<div class="down_page">
					<span></span>
					<div class="angle"></div>
				</div>
			</a>
		</section>
		<!-- 內容 -->
		<section class="faq01">
			<div class="select_box">
			    <!-- 電腦menu -->
			   <div class="select_inner">
					<span class="select_style">{{ $FaqSet->cate_title }}</span>
					<ul class="select_ul">
						<li cate='ALL' class="on"><a href="javascript:;" >ALL</a></li>
						@foreach($FaqCategory as $value)
						<li cate='Q{{ $value->id }}'><a href="javascript:;">{{ $value->title }}</a></li>
						@endforeach
					</ul>
			  </div>
			     <!-- rwd menu -->
			   <div class="a_id_box">
			        <span class="span_space">ALL</span><i class="fa fa-caret-down" aria-hidden="true"></i>
			   	    <ul>
			   	    	<li><a href="javascript:;">ALL</a></li>
						@foreach($FaqCategory as $value)
						<li class='cate{{ $value->id }}'><a href="javascript:;">{{ $value->title }}</a></li>
						@endforeach
			   	    </ul>
			    </div>
			</div>
			<div class="faq_content">
				<ul>
					<?php  $co=1;?>
					@foreach($Faq as $value)
					 <li class="allques" cate='Q{{ $value->category_id }}'>
					    <div class="faq_q">
							<span>@if($co <10) O @endif {{ $co }} -</span>
							<?php $co++;?>
							<P>{!! $value->question !!}</P>
							<div class="x_icon">
							    <span></span>
						    </div>
						</div>
						<div class="faq_answer">
						    <P>{!! $value->answar !!}</P>
						</div>
					</li>
					@endforeach	
					 <li class="allques nodata" cate='nodata' style="display: none;">
					    <div class="faq_q" style='text-align: center;'>
						<p style='width: 100%;'>NO Data Found</p>
						   
						</div>
					</li>
				</ul>
			</div>
		</section>
		
	</main>
	@include($locale.'.footer') 
@section('script')  
    <!--主要jQuery-->
    <script >
    	$('.select_box li').on('click',function(){
    		var cate=$(this).attr('cate');
    		$('.allques').hide();
    		var co=1;
    		$('.allques').each(function(){
    			
    			if($(this).attr('cate')==cate || cate=='ALL' && $(this).attr('cate')!="nodata")
    			{
    				$(this).show();
    				var num='';
    				if(num<10)
    				num="O ";
	    			num+=co;
    				num+=' -';
    				$(this).find('span').eq(0).text(num);
    				co++;
    			}
    		});
    		if(co ==1)
    			$('.nodata').show();
    	});
    </script>

    <script >
   
    </script>
@stop


@stop