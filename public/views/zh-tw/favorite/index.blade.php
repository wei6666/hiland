
@extends('template')

    <!--RWD Setting-->
@section('css')

@stop
@section('content')

@include($locale.'.header') 
	<main >
	   <!-- 大標題 -->
		<section class="big_title" id="top">
			<div class="banner_inner">
				<span>{!! $Home->title !!}</span>
				<div class="banner_note">
					{!! $Home->sub_title !!}
				</div>
			</div> 
			<a href="javascript:;" class="scroll_line_box">
				<span>s</span>
				<span>c</span>
				<span>r</span>
				<span>o</span>
				<span>l</span>
				<span>l</span>
				<div class="down_page">
					<span></span>
					<div class="angle"></div>
				</div>
			</a>
		</section>
		<!-- 內容 -->
		<section class="favorite01">
			<div class="favorite_total">
				<span>
					<?php
					 if(count($productdata)>0)
					 	{ 
					 		echo "Total(".count($productdata).")"; 
					 	}



					?></span> 
			</div>
			<ul class="favorite_list" id='toprint'>
			     <!-- 圖統一 180*180 -->
			     @if(count($productdata) >0)
			     @foreach($productdata as $value)
				<li>
					<div class="favorite_img">
						  <a class="openProduct detail_lbox_btn" href="{{ ItemMaker::url('productdetail/'.$value->category_id.'?Open='.$value->id) }}" last="{{ ItemMaker::url('favorite') }}">
						  	<img src="{{ $value->out_image }}" alt="">
						  </a>


					   
					</div>
					<div class="favorite_p_number">
					    <a class="openProduct detail_lbox_btn" href="{{ ItemMaker::url('productdetail/'.$value->category_id.'?Open='.$value->id) }}" last="{{ ItemMaker::url('favorite') }}">
						 <h5>{{ $value->pro_num }}</h5>
						</a>
					</div>
					<div  class="favorite_p_detail">
						<p>{{ $value->size }}</p>
						<h5>{{ $pcate[$value->category_id] }}</h5>
					</div>
					<div class="favorite_p_delete" pro_num='{{ $value->id }}'>
						<i class="icon-delet"></i>
					</div>

				</li>
				@endforeach
				@else

				<div class="favorite_p_number" style='width: 100%; border="1px;" '>
					  
						 <h5 style="
						    border: 5px solid ;
						    padding: 20px;
						">No Data Found</h5>
												
					</div>
				 
				@endif
				
			</ul>
			<!-- 按鈕 -->
			@if(count($productdata) > 0)
			<div class="favorite_button_box">
				<div class="half_color_box">
				    <a href="{{ ItemMaker::url('favorite?toprint=1') }}" target="_blank">
						<div class="half_color " id='download'>
							<span>下載清單</span>
						</div>
					</a>
	            </div>
	           {{--  <div class="half_color_box green_btn">
				    <a href="javascript:;" id='link'>
						<div class="half_color " >
							<span>清單清結<span>
						</div>
					</a>
	            </div> --}}
	            <div class="half_color_box contact_list" id='contact'>
				    <a href="javascript:;" id='linknotalert' class='notalert'>
						<div class="half_color ">
							<span>與昌達洽詢</span>
						</div>
					</a>
	            </div>
	           
			</div>
			@endif
			<!-- 分隔線 -->
			<div class="favorite_line">
			</div>

			<!-- /*需求單*/ -->
			<div class="favorite_from_title">
				<span>Wishlist Quotation</span>
				<p>清單洽詢表</p>
			</div>
			<form action="{{ ItemMaker::url('contactsubmit')}}" method='POST' id='gosubmit'>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div class="favorite_from_content">
                <ul>
                    <li>
                        <div class="favorite_name">
                            <span class="favorite_title">姓　　名</span>
                            <input type="text" name='data[name]' placeholder="黃山石">
                        </div>
                        <div class="favorite_male">
                            <span class="favorite_title">性　　別</span>
                            <input type='hidden' id='gender' value='男' name='data[gender]'>
                            <div class="sex">
                                <div class="sex_b">
                                    <i class="fa fa-stop" aria-hidden="true"></i>
                                    <span>男</span>
                                </div>
                                <div class="sex_g">
                                    <i class="fa fa-stop" aria-hidden="true"></i>
                                    <span>女</span>
                                </div>
                                
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="favorite_tele">
                            <span class="favorite_title">電　　話</span>
                            <input type="text" name='data[phone]' placeholder="0912 - 345 - 678">
                        </div>
                        <div class="favorite_email">
                            <span class="favorite_title">E - mail</span>
                            <input type="text" name='data[email]' placeholder="service@hiland.com.tw">
                        </div>
                    </li>
                    <li>
                         <div class="favorite_city_box">
                            <span class="favorite_title">地址</span>
                            <input type="text" name='data[address]' placeholder="">
                        </div>
                        @if(0)
                        <div class="favorite_city_box">
                            <div class="favorite_city ">
                                <span class="favorite_title">所在區域</span>
                                <input type='hidden'  id='city'  name='data[city]'>
                                <input type='hidden' id='area' name='data[area]'>
                                <div class="favorite_city_s">
                                    <select class="dropdown">
                                        <option value="1">選擇您的縣市</option>
                                        @foreach($country as $key => $value)
                                        <option value="2">{{$key}}</option>
                                        @endforeach
                                        <!-- <option value="2">台北</option>
                                        <option value="3">台中 彰化 南投</option>
                                        <option value="4">新竹</option> -->
                                    </select>
                                </div>
                            </div>
                            
                            <?php $co=0; ?>
                            @foreach($country as $key => $value)
                            
                            <div class="favorite_home_box " country="{{$key}}" @if(/*$co>0*/1)  @endif>
                                <div class="favorite_home">
                                    <select class="dropdown">
                                        <option value="1">鄉鎮市區</option>
                                        
                                            @foreach($value as $key1 => $value1)
                                        <option  value="2">{{$value1->title}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                            @endforeach
                            
                        </div>
                        @endif
                    </li>
                    <li>
                        <div class="favorite_need">
                            <span class="favorite_title">特殊需求</span>
                            <textarea rows="8" cols="100" name='data[message]'  placeholder="需求內容"></textarea>
                        </div>
                    </li>
                    <li>
                        <div class="favorite_code">
                            <span class="favorite_title">驗 &nbsp;  證  &nbsp; 碼</span>
                            <input type="text" name="captcha" class="requied" placeholder="點擊圖片可更換驗證碼">
                            <div class="f_code_box">
                                <div class="reset"><img src="/assets/img/code.png" alt=""></div>
                               <div id="changeCap">{!! Captcha::img() !!}</div>
                            </div>
                        </div>
                    </li>
                     
                    <li></li>
                </ul>
                <div class="favorite_button_box js_send">
                    <div class="half_color_box green_btn">
                        <a href="javascript:;" >
                            <div class="half_color ">
                                <span>SEND<span>
							</div>
						</a>
		            </div>
		            <div class="half_color_box on js_clear">
					    <a href="javascript:;">
							<div class="half_color ">
								<span>CANCEL</span>
                            </div>
                        </a>
                    </div>
                </div>
                <input type="hidden" name="data[is_favorite]" value='是'>
                <input type="hidden" name="data[link]" id='thelink'>
                </form>
		</section>
	</main>
	@include($locale.'.footer') 
@section('script')  
    <!--主要jQuery-->
    <script >
    	@if(!empty($_GET['toprint']))
    	alert('請將配置調整成縱向,並將邊界設定成無')
    	$('header').hide();
    	$('.big_title').hide();
    	$('.favorite_button_box').hide();
    	$('.footer_top').hide();
    	$('.footer_bottom_box').hide();
        $('.favorite_from_title').hide();
        $('form').hide();
        $(".favorite_list li").show();
        $(".favorite_list li").css('opcity',1);
    	window.print();
    	@endif

    	$(document).ready(function(){
            @if(!empty($_GET))
                $('#linknotalert').click();
                $('html,body').animate({ scrollTop: $('#gosubmit').offset().top}, 1000);
            @endif

            $('.favorite_home_box').hide();
                $(".favorite_city").on('click',"li",function(){
                    $('.favorite_home_box').hide();

                    var select_city=$(this).text();
                    $('.favorite_home_box').each(function(){
                       
                        if($(this).attr('country') == select_city)
                        {
                         
                         $(this).show();
                         $('#city').val(select_city);
                         
                        }
                    });

            });

                $(".favorite_home").on('click',"li",function(){
                    var select_city=$(this).text();
                    $('#area').val(select_city);
             });


            $('.js_send div').eq(0).off().on('click',function(){
                alltext='';
                
                $('.favorite_from_content ul input,.favorite_need ul textarea').each(function(e){

                    if($(this).attr('id')=="city" && $(this).val() =="")
                    {
                        alltext+=$(this).siblings('span').eq(0).text()+"縣市\n";
                    }
                    else if($(this).attr('id') =="area" && $(this).val() =="")
                    {
                        alltext+=$(this).siblings('span').eq(0).text()+"區域\n";
                    }
                    else if($(this).val() =="")
                    {
                        
                        alltext+=$(this).siblings('span').eq(0).text()+"\n";
                    }


                });

                if(alltext=="")
                {
                    $('#gosubmit').submit();
                }
                else
                {
                    alert("請填寫\n"+alltext+"\n 欄位");
                }

                
                
            });

            $('.favorite_male i').on('click',function(){

                $('#gender').val($(this).siblings('span').text());
            });

               /*驗證碼點擊更換*/

            var cap = document.querySelector('div[id="changeCap"]');

            cap.addEventListener("click", function () {



                var d = new Date(),

                    img = this.childNodes[0];



                img.src = img.src.split('?')[0] + '?t=' + d.getMilliseconds();



            });
        });

         $('.js_clear').off().on('click',function(){
          $(".favorite_from_content input ,.favorite_from_content textarea").val("");
            //window.location.href=$("base").attr("href")+"/favorite?cl=1";
        });

        $(".reset").click(function(){
            $("#changeCap").click();
        });


    </script>
@stop


@stop