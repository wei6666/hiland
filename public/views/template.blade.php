<!DOCTYPE html>

<html lang="{{$locateLang}}" class="no-js">
<head>
    <base href="{{ItemMaker::url('/')}}">
    <?php $li=str_replace($locale ,"",ItemMaker::url('')); ?>
    <link rel="shortcut icon" type="image/x-icon" href="{{$li.'assets/img/favicon/favicon.ico' }}">
    <link rel="apple-touch-icon" href="{{ $li.'assets/img/favicon/favicon.png' }}" sizes="133*133">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

   @if( !empty($seo['web_title']) )
        <title>{{ $seo['web_title'] }}</title>
        <meta property="og:title" content="{{ $seo['web_title'].$seo['meta_description'] }}">
        <meta name="keywords" content="{{ $seo['meta_keyword'] }}">
        <meta name="description" content="{{ $seo['meta_description'] }}">
    @elseif(!empty($og_data['web_title']))
    <meta property="og:url" content="{{ "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $og_data['web_title'] }}" />
    <meta property="og:description" content="{{ $og_data['meta_description'] }}" />
    <meta property="og:image" content="{{ "http://$_SERVER[HTTP_HOST]".$og_data['og_image'] }}" />
    <title>{{ $seo['web_title'] }}</title>
    <meta property="og:title" content="{{ $seo['web_title'].$seo['meta_description'] }}">
    <meta name="description" content="{{ $seo['meta_description'] }}">
        
    @else
        <title>{{ $globalSeo['web_title'] }}</title>
        <meta property="og:title" content="{{ $globalSeo['web_title'].$globalSeo['meta_description'] }}">
        <meta name="keywords" content="{{ $globalSeo['meta_keyword'] }}">
        <meta name="description" content="{{ $globalSeo['meta_description'] }}">
    @endif





    <!-- 主要css -->
    @yield('fontcss')

   <link rel="stylesheet" href="/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/js/vendor/slick/slick.css">
    <link rel="stylesheet" href="/assets/js/vendor/slick/slick-theme.css">
    <link rel="stylesheet" href="/assets/js/vendor/easydropdown/easydropdown.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/css/search.css">
    <link rel="stylesheet" href="/assets/css/cgpagechange.css">    
    <link rel="stylesheet" href="/assets/js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.css">
 
    


    @yield('css')

</head>
    
<body @yield('bodySetting') @if(!empty($pageBodyId))   class="{{ $pageBodyId }}"   @else  @endif>



    @yield('content')



    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

    <!--共用 js-->
    
    


    



<script type="text/javascript">

    //前台系統訊息
    @if( Session::get('Message') )
    $(function() {
        alert( "{{Session::get('Message')}}" );
    });
    @endif

</script>



    <!--共用-->
    @yield('frontscript')

    <script src="/assets/js/vendor/jquery.1.11.3.min.js"></script>
    <script src="/assets/js/vendor/slick/slick.min.js"></script>
    <script src="/assets/js/vendor/waypoints.min.js"></script>
    <script src="/assets/js/vendor/cgpagechange.js"></script>
    <script src="/assets/js/vendor/ui-easing.js"></script>
    <script src="/assets/js/vendor/easydropdown/jquery.easydropdown.js"></script>
   <!--  scroll -->
    <script src="/assets/js/vendor/jquery.mCustomScrollbar/jquery-ui-1.10.4.min.js"></script>
    <script src="/assets/js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.min.js"></script>
    <script src="/assets/js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!--共用js-->
    <script src="/assets/js/main.js"></script>
    
    <script src="/assets/js/main_waypoint.js"></script>

    <script src="/assets/js/box.js"></script>
    <!--補瀏覽器前綴-->
    <script src="/assets/js/vendor/prefixfree.min.js"></script>
    <script >
        
        load.ft();
    </script>
@yield('script')


</body>



</html>
